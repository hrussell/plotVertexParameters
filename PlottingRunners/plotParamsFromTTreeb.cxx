//
//  plotParamsFromTTree.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/HistTools.h"

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
double jetSliceWeights[13];
TStopwatch timer;


double wrapPhi(double phi);
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
    
}


int main(int argc, char **argv){
    
    std::cout << "running program!" << std::endl;
    
    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
        
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
        
    //Directory the plots will go in

    
    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    
    //one set of histograms for barrel, one for endcaps,
    //one for the full detector together (nice for 2D maps)
    
    setPrettyCanvasStyle();
    
    TH1::SetDefaultSumw2();
    
    timer.Start();
	
    TString plotDirectory = TString(argv[2]);

    TFile *outputFile = new TFile(plotDirectory+"/outputDeltaRTest.root","RECREATE");
    HistTools* jetLLPTest = new HistTools;
    jetLLPTest->addHist("jetLLPdR",100,0,1);
    jetLLPTest->addHist("jetLLPdEta",100,0,1);
    jetLLPTest->addHist("jetLLPdPhi",100,0,1);
    jetLLPTest->addHist("LLPLxy",20,0,5);
    jetLLPTest->addHist("LLPEta",25,-2.5,2.5);
    jetLLPTest->addHist("LLPPhi",32,-3.2,3.2);
    jetLLPTest->addHist("LLPLz",20,0,5);

    jetLLPTest->addHist("jetCR",50,0,2);
    jetLLPTest->addHist("jetEta",25,-2.5,2.5);
    jetLLPTest->addHist("jetPhi",32,-3.2,3.2);
    jetLLPTest->addHist("jetET",50,0,500);

    
    std::cout << "filled hists!" << std::endl;
    
    
    //Directory the plots will go in
    
    
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain);
    llpVar->setZeroVectors();
    llpVar->setBranchAdresses(chain);
    
    timer.Start();
    
    setJetSliceWeights(jetSliceWeights);
    
    int jetSlice = -1;
    double finalWeight = 0;
    
    for (int l=0;l<chain->GetEntries();l++)
    {
        
        if(l % 10000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        
        chain->GetEntry(l);

        std::vector<int> llpMatching;
        
        for(int i=0;i<commonVar->Jet_indexLLP->size(); i++){
        
            if(commonVar->Jet_indexLLP->at(i) < 0 && commonVar->Jet_logRatio->at(i) > 1.0 && TMath::Abs(commonVar->Jet_eta->at(i)) < 2.5 ){ //criteria for being a weird jet

                double eta = commonVar->Jet_eta->at(i);
                double phi = commonVar->Jet_phi->at(i);
                
                jetLLPTest->fill("jetCR",commonVar->Jet_logRatio->at(i) ,1.0);
                jetLLPTest->fill("jetEta",eta ,1.0);
                jetLLPTest->fill("jetPhi",phi,1.0);
                jetLLPTest->fill("jetET",commonVar->Jet_ET->at(i) ,1.0);
                
            
                double smalldR = 99; double smalldEta=99; double smalldPhi=99;
                double indexClosest = -1;
                
                for(int j=0; j<llpVar->eta->size(); j++){
                
                llpMatching.push_back(0);
                double dR_llp = DeltaR(phi,llpVar->phi->at(j),eta,llpVar->eta->at(j));
                    
                if(dR_llp < smalldR){
                    smalldR = dR_llp;
                    indexClosest = j;
                    smalldEta = TMath::Abs(eta - llpVar->eta->at(j));
                    smalldPhi = TMath::Abs(wrapPhi(phi-llpVar->phi->at(j)));
                }
                
            }
                
		jetLLPTest->fill("jetLLPdR",smalldR,1.0);
		jetLLPTest->fill("jetLLPdEta",smalldEta,1.0);
                jetLLPTest->fill("jetLLPdPhi",smalldPhi,1.0);
                jetLLPTest->fill("LLPLxy",llpVar->Lxy->at(indexClosest)*0.001,1.0);
                jetLLPTest->fill("LLPEta",llpVar->eta->at(indexClosest),1.0);
                jetLLPTest->fill("LLPPhi",llpVar->phi->at(indexClosest),1.0);
                jetLLPTest->fill("LLPLz",llpVar->Lz->at(indexClosest)*0.001,1.0);

                
        }
    } //end jet loop
    llpVar->clearAllVectors();
    commonVar->clearAllVectors();
   }//end loop through events 
    std::cout << "plotted hists!" << std::endl;

    outputFile->Write();
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
}
