//
//  doLimitExtrapolation.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 03/09/16.
//
//

#include <stdio.h>

#include <TH2.h>
#include <TStyle.h>
#include <iostream>
#include "math.h"
#include <TMath.h>
#include <TRandom3.h>
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TEfficiency.h"
#include <sys/time.h>
#include <numeric>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <vector>
#include <string>
#include "TGraphAsymmErrors.h"
#include <fstream>
#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/HistTools.h"
#include "PlottingPackage/SampleDetails.h"
#include "PlottingPackage/ExtrapolationUtils.h"

using namespace ExtrapolationUtils;

#define PI 3.14159265
#define sq(x) ((x)*(x))

const double LUMI = 32864.+3212.96; //in pb-1
//18857.4+3193.68

//these are in metres?
const double LTSTEP = 0.05;
const double LTSTEP2 = 0.5;
const double LTSTEP3 = 0.005;

//trigger scaling
const double delta_MC = 13.262;
const double delta_DATA = 12.171;
const double sigma_MC = 2.084;
const double sigma_DATA = 2.9548;

//speed of light
const double c = 299.7924580;// in mm/ns
const double VERTEX_DR_CUT = 1.0; //(minimum delta R between two vertices; used to count expected signal)
typedef unsigned long long timestamp_t;
static timestamp_t
get_timestamp ()
{
    struct timeval now;
    gettimeofday (&now, NULL);
    return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}
double myLogScale(double xMin, double nPoints, int nBin);
double myLogScale(double xMin, double nPoints, int nBin){
    double a = floor(double(nBin) / nPoints );
    double b = double(nBin % int(nPoints));

    return pow(10, log10(xMin) + a)*(1 + 0.1*b);

}

//Systematic errors switch
const bool addSystematicErrors = true;

//Ratios switch
#define doRatios false
double LUMISYSERR = 0.0;

TH1D *h_BB_Decays, *h_BE_Decays, *h_EE_Decays, *h_1E_Decays, *h_1B_Decays;
TH1D *h_Expected_2MSVx;
TH1D *h_Expected_1MSVx, *h_Expected_1MSVx_2j150;

timestamp_t t0;

using namespace std;
using namespace plotVertexParameters;

int nMSvx_BB,nMSvx_BE,nMSvx_EE;

//random number
TRandom3 rnd;

bool debug = false;
double nEvents = 0;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}
int main(int argc, char **argv){
    if(addSystematicErrors){
        LUMISYSERR = 0.029; //from https://twiki.cern.ch/twiki/bin/view/Atlas/LuminosityForPhysics#2016_13_TeV_ICHEP_2016
    }

    nBB_Decay=0;
    nBE_Decay=0;
    nEE_Decay=0;
    n1B_Decay=0;
    n1E_Decay=0;
    
    TChain *chain = new TChain("recoTree");

    std::cout << "running program!" << std::endl;
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over sample: " << argv[1] << std::endl;
    std::cout << "Results aren't put in the folder: " << argv[2] << ". I'm ignoring it. Haha! Instead, they're in badReweighting/" << std::endl;
    std::cout << "Output File name is: " << argv[3] << std::endl;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    t0 = get_timestamp();

    Double_t xBinValues[541];
    for(int j=0; j<541; j++){
        xBinValues[j] = myLogScale(0.001, 90, j);
    }

    LLPVariables *toyLLP = new LLPVariables;
    toyLLP->setZeroVectors();
    std::cout << "I'm running over: "  << chain->GetEntries()<< " events" << std::endl;
    toyLLP->setToyBranchAddresses(chain);

    bool lDebug=true;
    
    TString output="pp";

    TString amend="";

    //set parameters: masses, systematics...

    //string to enum?!?
    SampleDetails::setGlobalVariables(TString(argv[1]));

    std::cout << "Proper lifetime of this sample is: " << SampleDetails::sim_ctau << std::endl;

    //////////////////////////////
    //* Histograms declaration *//
    TFile * outputFile = new TFile("../OutputPlots/badReweighting/"+TString(argv[3]),"RECREATE");
    
    std::cout << "output ROOT file " << output << std::endl;
    TDirectory *d_Expected = outputFile->mkdir("ExpectedEvents");


    h_1B_Decays = new TH1D("n1B_Decays","",540, xBinValues);
    h_1E_Decays = new TH1D("n1E_Decays","",540, xBinValues);
    h_BB_Decays = new TH1D("nBB_Decays","",540, xBinValues);
    h_BE_Decays = new TH1D("nBE_Decays","",540, xBinValues);
    h_EE_Decays = new TH1D("nEE_Decays","",540, xBinValues);

    for (int i_evt=0; i_evt < chain->GetEntries(); i_evt++)
    {
        /* if(i_evt % 1000 == 0){
         timer.Stop();
         std::cout << "event " << i_evt  << " at time " << timer.RealTime() << std::endl;
         timer.Start();
         }
         */
        chain->GetEntry(i_evt);
        if(debug) std::cout << "retrieved event " << i_evt << std::endl;

        toyLLP->SetAllVariables();

        nEvents+= 1.0;
        if(i_evt == 0) rnd.SetSeed(toyLLP->pT->at(0));
        if(i_evt % 10000 == 0) {
            Long64_t nEntries = chain->GetEntries();
            timestamp_t t1 = get_timestamp();
            double time1 = (t1 - t0) / 1000000.0L;
            // if(time1 > 0.0000031) cout << "time1 " << time1 << endl;
            cout << "nEvent: " << nEvents << "/" << nEntries  << " at time " << time1 << endl;
            t0 = t1;
        }

        double deltar_llps = -99;

        if(toyLLP->eta->size() == 2){
            deltar_llps = DeltaR(toyLLP->phi->at(0), toyLLP->phi->at(1), toyLLP->eta->at(0), toyLLP->eta->at(1));
        } else{
            cout << "there are: " << toyLLP->eta->size() << " llps in this event! That's not equal to 2... oh no!" << endl;
            //nEventsWithout2LLPs++;
            continue;
        }

        //if(deltar_vpi < 2.0) continue; //only if doing 2vx search only!

        if(TMath::Abs(toyLLP->eta->at(0)) > 2.5 && TMath::Abs(toyLLP->eta->at(1)) > 2.5)
            continue; //abcd can use one up to 3.2 (end of calorimeters...)
        else  if(TMath::Abs(toyLLP->eta->at(0)) > 3.2 || TMath::Abs(toyLLP->eta->at(1)) > 3.2)
            continue;
        if(debug) std::cout << "topology eta check is ok! " << std::endl;

        ////////////////////////////////
        //* Varibales initialization *//

        int barreldec = 0;
        int endcapdec = 0;

        double R1=0.;
        double R2=0.;
        double z1=0.;
        double z2=0.;
        double eta1=0.;
        double eta2=0.;
        double ctau = -999;

        //k's for sim_ctaus: 250 - 384, 500 - 304, 800 - 284, 1200 - 200, 1500 - 180, 2000 - 148
        //need to divide these, these are for the other binning
        //now these are wrong.
        
        //mH400mS50: 0.7, 1.26 correspond to 280, 410

        for(int k=0; k<540; ++k) {
            ctau = xBinValues[k] + 0.0005;

            //if(ctau > 10) break;

            std::vector<TVector3> new_decay_pos;
            for(unsigned int i=0; i<toyLLP->eta->size(); ++i) {

                ////////////////////////////////
                //* Generating v-pion decays *//O

                double bgct = toyLLP->bg->at(i)*ctau*1000.0; //in mm
                double Lxyz = rnd.Exp(bgct);
                TVector3 tmp_pos;
                tmp_pos.SetMagThetaPhi(Lxyz,toyLLP->theta->at(i),toyLLP->phi->at(i));
                new_decay_pos.push_back(tmp_pos);
            
            }
        } //end loop through ctaus
        //return kTRUE;
        if(debug) std::cout << "done event" << std::endl;
        toyLLP->clearAllToyVectors();
    } //end loop through events?


    cout << "nBB, BE, EE decays"  << endl;
    cout << nBB_Decay << ", " << nBE_Decay << ", " << nEE_Decay << endl;

       outputFile->Write();

}

