void plotSystematics(){
	//0.7 < |η| < 1.0 or 1.5 < |η| < 1.7
	    TCanvas* c1 = new TCanvas("c1","c1",800,600);

	c1->SetRightMargin(0.05);
	c1->SetTopMargin(0.05);
	c1->SetLeftMargin(0.15);
	c1->SetBottomMargin(0.15);


	TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/dijetData/outputMSSystematics.root");

	TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/bkgdJZ/JZ3_8/outputMSSystematics.root");

	TH1D *h_MSeg_MC = (TH1D*)_file1->Get("full_nAssocMSeg_LJ");
	TH1D *h_MSeg_Data = (TH1D*)_file0->Get("full_nAssocMSeg_LJ");
 h_MSeg_Data->SetLineColor(kBlack);h_MSeg_Data->SetMarkerColor(kBlack);h_MSeg_Data->SetMarkerSize(1.2);h_MSeg_Data->SetLineWidth(2);h_MSeg_Data->SetMarkerStyle(20);
 h_MSeg_MC->SetLineColor(kBlue+2);h_MSeg_Data->SetMarkerColor(kBlue+2);h_MSeg_Data->SetMarkerSize(1.2);h_MSeg_Data->SetLineWidth(2);h_MSeg_Data->SetMarkerStyle(4);	

	double DataIntegral_LJ = h_MSeg_Data->Integral();
	double MCIntegral_LJ = h_MSeg_MC->Integral();
	h_MSeg_Data->SetTitle("");
	h_MSeg_MC->Scale(DataIntegral_LJ/MCIntegral_LJ);
	c1->SetLogy(1);
	c1->SetLogx(1);
	h_MSeg_Data->Draw();
	h_MSeg_MC->Draw("SAME");

	h_MSeg_Data->GetXaxis()->SetTitle("Number of associated MSegs");
	h_MSeg_Data->GetYaxis()->SetTitle("Number of leading jets");

	c1->Print("SystematicsOutputs/dataAndMC_nMSegs_LJ.pdf");
		c1->SetLogy(0);
	c1->SetLogx(0);
	TH1D *h_etaNSeg_MC = (TH1D*)_file1->Get("full_eta_nAssocMSegLJ");
	TH1D *h_etaNSeg_Data = (TH1D*)_file0->Get("full_eta_nAssocMSegLJ");
	h_etaNSeg_MC->SetTitle("");
	//h_etaNSeg_MC->Scale(h_etaNSeg_Data->Integral()/h_etaNSeg_MC->Integral()); 

	h_etaNSeg_MC->SetLineColor(kBlue+2);h_etaNSeg_MC->SetMarkerColor(kBlue+2);h_etaNSeg_MC->SetMarkerSize(1.2);h_etaNSeg_MC->SetLineWidth(2);h_etaNSeg_MC->SetMarkerStyle(4);
	//h_etaNSeg_MC->Scale(DataIntegral_LJ/MCIntegral_LJ);
	h_etaNSeg_MC->Draw();
	h_etaNSeg_MC->GetYaxis()->SetRangeUser(0,10);
	h_etaNSeg_MC->GetXaxis()->SetTitle("Leading jet #eta");
	h_etaNSeg_MC->GetYaxis()->SetTitle("<associated muon segments>");

	h_etaNSeg_Data->Draw("SAME");

	c1->Print("SystematicsOutputs/dataAndMC_eta_vs_nAssocMSeg.pdf");

	h_etaNSeg_MC->Divide(h_etaNSeg_Data);
	h_etaNSeg_MC->Draw();	
	c1->Print("SystematicsOutputs/MCoverData_eta_vs_nAssocMSeg.pdf");


	TH1D *h_MC1 = (TH1D*)_file1->Get("eta1_nMuonRoIs_AllJets");
	TH1D *h_Data1 = (TH1D*)_file0->Get("eta1_nMuonRoIs_AllJets");
	TH1D *h_MC2 = (TH1D*)_file1->Get("eta2_nMuonRoIs_AllJets");
	TH1D *h_Data2 = (TH1D*)_file0->Get("eta2_nMuonRoIs_AllJets");

	TH1D *h_MC3 = (TH1D*)_file1->Get("eta3_nMuonRoIs_AllJets");
	TH1D *h_Data3 = (TH1D*)_file0->Get("eta3_nMuonRoIs_AllJets");

	TH1D *h_MC4 =  (TH1D*)_file1->Get("eta4_nMuonRoIs_AllJets");
	TH1D *h_Data4 = (TH1D*)_file0->Get("eta4_nMuonRoIs_AllJets");

	TH1D *h_MC5 = (TH1D*)_file1->Get("eta5_nMuonRoIs_AllJets");
	TH1D *h_Data5 = (TH1D*)_file0->Get("eta5_nMuonRoIs_AllJets");

 h_Data1->SetLineColor(kBlack);h_Data1->SetMarkerColor(kBlack);h_Data1->SetMarkerSize(1.2);h_Data1->SetLineWidth(2);h_Data1->SetMarkerStyle(20);
 h_Data2->SetLineColor(kBlack);h_Data2->SetMarkerColor(kBlack);h_Data2->SetMarkerSize(1.2);h_Data2->SetLineWidth(2);h_Data2->SetMarkerStyle(20);
 h_Data3->SetLineColor(kBlack);h_Data3->SetMarkerColor(kBlack);h_Data3->SetMarkerSize(1.2);h_Data3->SetLineWidth(2);h_Data3->SetMarkerStyle(20);
 h_Data4->SetLineColor(kBlack);h_Data4->SetMarkerColor(kBlack);h_Data4->SetMarkerSize(1.2);h_Data4->SetLineWidth(2);h_Data4->SetMarkerStyle(20);
 h_Data5->SetLineColor(kBlack);h_Data5->SetMarkerColor(kBlack);h_Data5->SetMarkerSize(1.2);h_Data5->SetLineWidth(2);h_Data5->SetMarkerStyle(20);
	h_MC1->Scale(h_Data1->Integral()/h_MC1->Integral()); h_MC1->SetLineColor(kBlue+2);h_MC1->SetMarkerColor(kBlue+2);h_MC1->SetMarkerSize(1.2);h_MC1->SetLineWidth(2);h_MC1->SetMarkerStyle(22);
	h_MC2->Scale(h_Data2->Integral()/h_MC2->Integral());h_MC2->SetLineColor(kRed+2);h_MC2->SetMarkerColor(kRed+2);h_MC2->SetMarkerSize(1.2);h_MC2->SetLineWidth(2);h_MC2->SetMarkerStyle(22);
	h_MC3->Scale(h_Data3->Integral()/h_MC3->Integral());h_MC3->SetLineColor(kGreen-3);h_MC3->SetMarkerColor(kGreen-3);h_MC3->SetMarkerSize(1.2);h_MC3->SetLineWidth(2);h_MC3->SetMarkerStyle(22);
	h_MC4->Scale(h_Data4->Integral()/h_MC4->Integral());h_MC4->SetLineColor(kViolet-7);h_MC4->SetMarkerColor(kViolet-7);h_MC4->SetMarkerSize(1.2);h_MC4->SetLineWidth(2);h_MC4->SetMarkerStyle(22);
	h_MC5->Scale(h_Data5->Integral()/h_MC5->Integral());h_MC5->SetLineColor(kAzure-3);h_MC5->SetMarkerColor(kAzure-3);h_MC5->SetMarkerSize(1.2);h_MC5->SetLineWidth(2);h_MC5->SetMarkerStyle(22);

	h_MC1->SetMinimum(0);
	h_MC2->SetMinimum(0);
	h_MC3->SetMinimum(0);
	h_MC4->SetMinimum(0);
	h_MC5->SetMinimum(0);

	h_MC1->GetXaxis()->SetTitle("Number of Muon RoIs");
	h_MC2->GetXaxis()->SetTitle("Number of Muon RoIs");
	h_MC3->GetXaxis()->SetTitle("Number of Muon RoIs");
	h_MC4->GetXaxis()->SetTitle("Number of Muon RoIs");
	h_MC5->GetXaxis()->SetTitle("Number of Muon RoIs");

	h_MC1->GetYaxis()->SetTitle("Number of jets");
	h_MC2->GetYaxis()->SetTitle("Number of jets");
	h_MC3->GetYaxis()->SetTitle("Number of jets");
	h_MC4->GetYaxis()->SetTitle("Number of jets");
	h_MC5->GetYaxis()->SetTitle("Number of jets");

	h_MC1->SetTitle("");
	h_MC2->SetTitle("");
	h_MC3->SetTitle("");
	h_MC4->SetTitle("");
	h_MC5->SetTitle("");

	h_MC1->Draw();
	h_Data1->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC1.pdf");
	h_MC2->Draw();
	h_Data2->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC2.pdf");
	h_MC3->Draw();
	h_Data3->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC3.pdf");
	h_MC4->Draw();
	h_Data4->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC4.pdf");
	h_MC5->Draw();
	h_Data5->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC5.pdf");

	h_MC1->Divide(h_Data1);
	h_MC2->Divide(h_Data2);
	h_MC3->Divide(h_Data3);
	h_MC4->Divide(h_Data4);
	h_MC5->Divide(h_Data5);

	h_MC1->GetYaxis()->SetRangeUser(0,5);
	h_MC2->GetYaxis()->SetRangeUser(0,5);
	h_MC3->GetYaxis()->SetRangeUser(0,5);
	h_MC4->GetYaxis()->SetRangeUser(0,5);
	h_MC5->GetYaxis()->SetRangeUser(0,5);

	h_MC1->GetXaxis()->SetRangeUser(0,8);
	h_MC2->GetXaxis()->SetRangeUser(0,8);
	h_MC3->GetXaxis()->SetRangeUser(0,8);
	h_MC4->GetXaxis()->SetRangeUser(0,8);
	h_MC5->GetXaxis()->SetRangeUser(0,8);
TF1 *f1 = new TF1("fit1", "pol0", 0, 8);

h_MC1->Draw();
h_MC1->Fit("fit1");
	c1->Print("SystematicsOutputs/MCoverDataSlice1.pdf");
h_MC2->Draw();
h_MC2->Fit("fit1");
	c1->Print("SystematicsOutputs/MCoverDataSlice2.pdf");
h_MC3->Draw();
h_MC3->Fit("fit1");
	c1->Print("SystematicsOutputs/MCoverDataSlice3.pdf");
h_MC4->Draw();
h_MC4->Fit("fit1");
	c1->Print("SystematicsOutputs/MCoverDataSlice4.pdf");
h_MC5->Draw();
h_MC5->Fit("fit1");
	c1->Print("SystematicsOutputs/MCoverDataSlice5.pdf");

	
	TH1D *h_trackletMC1 = (TH1D*)_file1->Get("eta1_nTracklets_AllJets");
	TH1D *h_trackletData1 = (TH1D*)_file0->Get("eta1_nTracklets_AllJets");
	TH1D *h_trackletMC2 = (TH1D*)_file1->Get("eta2_nTracklets_AllJets");
	TH1D *h_trackletData2 = (TH1D*)_file0->Get("eta2_nTracklets_AllJets");

	TH1D *h_trackletMC3 = (TH1D*)_file1->Get("eta3_nTracklets_AllJets");
	TH1D *h_trackletData3 = (TH1D*)_file0->Get("eta3_nTracklets_AllJets");

	TH1D *h_trackletMC4 =  (TH1D*)_file1->Get("eta4_nTracklets_AllJets");
	TH1D *h_trackletData4 = (TH1D*)_file0->Get("eta4_nTracklets_AllJets");

	TH1D *h_trackletMC5 = (TH1D*)_file1->Get("eta5_nTracklets_AllJets");
	TH1D *h_trackletData5 = (TH1D*)_file0->Get("eta5_nTracklets_AllJets");

 h_trackletData1->SetLineColor(kBlack);h_trackletData1->SetMarkerColor(kBlack);h_trackletData1->SetMarkerSize(1.2);h_trackletData1->SetLineWidth(2);h_trackletData1->SetMarkerStyle(20);
 h_trackletData2->SetLineColor(kBlack);h_trackletData2->SetMarkerColor(kBlack);h_trackletData2->SetMarkerSize(1.2);h_trackletData2->SetLineWidth(2);h_trackletData2->SetMarkerStyle(20);
 h_trackletData3->SetLineColor(kBlack);h_trackletData3->SetMarkerColor(kBlack);h_trackletData3->SetMarkerSize(1.2);h_trackletData3->SetLineWidth(2);h_trackletData3->SetMarkerStyle(20);
 h_trackletData4->SetLineColor(kBlack);h_trackletData4->SetMarkerColor(kBlack);h_trackletData4->SetMarkerSize(1.2);h_trackletData4->SetLineWidth(2);h_trackletData4->SetMarkerStyle(20);
 h_trackletData5->SetLineColor(kBlack);h_trackletData5->SetMarkerColor(kBlack);h_trackletData5->SetMarkerSize(1.2);h_trackletData5->SetLineWidth(2);h_trackletData5->SetMarkerStyle(20);
	h_trackletMC1->Scale(h_trackletData1->Integral()/h_trackletMC1->Integral()); h_trackletMC1->SetLineColor(kBlue+2);h_trackletMC1->SetMarkerColor(kBlue+2);h_trackletMC1->SetMarkerSize(1.2);h_trackletMC1->SetLineWidth(2);h_trackletMC1->SetMarkerStyle(22);
	h_trackletMC2->Scale(h_trackletData2->Integral()/h_trackletMC2->Integral());h_trackletMC2->SetLineColor(kRed+2);h_trackletMC2->SetMarkerColor(kRed+2);h_trackletMC2->SetMarkerSize(1.2);h_trackletMC2->SetLineWidth(2);h_trackletMC2->SetMarkerStyle(22);
	h_trackletMC3->Scale(h_trackletData3->Integral()/h_trackletMC3->Integral());h_trackletMC3->SetLineColor(kGreen-3);h_trackletMC3->SetMarkerColor(kGreen-3);h_trackletMC3->SetMarkerSize(1.2);h_trackletMC3->SetLineWidth(2);h_trackletMC3->SetMarkerStyle(22);
	h_trackletMC4->Scale(h_trackletData4->Integral()/h_trackletMC4->Integral());h_trackletMC4->SetLineColor(kViolet-7);h_trackletMC4->SetMarkerColor(kViolet-7);h_trackletMC4->SetMarkerSize(1.2);h_trackletMC4->SetLineWidth(2);h_trackletMC4->SetMarkerStyle(22);
	h_trackletMC5->Scale(h_trackletData5->Integral()/h_trackletMC5->Integral());h_trackletMC5->SetLineColor(kAzure-3);h_trackletMC5->SetMarkerColor(kAzure-3);h_trackletMC5->SetMarkerSize(1.2);h_trackletMC5->SetLineWidth(2);h_trackletMC5->SetMarkerStyle(22);

	h_trackletMC1->SetMinimum(0);
	h_trackletMC2->SetMinimum(0);
	h_trackletMC3->SetMinimum(0);
	h_trackletMC4->SetMinimum(0);
	h_trackletMC5->SetMinimum(0);

	h_trackletMC1->SetTitle("");
	h_trackletMC2->SetTitle("");
	h_trackletMC3->SetTitle("");
	h_trackletMC4->SetTitle("");
	h_trackletMC5->SetTitle("");
	h_trackletMC1->GetXaxis()->SetTitle("Number of tracklets");
	h_trackletMC2->GetXaxis()->SetTitle("Number of tracklets");
	h_trackletMC3->GetXaxis()->SetTitle("Number of tracklets");
	h_trackletMC4->GetXaxis()->SetTitle("Number of tracklets");
	h_trackletMC5->GetXaxis()->SetTitle("Number of tracklets");

	h_trackletMC1->GetYaxis()->SetTitle("Number of jets");
	h_trackletMC2->GetYaxis()->SetTitle("Number of jets");
	h_trackletMC3->GetYaxis()->SetTitle("Number of jets");
	h_trackletMC4->GetYaxis()->SetTitle("Number of jets");
	h_trackletMC5->GetYaxis()->SetTitle("Number of jets");

	h_trackletMC1->Draw();
	h_trackletData1->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC1_tracklets.pdf");
	h_trackletMC2->Draw();
	h_trackletData2->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC2_tracklets.pdf");
	h_trackletMC3->Draw();
	h_trackletData3->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC3_tracklets.pdf");
	h_trackletMC4->Draw();
	h_trackletData4->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC4_tracklets.pdf");
	h_trackletMC5->Draw();
	h_trackletData5->Draw("SAME");
	c1->Print("SystematicsOutputs/dataAndMC5_tracklets.pdf");

	h_trackletMC1->Divide(h_trackletData1);
	h_trackletMC2->Divide(h_trackletData2);
	h_trackletMC3->Divide(h_trackletData3);
	h_trackletMC4->Divide(h_trackletData4);
	h_trackletMC5->Divide(h_trackletData5);
	h_trackletMC1->GetYaxis()->SetRangeUser(0,5);
	h_trackletMC2->GetYaxis()->SetRangeUser(0,5);
	h_trackletMC3->GetYaxis()->SetRangeUser(0,5);
	h_trackletMC4->GetYaxis()->SetRangeUser(0,5);
	h_trackletMC5->GetYaxis()->SetRangeUser(0,5);

	h_trackletMC1->GetXaxis()->SetRangeUser(0,11);
	h_trackletMC2->GetXaxis()->SetRangeUser(0,11);
	h_trackletMC3->GetXaxis()->SetRangeUser(0,11);
	h_trackletMC4->GetXaxis()->SetRangeUser(0,11);
	h_trackletMC5->GetXaxis()->SetRangeUser(0,11);

TF1 *f2 = new TF1("fit2", "pol0", 0, 11);

h_trackletMC1->Draw();
h_trackletMC1->Fit("fit2");
	c1->Print("SystematicsOutputs/MCoverDataSlice1_tracklets.pdf");
h_trackletMC2->Draw();
h_trackletMC2->Fit("fit2");
	c1->Print("SystematicsOutputs/MCoverDataSlice2_tracklets.pdf");
h_trackletMC3->Draw();
h_trackletMC3->Fit("fit2");
	c1->Print("SystematicsOutputs/MCoverDataSlice3_tracklets.pdf");
h_trackletMC4->Draw();
h_trackletMC4->Fit("fit2");
	c1->Print("SystematicsOutputs/MCoverDataSlice4_tracklets.pdf");
h_trackletMC5->Draw();
h_trackletMC5->Fit("fit2");
	c1->Print("SystematicsOutputs/MCoverDataSlice5_tracklets.pdf");

}
