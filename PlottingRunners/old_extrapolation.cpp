//
//  doLimitExtrapolation.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 03/09/16.
//
//

#include <stdio.h>

#include <TH2.h>
#include <TStyle.h>
#include <iostream>
#include "math.h"
#include <TMath.h>
#include <TRandom3.h>
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TEfficiency.h"
#include <sys/time.h>
#include <numeric>

#define PI 3.14159265
#define sq(x) ((x)*(x))
const double LUMI = 18857.4+3193.68;
const double LTSTEP = 0.025;
const double LTSTEP2 = 0.25;
const double LTSTEP3 = 0.0025;

//trigger scaling
const double delta_MC = 10.137;
const double delta_DATA = 9.046;
const double sigma_MC = 2.084;
const double sigma_DATA = 2.9548;

//speed of light
const double c = 299.7924580;// in mm/ns
const double VERTEX_DR_CUT = 2.0; //(minimum delta R between two vertices; used to count expected signal)


//Systematic errors switch
const bool addSystematicErrors = true;

//Ratios switch
#define doRatios false
const double LUMISYSERR = 0.0;
if(addSystematicErrors){
    LUMISYSERR = 0.029; //from https://twiki.cern.ch/twiki/bin/view/Atlas/LuminosityForPhysics#2016_13_TeV_ICHEP_2016
}

//random number
TRandom3 rnd;

using namespace std;

int main(int argc, char **argv){
    
    std::cout << "running program!" << std::endl;
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over sample: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output File name is: " << argv[3] << std::endl;
    
    t0 = get_timestamp();
    
    double xBinValues[1201];
    LimitExtrapolation::make_x_axis(xBinValues);
    
    nEvents = 0;
    
    CutflowCounter cutflow_2msvx;
    CutflowCounter cutflow_1msvx;
    
    cutflow_2msvx.initialize();
    cutflow_2msvx.addVariable("nEvents");
    cutflow_2msvx.addVariable("nBB_Decay");
    cutflow_2msvx.addVariable("nBE_Decay");
    cutflow_2msvx.addVariable("nEE_Decay");
    
    cutflow_2msvx.addVariable("nBB_Trig");
    cutflow_2msvx.addVariable("nBE_Trig");
    cutflow_2msvx.addVariable("nEE_Trig");
    
    cutflow_2msvx.addVariable("nBB_TrigVx");
    cutflow_2msvx.addVariable("nBE_TrigVx");
    cutflow_2msvx.addVariable("nEE_TrigVx");
    
    cutflow_2msvx.addVariable("nBB_TrigVx_Vx");
    cutflow_2msvx.addVariable("nBE_TrigVx_Vx");
    cutflow_2msvx.addVariable("nEE_TrigVx_Vx");
    
    cutflow_1msvx.initialize();
    cutflow_1msvx.addVariable("nEvents");
    cutflow_1msvx.addVariable("nBTrig");
    cutflow_1msvx.addVariable("nETrig");
    cutflow_1msvx.addVariable("nBTrig_2Jets");
    cutflow_1msvx.addVariable("nETrig_2Jets");
    cutflow_1msvx.addVariable("nBTrig_2Jets_Vx");
    cutflow_1msvx.addVariable("nETrig_2Jets_Vx");
    
    BROISYSTERR = 0; EROISYSTERR = 0;
    BMSVXSYSTERR = 0; EMSVXSYSTERR = 0;
    IDVXSYSTERR = 0;
    
    LLPVariables *toyLLP = new LLPVariables;
    toyLLP->setZeroVectors();
    toyLLP->setToyBranchAddressses(chain);
    
    for (int i_evt=0;i_evt<chain->GetEntries();i_evt++)
    {
        
        if(i_evt % 1000 == 0){
            timer.Stop();
            std::cout << "event " << i_evt  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        
        chain->GetEntry(i_evt);
        
        bool lDebug=true;
        
        if(nEvents == 0) {
            
            TString inputFile = GetFileName();
            TString output="pp";
            
            TString amend="";
            
            cout << "input File: " << inputFile << endl;
            BROISYSTERR = 0.01; EROISYSTERR = 0.01
            
            //set parameters: masses, systematics...
            
            if(inputFile.Contains("250")) {
                cout << "Gluino mass is 250 GeV" << endl;
                mediatorMass = 250; vpionMass = 100; sim_ctau = 2.2; pdgid = 3000001;
                BMSVXSYSTERR = 0.01; EMSVXSYSTERR = 0.01;
            } else if(inputFile.Contains("500")) {
                cout << "Gluino mass is 500 GeV" << endl;
                mediatorMass = 500; vpionMass = 100; sim_ctau = 1.38; pdgid = 3000001;
                BMSVXSYSTERR = 0.01; EMSVXSYSTERR = 0.01;
            } else if(inputFile.Contains("800")) {
                cout << "Gluino mass is 800 GeV" << endl;
                mediatorMass = 800; vpionMass = 100; sim_ctau = 0.95; pdgid = 3000001;
                BMSVXSYSTERR = 0.01; EMSVXSYSTERR = 0.01;
            } else if(inputFile.Contains("1200")) {
                cout << "Gluino mass is 1200 GeV" << endl;
                mediatorMass = 1200; vpionMass = 100; sim_ctau = 0.69; pdgid = 3000001;
                BMSVXSYSTERR = 0.01; EMSVXSYSTERR = 0.01;
            } else {
                cout << "ERROR! Unrecognized input file: " << inputFile << endl;
                return -1;
            }
            
            if(!addSystematicErrors){
                BMSVXSYSTERR = 0.0; EMSVXSYSTERR = 0.0; IDVXSYSTERR = 0.0;
                BROISYSTERR = 0.0; EROISYSTERR = 0.0;
                amend = "_NOSYSERROR";
            }
            bool REBIN_1D(false);
            if(REBIN_1D) amend = "REBIN_1D";
            
            cout << "Systematic Errors are: " << addSystematicErrors << " and have values..." << endl;
            cout << "Barrel RoI:" << BROISYSTERR << ", Endcap RoI: " << EROISYSTERR << endl;
            cout << "Barrel MSVx:" << BMSVXSYSTERR << ", Endcap MSVx: " << EMSVXSYSTERR << endl;
            cout << "Luminosity (not included in total error calculation at end!): " << LUMISYSERR << endl;
            cout << "Note that these are all relative errors!" << endl;
            
            if(mediatorMass==110){
                MediatorXS = 18400;
                minMediatorXS = 0;	// minMediatorXS = 0.153749;
                maxMediatorXS = 0;    // maxMediatorXS = 0.153749;
            } else if(mediatorMass==250){
                MediatorXS = 302.3;
                minMediatorXS = 0;     // minMediatorXS = 0.148648;
                maxMediatorXS = 0;     // maxMediatorXS = 0.148648;
            } else if(mediatorMass==500){
                MediatorXS =  4.52485;
                minMediatorXS = 0;     // minMediatorXS = 0.157449;
                maxMediatorXS = 0;     // maxMediatorXS = 0.157449;
            } else if(mediatorMass==800){
                MediatorXS = 0.157399;
                minMediatorXS = 0;     // minMediatorXS = 0.201277;
                maxMediatorXS = 0;     // maxMediatorXS = 0.201277;
            } else if(mediatorMass==1200){
                MediatorXS = 0.00440078;
                minMediatorXS = 0;	// minMediatorXS = 0.317987;
                maxMediatorXS = 0;	// maxMediatorXS = 0.317987;
            }
            TString inputDir = "/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/";
            TFile *fTrig = TFile::Open(inputDir + TString(argv[1]) + "/outputMSTrigEff.root");
            TFile *fMSVx = TFile::Open(inputDir + TString(argv[1]) + "/outputMSVxEff.root");
            
            //////////////////////////////
            //* Histograms declaration *//
            TFile * outputFile = new TFile(inputDir+"/extrapolation/"+TString(argv[3]),"RECREATE");
            std::cout << "output ROOT file " << output << std::endl;
            
            TDirectory *d_Event = outputFile->mkdir("EventCharacteristics");
            TDirectory *d_DPos = outputFile->mkdir("DecayPositions");
            TDirectory *d_DposCoinc = outputFile->mkdir("DecayPositionCoincidence");
            TDirectory *d_Expected = outputFile->mkdir("ExpectedEvents");
            TDirectory *d_Expectedb = outputFile->mkdir("StatUncertainties");
            
            /////////////////////////
            //* v-pion kinematics *//
            
            h_eta = new TH1F("eta","",250,-5,5);
            h_eta->SetDirectory(d_Event);
            h_beta = new TH1F("beta","",100,0,1);
            h_beta->SetDirectory(d_Event);
            
            h_barrel_beta = new TH1F("barrel_beta","",100,0,1);
            h_barrel_beta->SetDirectory(d_Event);
            h_endcap_beta = new TH1F("endcap_beta","",100,0,1);
            h_endcap_beta->SetDirectory(d_Event);
            
            h_gamma = new TH1F("gamma","",200,0,20);
            h_gamma->SetDirectory(d_Event);
            
            h_barrel_gamma = new TH1F("barrel_gamma","",200,0,20);
            h_barrel_gamma->SetDirectory(d_Event);
            h_endcap_gamma = new TH1F("endcap_gamma","",200,0,20);
            h_endcap_gamma->SetDirectory(d_Event);
            
            h_barrel_gamma_50 = new TH1F("barrel_gamma_50","",200,0,20);
            h_barrel_gamma_50->SetDirectory(d_Event);
            h_endcap_gamma_50 = new TH1F("endcap_gamma_50","",200,0,20);
            h_endcap_gamma_50->SetDirectory(d_Event);
            
            h_barrel_gamma_025 = new TH1F("barrel_gamma_025","",200,0,20);
            h_barrel_gamma_025->SetDirectory(d_Event);
            h_endcap_gamma_025 = new TH1F("endcap_gamma_025","",200,0,20);
            h_endcap_gamma_025->SetDirectory(d_Event);
            
            h_barrel_gamma = new TH1F("barrel_gamma","",200,0,20);
            h_barrel_gamma->SetDirectory(d_Event);
            h_endcap_gamma = new TH1F("endcap_gamma","",200,0,20);
            h_endcap_gamma->SetDirectory(d_Event);
            
            h_vpion_dphi = new TH1F("vpion_dphi","",100,-3.14,3.14);
            h_vpion_dphi->SetDirectory(d_Event);
            h_vpion_deta = new TH1F("vpion_deta","",100,0,10);
            h_vpion_deta->SetDirectory(d_Event);
            h_vpion_dR = new TH1F("vpion_dR","",100,0,10);
            h_vpion_dR->SetDirectory(d_Event);
            
            h_vpion_signal_dphi = new TH1F("vpion_signal_dphi","",100,-3.14,3.14);
            h_vpion_signal_dphi->SetDirectory(d_Event);
            h_vpion_signal_deta = new TH1F("vpion_signal_deta","",100,0,10);
            h_vpion_signal_deta->SetDirectory(d_Event);
            h_vpion_signal_dR = new TH1F("vpion_signal_dR","",100,0,10);
            h_vpion_signal_dR->SetDirectory(d_Event);
            
            h_hmass = new TH1F("hmass","",160,80,160);
            h_hmass->SetDirectory(d_Event);
            h_vpimass = new TH1F("vpimass","",100,0,50);
            h_vpimass->SetDirectory(d_Event);
            h_hpt = new TH1F("hpt","",100,0,100);
            h_hpt->SetDirectory(d_Event);
            h_hpz = new TH1F("hpz","",100,0,500);
            h_hpz->SetDirectory(d_Event);
            
            cout << "done kinematics histos " << endl;
            
            //////////////////////////////////////////////////////////////////////////
            //* Expected number of events selected by the trigger *//
            
            h_Expected_Trig = new TH1F("Expected_Trig","",1200, xBinValues);
            h_Expected_Trig_max = new TH1F("Expected_Trig_max","",1200, xBinValues);
            h_Expected_Trig_min = new TH1F("Expected_Trig_min","",1200, xBinValues);
            h_Expected_Trig->SetDirectory(d_Expected);
            h_Expected_Trig_max->SetDirectory(d_Expected);
            h_Expected_Trig_min->SetDirectory(d_Expected);
            
            h_Expected_BBTrig = new TH1F("Expected_BBTrig","",1200, xBinValues);
            h_Expected_BBTrig_maxStatTotal = new TH1F("Expected_BBTrig_maxStatTotal","",1200, xBinValues);
            h_Expected_BBTrig_maxStatTotal->SetDirectory(d_Expected);
            h_Expected_BBTrig_minStatTotal = new TH1F("Expected_BBTrig_minStatTotal","",1200, xBinValues);
            h_Expected_BBTrig_minStatTotal->SetDirectory(d_Expected);
            h_Expected_BBTrig_maxSyst = new TH1F("Expected_BBTrig_maxSyst","",1200, xBinValues);
            h_Expected_BBTrig_minSyst = new TH1F("Expected_BBTrig_minSyst","",1200, xBinValues);
            h_Expected_BBTrig->SetDirectory(d_Expected);
            
            h_Expected_BBTrig_maxSyst->SetDirectory(d_Expected);
            h_Expected_BBTrig_minSyst->SetDirectory(d_Expected);
            
            h_Expected_BETrig = new TH1F("Expected_BETrig","",1200, xBinValues);
            h_Expected_BETrig_maxStatTotal = new TH1F("Expected_BETrig_maxStatTotal","",1200, xBinValues);
            h_Expected_BETrig_maxStatTotal->SetDirectory(d_Expected);
            h_Expected_BETrig_minStatTotal = new TH1F("Expected_BETrig_minStatTotal","",1200, xBinValues);
            h_Expected_BETrig_minStatTotal->SetDirectory(d_Expected);
            h_Expected_BETrig_maxSyst = new TH1F("Expected_BETrig_maxSyst","",1200, xBinValues);
            h_Expected_BETrig_minSyst = new TH1F("Expected_BETrig_minSyst","",1200, xBinValues);
            h_Expected_BETrig->SetDirectory(d_Expected);
            h_Expected_BETrig_maxSyst->SetDirectory(d_Expected);
            h_Expected_BETrig_minSyst->SetDirectory(d_Expected);
            
            h_Expected_EETrig = new TH1F("Expected_EETrig","",1200, xBinValues);
            h_Expected_EETrig_maxStatTotal = new TH1F("Expected_EETrig_maxStatTotal","",1200, xBinValues);
            h_Expected_EETrig_maxStatTotal->SetDirectory(d_Expected);
            h_Expected_EETrig_minStatTotal = new TH1F("Expected_EETrig_minStatTotal","",1200, xBinValues);
            h_Expected_EETrig_minStatTotal->SetDirectory(d_Expected);
            h_Expected_EETrig_maxSyst = new TH1F("Expected_EETrig_maxSyst","",1200, xBinValues);
            h_Expected_EETrig_minSyst = new TH1F("Expected_EETrig_minSyst","",1200, xBinValues);
            h_Expected_EETrig->SetDirectory(d_Expected);
            h_Expected_EETrig_maxSyst->SetDirectory(d_Expected);
            h_Expected_EETrig_minSyst->SetDirectory(d_Expected);
            
            cout << "done trigger histos " << endl;
            /////////////////////////////////////////////////////////////////////////////
            //* Expected number of events with at least two reconstructed MS vertices *//
            
            h_Expected_2MSVx = new TH1F("Expected_2MSVx","",1200, xBinValues);
            
            h_Expected_2MSVx->SetDirectory(d_Expected);
            
            ////////////////////////////////////////////////////////////////////////////////
            //* Expected number of events with at least two reconstructed vertices MS+ID *//
            
            h_Expected_EEMSVx = new TH1F("Expected_EEMSVx","",1200, xBinValues);
            h_Expected_EEMSVx->SetDirectory(d_Expected);
            h_Expected_EEMSVx_maxTotal = new TH1F("Expected_EEMSVx_maxTotal","",1200, xBinValues);
            h_Expected_EEMSVx_minTotal = new TH1F("Expected_EEMSVx_minTotal","",1200, xBinValues);
            h_Expected_EEMSVx_maxTotal->SetDirectory(d_Expected);
            h_Expected_EEMSVx_minTotal->SetDirectory(d_Expected);
            h_Expected_EEMSVx_maxTrigSyst = new TH1F("Expected_EEMSVx_maxTrigSyst","",1200, xBinValues);
            h_Expected_EEMSVx_minTrigSyst = new TH1F("Expected_EEMSVx_minTrigSyst","",1200, xBinValues);
            h_Expected_EEMSVx_maxTrigSyst->SetDirectory(d_Expected);
            h_Expected_EEMSVx_minTrigSyst->SetDirectory(d_Expected);
            h_Expected_EEMSVx_maxTrigStatTotal = new TH1F("Expected_EEMSVx_maxTrigStatTotal","",1200, xBinValues);
            h_Expected_EEMSVx_minTrigStatTotal = new TH1F("Expected_EEMSVx_minTrigStatTotal","",1200, xBinValues);
            h_Expected_EEMSVx_maxTrigStatTotal->SetDirectory(d_Expected);
            h_Expected_EEMSVx_minTrigStatTotal->SetDirectory(d_Expected);
            h_Expected_EEMSVx_maxEVXSyst = new TH1F("Expected_EEMSVx_maxEVXSyst","",1200, xBinValues);
            h_Expected_EEMSVx_minEVXSyst = new TH1F("Expected_EEMSVx_minEVXSyst","",1200, xBinValues);
            h_Expected_EEMSVx_maxEVXSyst->SetDirectory(d_Expected);
            h_Expected_EEMSVx_minEVXSyst->SetDirectory(d_Expected);
            h_Expected_EEMSVx_maxEVXStatTotal = new TH1F("Expected_EEMSVx_maxEVXStatTotal","",1200, xBinValues);
            h_Expected_EEMSVx_minEVXStatTotal = new TH1F("Expected_EEMSVx_minEVXStatTotal","",1200, xBinValues);
            h_Expected_EEMSVx_maxEVXStatTotal->SetDirectory(d_Expected);
            h_Expected_EEMSVx_minEVXStatTotal->SetDirectory(d_Expected);
            cout << "done EE histos " << endl;
            
            h_Expected_BBMSVx = new TH1F("Expected_BBMSVx","",1200, xBinValues);
            h_Expected_BBMSVx->SetDirectory(d_Expected);
            h_Expected_BBMSVx_maxTotal = new TH1F("Expected_BBMSVx_maxTotal","",1200, xBinValues);
            h_Expected_BBMSVx_minTotal = new TH1F("Expected_BBMSVx_minTotal","",1200, xBinValues);
            h_Expected_BBMSVx_maxTotal->SetDirectory(d_Expected);
            h_Expected_BBMSVx_minTotal->SetDirectory(d_Expected);
            h_Expected_BBMSVx_maxTrigSyst = new TH1F("Expected_BBMSVx_maxTrigSyst","",1200, xBinValues);
            h_Expected_BBMSVx_minTrigSyst = new TH1F("Expected_BBMSVx_minTrigSyst","",1200, xBinValues);
            h_Expected_BBMSVx_maxTrigSyst->SetDirectory(d_Expected);
            h_Expected_BBMSVx_minTrigSyst->SetDirectory(d_Expected);
            h_Expected_BBMSVx_maxTrigStatTotal = new TH1F("Expected_BBMSVx_maxTrigStatTotal","",1200, xBinValues);
            h_Expected_BBMSVx_minTrigStatTotal = new TH1F("Expected_BBMSVx_minTrigStatTotal","",1200, xBinValues);
            h_Expected_BBMSVx_maxTrigStatTotal->SetDirectory(d_Expected);
            h_Expected_BBMSVx_minTrigStatTotal->SetDirectory(d_Expected);
            h_Expected_BBMSVx_maxBVXSyst = new TH1F("Expected_BBMSVx_maxBVXSyst","",1200, xBinValues);
            h_Expected_BBMSVx_minBVXSyst = new TH1F("Expected_BBMSVx_minBVXSyst","",1200, xBinValues);
            h_Expected_BBMSVx_maxBVXSyst->SetDirectory(d_Expected);
            h_Expected_BBMSVx_minBVXSyst->SetDirectory(d_Expected);
            h_Expected_BBMSVx_maxBVXStatTotal = new TH1F("Expected_BBMSVx_maxBVXStatTotal","",1200, xBinValues);
            h_Expected_BBMSVx_minBVXStatTotal = new TH1F("Expected_BBMSVx_minBVXStatTotal","",1200, xBinValues);
            h_Expected_BBMSVx_maxBVXStatTotal->SetDirectory(d_Expected);
            h_Expected_BBMSVx_minBVXStatTotal->SetDirectory(d_Expected);
            
            cout << "done BB histos " << endl;
            
            h_Expected_BEMSVx = new TH1F("Expected_BEMSVx","",1200, xBinValues);
            h_Expected_BEMSVx->SetDirectory(d_Expected);
            
            h_Expected_BEMSVx_maxTotal = new TH1F("Expected_BEMSVx_maxTotal","",1200, xBinValues);
            h_Expected_BEMSVx_minTotal = new TH1F("Expected_BEMSVx_minTotal","",1200, xBinValues);
            h_Expected_BEMSVx_maxTotal->SetDirectory(d_Expected);
            h_Expected_BEMSVx_minTotal->SetDirectory(d_Expected);
            
            h_Expected_BEMSVx_maxTrigSyst = new TH1F("Expected_BEMSVx_maxTrigSyst","",1200, xBinValues);
            h_Expected_BEMSVx_minTrigSyst = new TH1F("Expected_BEMSVx_minTrigSyst","",1200, xBinValues);
            h_Expected_BEMSVx_maxTrigSyst->SetDirectory(d_Expected);
            h_Expected_BEMSVx_minTrigSyst->SetDirectory(d_Expected);
            
            h_Expected_BEMSVx_maxTrigStatTotal = new TH1F("Expected_BEMSVx_maxTrigStatTotal","",1200, xBinValues);
            h_Expected_BEMSVx_minTrigStatTotal = new TH1F("Expected_BEMSVx_minTrigStatTotal","",1200, xBinValues);
            h_Expected_BEMSVx_maxTrigStatTotal->SetDirectory(d_Expected);
            h_Expected_BEMSVx_minTrigStatTotal->SetDirectory(d_Expected);
            
            h_Expected_BEMSVx_maxEBVXSyst = new TH1F("Expected_BEMSVx_maxEBVXSyst","",1200, xBinValues);
            h_Expected_BEMSVx_minEBVXSyst = new TH1F("Expected_BEMSVx_minEBVXSyst","",1200, xBinValues);
            h_Expected_BEMSVx_maxEBVXSyst->SetDirectory(d_Expected);
            h_Expected_BEMSVx_minEBVXSyst->SetDirectory(d_Expected);
            
            h_Expected_BEMSVx_maxEBVXStatTotal = new TH1F("Expected_BEMSVx_maxEBVXStatTotal","",1200, xBinValues);
            h_Expected_BEMSVx_minEBVXStatTotal = new TH1F("Expected_BEMSVx_minEBVXStatTotal","",1200, xBinValues);
            h_Expected_BEMSVx_maxEBVXStatTotal->SetDirectory(d_Expected);
            h_Expected_BEMSVx_minEBVXStatTotal->SetDirectory(d_Expected);
            
            
            h_Expected_BEMSVx_maxBEVXSyst = new TH1F("Expected_BEMSVx_maxBEVXSyst","",1200, xBinValues);
            h_Expected_BEMSVx_minBEVXSyst = new TH1F("Expected_BEMSVx_minBEVXSyst","",1200, xBinValues);
            h_Expected_BEMSVx_maxBEVXSyst->SetDirectory(d_Expected);
            h_Expected_BEMSVx_minBEVXSyst->SetDirectory(d_Expected);
            
            h_Expected_BEMSVx_maxBEVXStatTotal = new TH1F("Expected_BEMSVx_maxBEVXStatTotal","",1200, xBinValues);
            h_Expected_BEMSVx_minBEVXStatTotal = new TH1F("Expected_BEMSVx_minBEVXStatTotal","",1200, xBinValues);
            h_Expected_BEMSVx_maxBEVXStatTotal->SetDirectory(d_Expected);
            h_Expected_BEMSVx_minBEVXStatTotal->SetDirectory(d_Expected);
            
            
            cout << "done ms ms histos" << endl;
            ////////////////////////////////////////////////////////////////////////////////
            //* Expected number of events with at least one reconstructed ID vertex *//
            
            h_Expected_Tot2Vx = new TH1F("Expected_Tot2Vx","",1200, xBinValues);
            h_Expected_Tot2Vx_maxTotal = new TH1F("Expected_Tot2Vx_maxTotal","",1200, xBinValues);
            h_Expected_Tot2Vx_minTotal = new TH1F("Expected_Tot2Vx_minTotal","",1200, xBinValues);
            h_Expected_Tot2Vx->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_maxTotal->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_minTotal->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_maxTotalLin = new TH1F("Expected_Tot2Vx_maxTotalLin","",1200, xBinValues);
            h_Expected_Tot2Vx_minTotalLin = new TH1F("Expected_Tot2Vx_minTotalLin","",1200, xBinValues);
            h_Expected_Tot2Vx_maxTotalLin->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_minTotalLin->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_maxStatTotal = new TH1F("Expected_Tot2Vx_maxStatTotal","",1200, xBinValues);
            h_Expected_Tot2Vx_minStatTotal = new TH1F("Expected_Tot2Vx_minStatTotal","",1200, xBinValues);
            h_Expected_Tot2Vx_maxStatTotal->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_minStatTotal->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_maxSystTotal = new TH1F("Expected_Tot2Vx_maxSystTotal","",1200, xBinValues);
            h_Expected_Tot2Vx_minSystTotal = new TH1F("Expected_Tot2Vx_minSystTotal","",1200, xBinValues);
            h_Expected_Tot2Vx_maxSystTotal->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_minSystTotal->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_maxSystLinear = new TH1F("Expected_Tot2Vx_maxSystLinear","",1200, xBinValues);
            h_Expected_Tot2Vx_minSystLinear = new TH1F("Expected_Tot2Vx_minSystLinear","",1200, xBinValues);
            h_Expected_Tot2Vx_maxSystLinear->SetDirectory(d_Expected);
            h_Expected_Tot2Vx_minSystLinear->SetDirectory(d_Expected);
            
            cout << "done 2vx histo" << endl;
            ///////////////////////////////////////////////////////
            //* Get the trigger and reconstruction efficiencies *//
            
            
            h_BBTrigeff     = (TH2D*)fTrig->Get("RoICluster/RoICluster_BB_noscale");
            h_BBTrignorm    = (TH2D*)fTrig->Get("RoICluster/RoICluster_BB_Norm");
            
            h_EETrigeff     = (TH2D*)fTrig->Get("RoICluster/RoICluster_EE_Eff");
            h_EETrignorm    = (TH2F*)fTrig->Get("RoICluster/RoICluster_EE_Norm");
            
            h_BETrigeff     = (TH2F*)fTrig->Get("RoICluster/RoICluster_BE_Eff");
            h_BETrignorm    = (TH2F*)fTrig->Get("RoICluster/RoICluster_BE_Norm");
            
            
            h_BBVxpass   = (TH1F*)fMSVx->Get("MSvx/MSvx_Barrel_Eff_BB_EvTrig_noscale");
            h_BBVxnorm   = (TH1F*)fMSVx->Get("Normalization/llp_barrel_decays_BB_EvTrig_noscale");
            h_EEVxpass   = (TH1F*)fMSVx->Get("MSvx/MSvx_Endcap_Eff_EE_EvTrig");
            h_EEVxnorm  = (TH1F*)fMSVx->Get("Normalization/llp_endcap_decays_EE_EvTrig");
            h_BEVxpass   = (TH1F*)fMSVx->Get("MSvx/MSvx_Barrel_Eff_BE_EvTrig");
            h_BEVxnorm  = (TH1F*)fMSVx->Get("Normalization/llp_barrel_decays_BE_EvTrig");
            h_EBVxpass   = (TH1F*)fMSVx->Get("MSvx/MSvx_Endcap_Eff_EB_EvTrig");
            h_EBVxnorm  = (TH1F*)fMSVx->Get("Normalization/llp_endcap_decays_BE_EvTrig");
            
            /*	h_BBVxeff   = (TH1F*)f_effFile->Get("MSvx/MSvx_B_1MS_Eff_Trig");
             h_EEVxeff   = (TH1F*)f_effFile->Get("MSvx/MSvx_E_1MS_Eff_Trig");
             h_BEVxeff   = (TH1F*)f_effFile->Get("MSvx/MSvx_B_1MS_Eff_Trig");
             h_EBVxeff   = (TH1F*)f_effFile->Get("MSvx/MSvx_E_1MS_Eff_Trig");
             */
            
            if (lDebug) {
                cout << "==>> Pointers to efficiency histograms <<==" << endl;
                cout << "     Trigger_Barrel_Eff "     << h_BTrigpass << endl;
                cout << "     Trigger_Endcap_Eff "     << h_ETrigpass << endl;
                cout << "     Trigger_BB_Eff "     << h_BBTrigeff << endl;
                cout << "     Trigger_BE_Eff "     << h_BETrigeff << endl;
                cout << "     Trigger_EE_Eff "     << h_EETrigeff << endl;
                cout << "     MSvx_Barrel_Eff_Trig "   << h_BTrigVxpass << endl;
                cout << "     MSvx_Endcap_Eff_Trig "   << h_ETrigVxpass << endl;
                cout << "     MSvx_BB_Trig "   << h_BBVxpass << ", " << h_BBVxnorm << endl;
                cout << "     MSvx_BE_Trig "   << h_BEVxpass << ", " << h_BEVxnorm << endl;
                cout << "     MSvx_EB_Trig "   << h_EBVxpass << ", " << h_EBVxnorm << endl;
                cout << "     MSvx_EE_Trig "   << h_EEVxpass << ", " << h_EEVxnorm << endl;
                
                cout << "" << endl;
            }
            
            bool REBIN_TRIG(true);
            cout << "rebin? " << REBIN_TRIG << endl;
            if(REBIN_TRIG){
                h_BBTrigeff->Rebin2D(2,2);
                h_BETrigeff->Rebin2D(2,2);
                h_EETrigeff->Rebin2D(2,2);
                h_BBTrignorm->Rebin2D(2,2);
                h_BETrignorm->Rebin2D(2,2);
                h_EETrignorm->Rebin2D(2,2);
            }
            cout << "rebinned 2D efficiencies?" << REBIN_1D << endl;
            if(REBIN_1D){
                h_BTrigpass->Rebin(2);
                h_BTrignorm->Rebin(2);
                h_ETrigpass->Rebin(2);
                h_ETrignorm->Rebin(2);
                h_BBVxpass->Rebin(2);
                h_BBVxnorm->Rebin(2);
                h_BEVxpass->Rebin(2);
                h_BEVxnorm->Rebin(2);
                h_EEVxpass->Rebin(2);
                h_EEVxnorm->Rebin(2);
                h_EBVxpass->Rebin(2);
                h_EBVxnorm->Rebin(2);
                h_BTrigVxpass->Rebin(2);
                h_ETrigVxpass->Rebin(2);
                cout << "done rebinning" << endl;
            }
            
            
            h_BTrigeff = (TH1F*)h_BTrigpass->Clone();
            h_ETrigeff = (TH1F*)h_ETrigpass->Clone();
            h_BTrigVxeff = (TH1F*)h_BTrigVxpass->Clone();
            h_ETrigVxeff = (TH1F*)h_ETrigVxpass->Clone();
            h_BBVxeff = (TH1F*)h_BBVxpass->Clone();
            h_BEVxeff =(TH1F*) h_BEVxpass->Clone();
            h_EBVxeff = (TH1F*)h_EBVxpass->Clone();
            h_EEVxeff = (TH1F*)h_EEVxpass->Clone();
            
            cout << "done cloning" << endl;
            cout << h_ETrigeff->GetBinContent(14) << ", " << h_ETrigVxeff->GetBinContent(14) << endl;
            cout << h_ETrigpass->GetBinContent(14) << ", " << h_ETrigVxpass->GetBinContent(14) << endl;
            cout << h_ETrignorm->GetBinContent(14) << endl;
            
            h_BTrigeff->Divide(h_BTrigeff,h_BTrignorm,1.0,1.0,"B");
            h_ETrigeff->Divide(h_ETrigeff,h_ETrignorm,1.0,1.0,"B");
            
            h_BTrigVxeff->Divide(h_BTrigVxpass,h_BTrigpass,1.0,1.0,"B");
            h_ETrigVxeff->Divide(h_ETrigVxpass,h_ETrigpass,1.0,1.0,"B");
            
            h_BBVxeff->Divide(h_BBVxpass,h_BBVxnorm,1.0,1.0,"B");
            h_BEVxeff->Divide(h_BEVxpass,h_BEVxnorm,1.0,1.0,"B");
            h_EBVxeff->Divide(h_EBVxpass,h_EBVxnorm,1.0,1.0,"B");
            h_EEVxeff->Divide(h_EEVxpass,h_EEVxnorm,1.0,1.0,"B");
            
            cout << "done making efficiencies" << endl;
            cout << h_ETrigeff->GetBinContent(14) << ", " << h_ETrigVxeff->GetBinContent(14) << endl;
            
            nBTrigbins     = h_BTrigeff->GetNbinsX();
            nETrigbins     = h_ETrigeff->GetNbinsX();
            
            nBBTrigbins     = h_BBTrigeff->GetNbinsX();
            nBETrigbinsX    = h_BETrigeff->GetNbinsX();
            nBETrigbinsY    = h_BETrigeff->GetNbinsY();
            nEETrigbins     = h_EETrigeff->GetNbinsX();
            
            if(nBBTrigbins != h_BBTrignorm->GetNbinsX()){
                cout << "ERROR, not same # bins in pass/norm histos!" << endl;
            }
            
            nBTrigVXbins   = h_BTrigVxeff->GetNbinsX();
            nETrigVXbins   = h_ETrigVxeff->GetNbinsX();
            
            nIDVXbins      = h_IDVxeff->GetNbinsX();
            nBIDVXbins     = h_BIDVxeff->GetNbinsX();
            nEIDVXbins     = h_EIDVxeff->GetNbinsX();
            
            nBBVXbins      = h_BBVxeff->GetNbinsX();
            nBEVXbins      = h_BEVxeff->GetNbinsX();
            nEBVXbins      = h_EBVxeff->GetNbinsX();
            nEEVXbins      = h_EEVxeff->GetNbinsX();
            
            if (lDebug) {
                cout << "==>> Number of bins in efficiency histograms <<==" << endl;
                cout << "     Trigger_Barrel_Eff "     << nBTrigbins << endl;
                cout << "     Trigger_BB_Eff "     << nBBTrigbins << endl;
                cout << "     Trigger_BE_Eff (x) "     << nBETrigbinsX << endl;
                cout << "     Trigger_BE_Eff (y) "     << nBETrigbinsY << endl;
                cout << "     Trigger_EE_Eff "     << nEETrigbins << endl;
                cout << "     Trigger_Endcap_Eff "     << nETrigbins << endl;
                cout << "     MSvx_Barrel_Eff_Trig "   << nBTrigVXbins << endl;
                cout << "     MSvx_Endcap_Eff_Trig "   << nETrigVXbins << endl;
                cout << "     IDvx_RecoVtx_Matched "   << nIDVXbins << endl;
                cout << "     IDvx_RecoVtx_Barrel_Matched "   << nBIDVXbins << endl;
                cout << "     IDvx_RecoVtx_Endcap_Matched "   << nEIDVXbins << endl;
                cout << "     MSvx_Barrel_Eff_BB " << nBBVXbins << endl;
                cout << "     MSvx_Barrel_Eff_BE " << nBEVXbins << endl;
                cout << "     MSvx_Endcap_Eff_EB " << nEBVXbins << endl;
                cout << "     MSvx_Endcap_Eff_EE " << nEEVXbins << endl;
                cout << "" << endl;
            }
            
            BMSstepTRIG    = (h_BBTrigeff->GetBinCenter(h_BBTrigeff->GetNbinsX())+h_BBTrigeff->GetBinWidth(0)/2)*1000/(h_BBTrigeff->GetNbinsX());
            EMSstepTRIG    = (h_EETrigeff->GetBinCenter(h_EETrigeff->GetNbinsX())+h_EETrigeff->GetBinWidth(0)/2)*1000/(h_EETrigeff->GetNbinsX());
            BEMSstepTRIG_X = (h_BETrigeff->GetXaxis()->GetBinCenter(h_BETrigeff->GetNbinsX())+h_BETrigeff->GetXaxis()->GetBinWidth(0)/2)*1000/(h_BETrigeff->GetNbinsX());
            BEMSstepTRIG_Y = (h_BETrigeff->GetYaxis()->GetBinCenter(h_BETrigeff->GetNbinsY())+h_BETrigeff->GetYaxis()->GetBinWidth(0)/2)*1000/(h_BETrigeff->GetNbinsY());
            
            BBMSstep = (h_BBVxeff->GetBinCenter(h_BBVxeff->GetNbinsX())+h_BBVxeff->GetBinWidth(0)/2)*1000/(h_BBVxeff->GetNbinsX());
            BEMSstep = (h_BEVxeff->GetBinCenter(h_BEVxeff->GetNbinsX())+h_BEVxeff->GetBinWidth(0)/2)*1000/(h_BEVxeff->GetNbinsX());
            EBMSstep = (h_EBVxeff->GetBinCenter(h_EBVxeff->GetNbinsX())+h_EBVxeff->GetBinWidth(0)/2)*1000/(h_EBVxeff->GetNbinsX());
            EEMSstep = (h_EEVxeff->GetBinCenter(h_EEVxeff->GetNbinsX())+h_EEVxeff->GetBinWidth(0)/2)*1000/(h_EEVxeff->GetNbinsX());
            
            BMSstep  = (h_BTrigVxeff->GetBinCenter(h_BTrigVxeff->GetNbinsX())+h_BTrigVxeff->GetBinWidth(0)/2)*1000/(h_BTrigVxeff->GetNbinsX());
            EMSstep  = (h_ETrigVxeff->GetBinCenter(h_ETrigVxeff->GetNbinsX())+h_ETrigVxeff->GetBinWidth(0)/2)*1000/(h_ETrigVxeff->GetNbinsX());
            IDstep  = (h_IDVxeff->GetBinCenter(h_IDVxeff->GetNbinsX())+h_IDVxeff->GetBinWidth(0)/2)*1000/(h_IDVxeff->GetNbinsX());
            BIDstep  = (h_BIDVxeff->GetBinCenter(h_BIDVxeff->GetNbinsX())+h_BIDVxeff->GetBinWidth(0)/2)*1000/(h_BIDVxeff->GetNbinsX());
            EIDstep  = (h_EIDVxeff->GetBinCenter(h_EIDVxeff->GetNbinsX())+h_EIDVxeff->GetBinWidth(0)/2)*1000/(h_EIDVxeff->GetNbinsX());
            
            if (lDebug) {
                cout << "==>> Step for the efficiency histograms <<==" << endl;
                cout << "     Barrel MS Trig BB" << BMSstepTRIG << endl;
                cout << "     Endcap MS Trig EE" << EMSstepTRIG << endl;
                cout << "     Endcap MS Trig BE" << BEMSstepTRIG_X << endl;
                cout << "     Barrel MS Trig BE" << BEMSstepTRIG_Y << endl;
                
                
                cout << "     Barrel MS " << BMSstep << endl;
                cout << "     Endcap MS " << EMSstep << endl;
                cout << "     Mixed ID " << IDstep << endl;
                cout << "     Barrel ID " << BIDstep << endl;
                cout << "     Endcap ID " << EIDstep << endl;
                cout << "" << endl;
            }
            
            if( nBTrigbins>100     ||
               nETrigbins>100     ||
               nBTrigVXbins>100   ||
               nETrigVXbins>100   ||
               nBNoTrigVXbins>100 ||
               nENoTrigVXbins>100 ||
               nBIDVXbins>100     ||
               nIDVXbins>100      ||
               nBBTrigbins>100    ||
               nEETrigbins>100    ||
               nBETrigbinsX>100   ||
               nBETrigbinsY>100   ||
               nBBVXbins>100      ||
               nBEVXbins>100      ||
               nEBVXbins>100      ||
               nEEVXbins>100 )
                cout << "ERROR! Not considering some bins in the efficiency histograms" << endl;
            
            
            for(int i=0; i<100; ++i) {
                
                if(i<nBTrigbins && h_BTrignorm->GetBinContent(i+1) > 10 && (h_BTrigeff->GetBinContent(i+1) > 0)) {
                    double bineff = h_BTrigeff->GetBinContent(i+1);
                    double binerr = h_BTrigeff->GetBinError(i+1);
                    double btrigerr = sqrt(sq(binerr/bineff));
                    BarTrigProb[i] = bineff;
                    BarTrigProb_maxStat[i] = (1+btrigerr)*bineff;
                    BarTrigProb_minStat[i] = (1-btrigerr)*bineff;
                    BarTrigProb_maxSyst[i] = (1+BROISYSTERR)*bineff;
                    BarTrigProb_minSyst[i] = (1-BROISYSTERR)*bineff;
                    //cout << h_BTrigeff->GetBinContent(i+1) << " in btrigeff bin " << i << endl;
                } else {
                    BarTrigProb[i] = 0;
                    BarTrigProb_maxSyst[i] = 0;
                    BarTrigProb_minSyst[i] = 0;
                    BarTrigProb_maxStat[i] = 0;
                    BarTrigProb_minStat[i] = 0;
                }
                if(i<nETrigbins && h_ETrignorm->GetBinContent(i+1) > 10 && (h_ETrigeff->GetBinContent(i+1) > 0)) {
                    double bineff = h_ETrigeff->GetBinContent(i+1);
                    double binerr = h_ETrigeff->GetBinError(i+1);
                    double etrigerr = sqrt(sq(binerr/h_ETrigeff->GetBinContent(i+1)));
                    EndTrigProb[i] = bineff;
                    EndTrigProb_maxStat[i] = (1+etrigerr)*bineff;
                    EndTrigProb_minStat[i] = (1-etrigerr)*bineff;
                    EndTrigProb_maxSyst[i] = (1+EROISYSTERR)*bineff;
                    EndTrigProb_minSyst[i] = (1-EROISYSTERR)*bineff;
                    //cout << h_ETrigeff->GetBinContent(i+1) << " in etrigeff bin " << i << endl;
                } else {
                    EndTrigProb[i] = 0;
                    EndTrigProb_maxStat[i] = 0;
                    EndTrigProb_minStat[i] = 0;
                    EndTrigProb_maxSyst[i] = 0;
                    EndTrigProb_minSyst[i] = 0;
                }
                if(i<nBTrigVXbins && h_BTrigpass->GetBinContent(i+1)>10 && (h_BTrigVxeff->GetBinContent(i+1)>0) ) {
                    double bineff = h_BTrigVxeff->GetBinContent(i+1);
                    double binerr = h_BTrigVxeff->GetBinError(i+1);
                    double btrigvxerr = sqrt(sq(binerr/h_BTrigVxeff->GetBinContent(i+1)));
                    BarTrigMSVxProb[i] = bineff;
                    BarTrigMSVxProb_maxStat[i] = (1+btrigvxerr)*bineff;
                    BarTrigMSVxProb_minStat[i] = (1-btrigvxerr)*bineff;
                    BarTrigMSVxProb_maxSyst[i] = (1+BMSVXSYSTERR)*bineff;
                    BarTrigMSVxProb_minSyst[i] = (1-BMSVXSYSTERR)*bineff;
                } else {
                    BarTrigMSVxProb[i] = 0;
                    BarTrigMSVxProb_maxStat[i] = 0;
                    BarTrigMSVxProb_minStat[i] = 0;
                    BarTrigMSVxProb_maxSyst[i] = 0;
                    BarTrigMSVxProb_minSyst[i] = 0;
                }
                if(i<nBBVXbins && h_BBVxnorm->GetBinContent(i+1) > 10 && (h_BBVxeff->GetBinContent(i+1) > 0)) {
                    double bineff = h_BBVxeff->GetBinContent(i+1);
                    double binerr = h_BBVxeff->GetBinError(i+1);
                    double bbvxerr = sqrt(sq(binerr/h_BBVxeff->GetBinContent(i+1)));
                    BBMSVxProb[i] = bineff;
                    
                    BBMSVxProb_maxStat[i] = (1+bbvxerr)*bineff;
                    BBMSVxProb_minStat[i] = (1-bbvxerr)*bineff;
                    
                    
                    BBMSVxProb_maxSyst[i] = (1+BMSVXSYSTERR)*bineff;
                    BBMSVxProb_minSyst[i] = (1-BMSVXSYSTERR)*bineff;
                } else {
                    BBMSVxProb[i] = 0;
                    
                    BBMSVxProb_maxStat[i] = 0;
                    BBMSVxProb_minStat[i] = 0;
                    
                    BBMSVxProb_maxSyst[i] = 0;
                    BBMSVxProb_minSyst[i] = 0;
                }
                if(i<nBEVXbins && h_BEVxnorm->GetBinContent(i+1) > 10 && (h_BEVxeff->GetBinContent(i+1) > 0) ) {
                    double bineff = h_BEVxeff->GetBinContent(i+1);
                    double binerr = h_BEVxeff->GetBinError(i+1);
                    double bevxerr = sqrt(sq(binerr/bineff));
                    BEMSVxProb[i] = bineff;
                    BEMSVxProb_maxStat[i] = (1+bevxerr)*bineff;
                    BEMSVxProb_minStat[i] = (1-bevxerr)*bineff;
                    BEMSVxProb_maxSyst[i] = (1+BMSVXSYSTERR)*bineff;
                    BEMSVxProb_minSyst[i] = (1-BMSVXSYSTERR)*bineff;
                } else {
                    BEMSVxProb[i] = 0;
                    BEMSVxProb_maxStat[i] = 0;
                    BEMSVxProb_minStat[i] = 0;
                    BEMSVxProb_maxSyst[i] = 0;
                    BEMSVxProb_minSyst[i] = 0;
                }
                if(i<nEBVXbins && h_EBVxnorm->GetBinContent(i+1) > 10 && (h_EBVxeff->GetBinContent(i+1) > 0) ) {
                    double bineff = h_EBVxeff->GetBinContent(i+1);
                    double binerr = h_EBVxeff->GetBinError(i+1);
                    double ebvxerr = sqrt(sq(binerr/bineff));
                    EBMSVxProb[i] = bineff;
                    EBMSVxProb_maxStat[i] = (1+ebvxerr)*bineff;
                    EBMSVxProb_minStat[i] = (1-ebvxerr)*bineff;
                    EBMSVxProb_maxSyst[i] = (1+EMSVXSYSTERR)*bineff;
                    EBMSVxProb_minSyst[i] = (1-EMSVXSYSTERR)*bineff;
                } else {
                    EBMSVxProb[i] = 0;
                    EBMSVxProb_maxStat[i] = 0;
                    EBMSVxProb_minStat[i] = 0;
                    EBMSVxProb_maxSyst[i] = 0;
                    EBMSVxProb_minSyst[i] = 0;
                }
                if(i<nEEVXbins && h_EEVxnorm->GetBinContent(i+1) > 10 && (h_EEVxeff->GetBinContent(i+1) > 0)) {
                    double bineff = h_EEVxeff->GetBinContent(i+1);
                    double binerr = h_EEVxeff->GetBinError(i+1);
                    double eevxerr = sqrt(sq(binerr/bineff));
                    EEMSVxProb[i] = bineff;
                    
                    EEMSVxProb_maxStat[i] = (1+eevxerr)*bineff;
                    EEMSVxProb_minStat[i] = (1-eevxerr)*bineff;
                    
                    EEMSVxProb_maxSyst[i] = (1+EMSVXSYSTERR)*bineff;
                    EEMSVxProb_minSyst[i] = (1-EMSVXSYSTERR)*bineff;
                } else {
                    EEMSVxProb[i] = 0;
                    EEMSVxProb_maxStat[i] = 0;
                    EEMSVxProb_minStat[i] = 0;
                    
                    EEMSVxProb_maxSyst[i] = 0;
                    EEMSVxProb_minSyst[i] = 0;
                }
                if(i<nETrigVXbins && h_ETrigpass->GetBinContent(i+1) > 10 && (h_ETrigVxeff->GetBinContent(i+1) > 0)) {
                    double bineff = h_ETrigVxeff->GetBinContent(i+1);
                    double binerr = h_ETrigVxeff->GetBinError(i+1);
                    double etrigvxerr = sqrt(sq(binerr/bineff));
                    EndTrigMSVxProb[i] = bineff;
                    EndTrigMSVxProb_maxStat[i] = (1+etrigvxerr)*bineff;
                    EndTrigMSVxProb_minStat[i] = (1-etrigvxerr)*bineff;
                    EndTrigMSVxProb_maxSyst[i] = (1+EMSVXSYSTERR)*bineff;
                    EndTrigMSVxProb_minSyst[i] = (1-EMSVXSYSTERR)*bineff;
                    //cout << h_ETrigVxeff->GetBinContent(i+1) << " in etrigvxeff bin " << i << " max stat " << EndTrigMSVxProb_maxStat[i] << endl;
                } else {
                    EndTrigMSVxProb[i] = 0;
                    EndTrigMSVxProb_maxStat[i] = 0;
                    EndTrigMSVxProb_minStat[i] = 0;
                    EndTrigMSVxProb_maxSyst[i] = 0;
                    EndTrigMSVxProb_minSyst[i] = 0;
                }
                if(i<nBIDVXbins && h_BIDVxpass->GetBinContent(i+1) > 10) {
                    double bineff = h_BIDVxeff->GetBinContent(i+1);
                    double binerr = h_BIDVxeff->GetBinError(i+1);
                    double idvxerr = sqrt(sq(binerr/bineff) );
                    BIDProb[i] = h_BIDVxeff->GetBinContent(i+1);
                } else {
                    BIDProb[i] = 0;
                    
                }
                if(i<nIDVXbins && h_IDVxnorm->GetBinContent(i+1)>10 && (h_IDVxeff->GetBinContent(i+1) > 0) ) {
                    double bineff = h_IDVxeff->GetBinContent(i+1);
                    double binerr = h_IDVxeff->GetBinError(i+1);
                    double idvxerr = sqrt(sq(binerr/bineff) );
                    IDProb[i] = h_IDVxeff->GetBinContent(i+1);
                    IDProb_maxStat[i] = (1+idvxerr)*h_IDVxeff->GetBinContent(i+1);
                    IDProb_minStat[i] = (1-idvxerr)*h_IDVxeff->GetBinContent(i+1);
                    IDProb_maxSyst[i] = (1+IDVXSYSTERR)*h_IDVxeff->GetBinContent(i+1);
                    IDProb_minSyst[i] = (1-IDVXSYSTERR)*h_IDVxeff->GetBinContent(i+1);
                } else {
                    IDProb[i] = 0;
                    IDProb_maxStat[i] = 0;
                    IDProb_minStat[i] = 0;
                    IDProb_maxSyst[i] = 0;
                    IDProb_minSyst[i] = 0;
                }
                if(i<nEIDVXbins && h_EIDVxpass->GetBinContent(i+1) > 10) {
                    double bineff = h_EIDVxeff->GetBinContent(i+1);
                    double binerr = h_EIDVxeff->GetBinError(i+1);
                    double idvxerr = sqrt(sq(binerr/bineff) );
                    EIDProb[i] = h_EIDVxeff->GetBinContent(i+1);
                } else {
                    EIDProb[i] = 0;
                }
                for(int j=0; j<100; j++){
                    if(j < nBBTrigbins && i < nBBTrigbins && h_BBTrignorm->GetBinContent(i+1,j+1) > 10 && (h_BBTrigeff->GetBinContent(i+1,j+1)>0) ) {
                        double passEv = h_BBTrigeff->GetBinContent(i+1,j+1);
                        if(!passEv) continue;
                        double totalEv = h_BBTrignorm->GetBinContent(i+1,j+1);
                        double bbeff = passEv/totalEv;
                        double bberr = sqrt(bbeff*(1.0-bbeff)/totalEv);
                        double bberrup =  sqrt( sq(bberr/bbeff) );
                        double bberrlow = sqrt( sq(bberr/bbeff) );
                        BBTrigProb[i][j] = bbeff;
                        BBTrigProb_maxStat[i][j] = (1+bberrup)*bbeff;
                        BBTrigProb_minStat[i][j] = (1-bberrlow)*bbeff;
                        BBTrigProb_maxSyst[i][j] = (1+BROISYSTERR)*bbeff;
                        BBTrigProb_minSyst[i][j] = (1-BROISYSTERR)*bbeff;
                        //cout << "bb trig eff: " << passEv << "/" << totalEv << "=" << bbeff << ", " << BBTrigProb_maxStat[i][j] <<", " << BBTrigProb_minStat[i][j] << " at " << i << ", " << j << endl;
                    } else {
                        BBTrigProb[i][j] = 0;
                        BBTrigProb_maxStat[i][j] = 0;
                        BBTrigProb_minStat[i][j] = 0;
                        BBTrigProb_maxSyst[i][j] = 0;
                        BBTrigProb_minSyst[i][j] = 0;
                    }
                    if(j < nBETrigbinsY && i < nBETrigbinsX && h_BETrignorm->GetBinContent(i+1,j+1)>10 && (h_BETrigeff->GetBinContent(i+1,j+1) > 0) ) {
                        double passEv = h_BETrigeff->GetBinContent(i+1,j+1);
                        if(!passEv) continue;
                        double totalEv = h_BETrignorm->GetBinContent(i+1,j+1);
                        double beeff = passEv/totalEv;
                        double beerr = sqrt(beeff*(1.0-beeff)/totalEv);
                        double beerrup = sqrt( sq(beerr/beeff) );
                        double beerrlow = sqrt( sq(beerr/beeff) );
                        BETrigProb[i][j] = beeff;
                        BETrigProb_maxStat[i][j] = (1+beerrup)*beeff;
                        BETrigProb_minStat[i][j] = (1-beerrlow)*beeff;
                        BETrigProb_maxSyst[i][j] = (1+BROISYSTERR)*beeff;
                        BETrigProb_minSyst[i][j] = (1-BROISYSTERR)*beeff;
                        cout << "be trig eff: " << passEv << "/" << totalEv << "=" << beeff << ", " << BETrigProb_maxStat[i][j] <<", " << BETrigProb_minStat[i][j] << " at " << i << ", " << j << endl;
                    } else {
                        BETrigProb[i][j] = 0;
                        BETrigProb_maxStat[i][j] = 0;
                        BETrigProb_minStat[i][j] = 0;
                        BETrigProb_maxSyst[i][j] = 0;
                        BETrigProb_minSyst[i][j] = 0;
                    }
                    if(j < nEETrigbins && i < nEETrigbins && h_EETrignorm->GetBinContent(i+1,j+1) > 10 && (h_EETrigeff->GetBinContent(i+1,j+1)>0)) {
                        double passEv = h_EETrigeff->GetBinContent(i+1,j+1);
                        if(!passEv) continue;
                        double totalEv = h_EETrignorm->GetBinContent(i+1,j+1);
                        double eeeff = passEv/totalEv;
                        double eeerr = sqrt(eeeff*(1.0-eeeff)/totalEv);
                        double eeerrup = sqrt( sq(eeerr/eeeff) );
                        double eeerrlow = sqrt( sq(eeerr/eeeff) );
                        EETrigProb[i][j] = eeeff;
                        EETrigProb_maxStat[i][j] = (1+eeerrup)*eeeff;
                        EETrigProb_minStat[i][j] = (1-eeerrlow)*eeeff;
                        EETrigProb_maxSyst[i][j] = (1+EROISYSTERR)*eeeff;
                        EETrigProb_minSyst[i][j] = (1-EROISYSTERR)*eeeff;
                    } else {
                        EETrigProb[i][j] = 0;
                        EETrigProb_maxStat[i][j] = 0;
                        EETrigProb_minStat[i][j] = 0;
                        EETrigProb_maxSyst[i][j] = 0;
                        EETrigProb_minSyst[i][j] = 0;
                    }
                }
            }
        }
        
        nEvents++;
        if(nEvents == 1) rnd.SetSeed((*vpion_e)[0]);
        if(nEvents % 10000 == 0) {
            Long64_t nEntries = fChain->GetEntries();
            timestamp_t t1 = get_timestamp();
            double time1 = (t1 - t0) / 1000000.0L;
            // if(time1 > 0.0000031) cout << "time1 " << time1 << endl;
            cout << "nEvent: " << nEvents << "/" << nEntries  << " at time " << time1 << endl;
            t0 = t1;
        }
        
        double etot(0),pxtot(0),pytot(0),pztot(0);
        double phi1(0),phi2(0);
        vector<TLorentzVector> vpion_p;
        for(unsigned int i=0; i<(*vpion_e).size(); ++i) {
            etot += (*vpion_e)[i];
            pxtot += (*vpion_px)[i];
            pytot += (*vpion_py)[i];
            pztot += (*vpion_pz)[i];
            TLorentzVector tmp_vpi;
            tmp_vpi.SetPxPyPzE((*vpion_px)[i],(*vpion_py)[i],(*vpion_pz)[i],(*vpion_e)[i]);
            vpion_p.push_back(tmp_vpi);
        }
        
        double hmass = sqrt(sq(etot)-(sq(pxtot)+sq(pytot)+sq(pztot)));
        double vpimass = sqrt(sq((*vpion_e)[0])-(sq((*vpion_px)[0])+sq((*vpion_py)[0])+sq((*vpion_pz)[0])));
        
        h_hpt->Fill(sqrt(sq(pxtot)+sq(pytot))/1000.);
        h_hpz->Fill(TMath::Abs(pztot)/1000.);
        h_hmass->Fill(hmass/1000.);
        h_vpimass->Fill(vpimass/1000.);
        
        double deltar_vpi = -99;
        
        //h_llps_in_event->Fill(vpion_p.size());
        
        if(vpion_p.size() > 1){
            deltar_vpi = vpion_p[0].DeltaR(vpion_p[1]);
            h_vpion_dphi->Fill(vpion_p[0].DeltaPhi(vpion_p[1]));
            h_vpion_deta->Fill(TMath::Abs(vpion_p[0].Eta()-vpion_p[1].Eta()));
            h_vpion_dR->Fill(deltar_vpi);
        } else{
            //cout << "there are: " << vpion_p.size() << " llps in this event!" << endl;
            nEventsWithout2LLPs++;
            return kTRUE;
        }
        for(unsigned int i=0; i<vpion_p.size(); ++i) {
            double tmp_eta = TMath::Abs(vpion_p[i].Eta());
            if(tmp_eta < 5) h_eta->Fill(vpion_p[i].Eta());
            if(tmp_eta < 2.5){
                h_beta->Fill(vpion_p[i].Beta());
                h_gamma->Fill(vpion_p[i].Gamma());
            }
            if(tmp_eta < 1.0 ){
                h_barrel_beta->Fill(vpion_p[i].Beta());
                h_barrel_gamma->Fill(vpion_p[i].Gamma());
            } else if(tmp_eta < 2.5){
                h_endcap_beta->Fill(vpion_p[i].Beta());
                h_endcap_gamma->Fill(vpion_p[i].Gamma());
            }
        }
        //don't bother with the event if we can't detect it anyways
        if(deltar_vpi < 2.0) return kTRUE;
        if(TMath::Abs(vpion_p[0].Eta()) > 2.5 || TMath::Abs(vpion_p[1].Eta()) > 2.5) return kTRUE;
        
        ////////////////////////////////
        //* Varibales initialization *//
        
        double finestep = LTSTEP;
        
        //////////////////////////
        //* Looping on v-pions *//
        int barreldec = 0;
        int endcapdec = 0;
        
        double R1=0.;
        double R2=0.;
        double z1=0.;
        double z2=0.;
        double eta1=0.;
        double eta2=0.;
        
        for(int k=0; k<1200; ++k) {
            
            //int isBeamPipe(0),isID(0),isECal(0),isHCal(0),isMS(0),isMET(0);
            //int isBarMS(0),isEndMS(0);
            
            int isBarTrig(0);
            int isBarTrig_maxSyst(0),isBarTrig_minSyst(0);
            int isBarTrig_maxStat(0),isBarTrig_minStat(0);
            
            int isEndTrig(0);
            int isEndTrig_maxStat(0),isEndTrig_minStat(0);
            int isEndTrig_maxSyst(0),isEndTrig_minSyst(0);
            
            int isBBTrig(0);
            int isBBTrig_maxStat(0),isBBTrig_minStat(0);
            int isBBTrig_maxSyst(0),isBBTrig_minSyst(0);
            int isBETrig(0);
            int isBETrig_maxStat(0),isBETrig_minStat(0);
            int isBETrig_maxSyst(0),isBETrig_minSyst(0);
            int isEETrig(0);
            int isEETrig_maxStat(0),isEETrig_minStat(0);
            int isEETrig_maxSyst(0),isEETrig_minSyst(0);
            
            int isBBMSVx(0);
            int isBBMSVx_maxTrigStat(0),isBBMSVx_minTrigStat(0);
            int isBBMSVx_maxTrigSyst(0),isBBMSVx_minTrigSyst(0);
            int isBBMSVx_maxSyst(0),isBBMSVx_minSyst(0);
            
            
            //first value is pass/fail, 2nd is bin index
            int isBBMSVx_maxStat(0);
            int isBBMSVx_minStat(0);
            
            int isBEMSVx_maxStat(0);
            int isBEMSVx_minStat(0);
            
            int isBEMSVx(0);
            int isBEMSVx_maxTrigStat(0),isBEMSVx_minTrigStat(0);
            int isBEMSVx_maxTrigSyst(0),isBEMSVx_minTrigSyst(0);
            int isBEMSVx_maxSyst(0),isBEMSVx_minSyst(0);
            
            int isEBMSVx_maxStat(0);
            int isEBMSVx_minStat(0);
            
            int isEBMSVx(0);
            int isEBMSVx_maxTrigStat(0),isEBMSVx_minTrigStat(0);
            int isEBMSVx_maxTrigSyst(0),isEBMSVx_minTrigSyst(0);
            int isEBMSVx_maxSyst(0),isEBMSVx_minSyst(0);
            
            int isEEMSVx_maxStat(0);
            int isEEMSVx_minStat(0);
            
            int isEEMSVx(0);
            int isEEMSVx_maxTrigStat(0),isEEMSVx_minTrigStat(0);
            int isEEMSVx_maxTrigSyst(0),isEEMSVx_minTrigSyst(0);
            int isEEMSVx_maxSyst(0),isEEMSVx_minSyst(0);
            
            int isBarTrigMSVx_maxStat(0);
            int isBarTrigMSVx_minStat(0);
            
            int isBarTrigMSVx(0);
            int isBarTrigMSVx_maxTrigStat(0),isBarTrigMSVx_minTrigStat(0);
            int isBarTrigMSVx_maxTrigSyst(0),isBarTrigMSVx_minTrigSyst(0);
            int isBarTrigMSVx_maxSyst(0),isBarTrigMSVx_minSyst(0);
            
            int isEndTrigMSVx(0);
            int isEndTrigMSVx_maxTrigStat(0),isEndTrigMSVx_minTrigStat(0);
            int isEndTrigMSVx_maxTrigSyst(0),isEndTrigMSVx_minTrigSyst(0);
            int isEndTrigMSVx_maxSyst(0),isEndTrigMSVx_minSyst(0);
            
            int isEndTrigMSVx_maxStat(0);
            int isEndTrigMSVx_minStat(0);
            
            int isBIDVx(0),isBIDVx_max(0),isBIDVx_min(0);
            int isEIDVx(0),isEIDVx_max(0),isEIDVx_min(0);
            
            int isIDVx_maxStat(0);
            int isIDVx_minStat(0);
            
            int isIDVx(0),isIDVxAll(0);
            int isIDVx_maxTrigStat(0),isIDVx_minTrigStat(0);
            int isIDVx_maxTrigSyst(0),isIDVx_minTrigSyst(0);
            int isIDVx_maxSyst(0),isIDVx_minSyst(0);
            
            int isPassedTrig(0),isPassedTrig_min(0),isPassedTrig_max(0);
            bool isGoodMSVx(false),isGoodMSVx_min(false),isGoodMSVx_max(false);
            int MSVxCombo(0),MSVxCombo_max(0),MSVxCombo_min(0);
            
            double vertex_dR(0.0);
            int nVertices(0);
            
            double ctau = -999;
            if(k < 400 ) ctau = k*LTSTEP3 + 0.0001;
            else if(k < 800) ctau = k*LTSTEP + 0.0001 - 9.;
            else if(k < 1200) ctau = k*LTSTEP2 + 0.0001 - 189.;
            
            //if(ctau > 10) break;
            
            vector<TVector3> new_decay_pos;
            for(unsigned int i=0; i<vpion_p.size(); ++i) {
                
                ////////////////////////////////
                //* Generating v-pion decays *//O
                
                double bgct = vpion_p[i].Beta()*vpion_p[i].Gamma()*ctau*1000.0;
                double Lxyz = rnd.Exp(bgct);
                TVector3 tmp_pos;
                tmp_pos.SetMagThetaPhi(Lxyz,vpion_p[i].Theta(),vpion_p[i].Phi());
                
                new_decay_pos.push_back(tmp_pos);
            }
            
            //can probably get rid of these next two
            /*     if(k == kBin){
             if(i==0){
             R1 = R;
             z1 = TMath::Abs(z);
             eta1 = vpieta;
             }
             else if(i==1){
             R2 = R;
             z2 = TMath::Abs(z);
             eta2 = vpieta;
             }
             }
             
             if(k == kBin){
             if(eta < 1.0) {
             int index = R/BMSstep;
             if(index < 50) vpiBarrelMS[index]++;
             if(R > 3000. && R < 8000.) barreldec++;
             }
             if(eta>1.0 && eta <2.5 && R < 10000) {
             int index = z/EMSstep;
             if(index < 50 ) vpiEndcapMS_R10[index]++;
             if(z >5000. && z < 15000.) endcapdec++;
             }
             }
             
             if(R > 12000 || z > 25000) {
             isMET[k]++;
             continue;
             }*/
            
            //TRIGGER EFFICIENCY HERE!
            //triggerPassProb returns the prob if all conditions are met, and -1 if the trigger fails for other reasons (i.e. timing)
            
            
            /*timestamp_t t0 = get_timestamp();*/
            double trigPassProb = triggerPassProb(new_decay_pos,vpion_p,k);
            /*timestamp_t t1 = get_timestamp();
             double time1 = (t1 - t0) / 1000000.0L;
             if(time1 > 0.0000031) cout << "time1 " << time1 << endl;*/
            
            /*
             int nID=0;
             int nJET=0;
             for(int i_dec = 0; i_dec < new_decay_pos.size(); i_dec++){
             if(checkDecLoc(new_decay_pos[i_dec]) == 3){
             nID++;
             double vxIDprob = rnd.Rndm();
             int BIDindex  = int(new_decay_pos[i_dec].Perp()/BIDstep);
             if(vxIDprob < IDProb[BIDindex]){
             h_Expected_IDVxIDVx->Fill(ctau);
             }
             } else if(checkDecLoc(new_decay_pos[i_dec]) == 5){
             nJET++;
             }
             }
             if(nID == 2) h_IDID_Events->Fill(ctau);
             if((nID + nJET) == 2) h_IDJET_Events->Fill(ctau);
             */
            if(trigPassProb > 0){
                if(isBB_Event(new_decay_pos)){
                    vector<int> bbMSindex;
                    for(int j=0; j< new_decay_pos.size(); j++){
                        bbMSindex.push_back(int(new_decay_pos[j].Perp()/BMSstepTRIG));
                    }
                    std::sort(bbMSindex.begin(),bbMSindex.end());
                    if(trigPassProb < BBTrigProb[bbMSindex[1]][bbMSindex[0]]){
                        isBBTrig++;
                        isPassedTrig++;
                        if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k)nBB_Trig++;
                    }
                    if(trigPassProb < BBTrigProb_maxStat[bbMSindex[1]][bbMSindex[0]]) isBBTrig_maxStat++;
                    if(trigPassProb < BBTrigProb_minStat[bbMSindex[1]][bbMSindex[0]]) isBBTrig_minStat++;
                    if(trigPassProb < BBTrigProb_maxSyst[bbMSindex[1]][bbMSindex[0]]) isBBTrig_maxSyst++;
                    if(trigPassProb < BBTrigProb_minSyst[bbMSindex[1]][bbMSindex[0]]) isBBTrig_minSyst++;
                    
                } else if(isEE_Event(new_decay_pos)){
                    vector<int> eeMSindex;
                    for(int j=0; j< new_decay_pos.size(); j++){
                        eeMSindex.push_back(int(TMath::Abs(new_decay_pos[j].Z())/EMSstepTRIG));
                    }
                    std::sort(eeMSindex.begin(),eeMSindex.end());
                    if(trigPassProb < EETrigProb[eeMSindex[1]][eeMSindex[0]]){
                        isEETrig++;
                        isPassedTrig++;
                        if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k)nEE_Trig++;
                    }
                    if(trigPassProb < EETrigProb_maxStat[eeMSindex[1]][eeMSindex[0]]) isEETrig_maxStat++;
                    if(trigPassProb < EETrigProb_minStat[eeMSindex[1]][eeMSindex[0]]) isEETrig_minStat++;
                    if(trigPassProb < EETrigProb_maxSyst[eeMSindex[1]][eeMSindex[0]]) isEETrig_maxSyst++;
                    if(trigPassProb < EETrigProb_minSyst[eeMSindex[1]][eeMSindex[0]]) isEETrig_minSyst++;
                    
                } else if(isBE_Event(new_decay_pos)){
                    int beMSindex_b=0;
                    int beMSindex_e=0;
                    if(checkDecLoc(new_decay_pos[0]) == 1){
                        beMSindex_b = int(new_decay_pos[0].Perp()/BEMSstepTRIG_Y);
                        beMSindex_e = int(TMath::Abs(new_decay_pos[1].Z())/BEMSstepTRIG_X);
                    } else{
                        beMSindex_b = int(new_decay_pos[1].Perp()/BEMSstepTRIG_Y);
                        beMSindex_e = int(TMath::Abs(new_decay_pos[0].Z())/BEMSstepTRIG_X);
                    }
                    
                    if(trigPassProb < BETrigProb[beMSindex_e][beMSindex_b]){
                        isBETrig++;
                        isPassedTrig++;
                        if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k)nBE_Trig++;
                    }
                    if(trigPassProb < BETrigProb_maxStat[beMSindex_e][beMSindex_b]) isBETrig_maxStat++;
                    if(trigPassProb < BETrigProb_minStat[beMSindex_e][beMSindex_b])  isBETrig_minStat++;
                    if(trigPassProb < BETrigProb_maxSyst[beMSindex_e][beMSindex_b]) isBETrig_maxSyst++;
                    if(trigPassProb < BETrigProb_minSyst[beMSindex_e][beMSindex_b])  isBETrig_minSyst++;
                    
                    
                } else if(isBID_Event(new_decay_pos)){
                    int BMSindex=0;
                    if(checkDecLoc(new_decay_pos[0]) == 1) BMSindex = int(new_decay_pos[0].Perp()/BMSstep);
                    else BMSindex = int(new_decay_pos[1].Perp()/BMSstep);
                    if(trigPassProb < BarTrigProb[BMSindex]) {
                        //isBarrelRoICluster = true;
                        isBarTrig++;
                        isPassedTrig = true;
                        if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k)nMSB_ID_Trig++;
                        if((h_Expected_2MSVx->FindBin(6)-1) == k){
                            if(checkDecLoc(new_decay_pos[0]) == 1) h_barrel_gamma_50->Fill(vpion_p[0].Gamma());
                            else h_barrel_gamma_50->Fill(vpion_p[1].Gamma());
                        } else if((h_Expected_2MSVx->FindBin(0.6)-1)==k){
                            if(checkDecLoc(new_decay_pos[0]) == 1) h_barrel_gamma_025->Fill(vpion_p[0].Gamma());
                            else h_barrel_gamma_025->Fill(vpion_p[1].Gamma());
                        }
                    }
                    if(trigPassProb < BarTrigProb_maxStat[BMSindex]) isBarTrig_maxStat++;
                    if(trigPassProb < BarTrigProb_minStat[BMSindex]) isBarTrig_minStat++;
                    if(trigPassProb < BarTrigProb_maxSyst[BMSindex]) isBarTrig_maxSyst++;
                    if(trigPassProb < BarTrigProb_minSyst[BMSindex]) isBarTrig_minSyst++;
                    
                } else if(isEID_Event(new_decay_pos)){
                    int EMSindex=0;
                    if(checkDecLoc(new_decay_pos[0]) == 2) EMSindex = int(TMath::Abs(new_decay_pos[0].Z())/EMSstep);
                    else EMSindex = int(TMath::Abs(new_decay_pos[1].Z())/EMSstep);
                    if(trigPassProb < EndTrigProb[EMSindex]) {
                        //isEndcapRoICluster = true;
                        isEndTrig++;
                        isPassedTrig = true;
                        if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k) nMSE_ID_Trig++;
                        if((h_Expected_2MSVx->FindBin(6)-1) == k){
                            if(checkDecLoc(new_decay_pos[0]) == 1) h_endcap_gamma_50->Fill(vpion_p[0].Gamma());
                            else h_endcap_gamma_50->Fill(vpion_p[1].Gamma());
                        } else if((h_Expected_2MSVx->FindBin(0.6)-1)==k){
                            if(checkDecLoc(new_decay_pos[0]) == 1) h_endcap_gamma_025->Fill(vpion_p[0].Gamma());
                            else h_endcap_gamma_025->Fill(vpion_p[1].Gamma());
                        }
                    }
                    if(trigPassProb < EndTrigProb_maxStat[EMSindex]) isEndTrig_maxStat++;
                    if(trigPassProb < EndTrigProb_minStat[EMSindex]) isEndTrig_minStat++;
                    if(trigPassProb < EndTrigProb_maxSyst[EMSindex]) isEndTrig_maxSyst++;
                    if(trigPassProb < EndTrigProb_minSyst[EMSindex]) isEndTrig_minSyst++;
                    
                }
            } // end if trigPassProb > 0
            else continue;
            /*timestamp_t t2 = get_timestamp();
             double time2 = (t2 - t1) / 1000000.0L;
             if(time2 > 0.0000031) cout << "time2 " << time2 << endl;*/
            //now, run through each of the vpions separately!
            
            for(unsigned int i=0; i<new_decay_pos.size(); ++i) {
                
                //CREATE R AND Z AGAIN, or USE TVECTOR3!
                
                int BMSindex = int(new_decay_pos[i].Perp()/BMSstep);
                int BBMSindex = int(new_decay_pos[i].Perp()/BBMSstep);
                int BEMSindex = int(new_decay_pos[i].Perp()/BEMSstep);
                
                int EMSindex = int(TMath::Abs(new_decay_pos[i].z())/EMSstep);
                int EEMSindex = int(TMath::Abs(new_decay_pos[i].z())/EEMSstep);
                int EBMSindex = int(TMath::Abs(new_decay_pos[i].z())/EBMSstep);
                
                int BIDindex  = int(new_decay_pos[i].Perp()/BIDstep);
                int EIDindex  = int(TMath::Abs(new_decay_pos[i].z())/EIDstep);
                
                /////////////////////////////////////////
                //* Saving where v-pion decay occured *//
                
                /*      if(R < 100) {
                 isBeamPipe[k]++;
                 h_BP->Fill(ctau);
                 } else if(R < ID_r && z < ID_z) {
                 isID[k]++;
                 h_ID->Fill(ctau);
                 } else if(R < ECAL_r3 && z < ECAL_z3) {
                 isECal[k]++;
                 h_ECAL->Fill(ctau);
                 } else if(R < HCAL_r3 && z < HCAL_z3) {
                 isHCal[k]++;
                 h_HCAL->Fill(ctau);
                 } else if(R < MS_r3 && z < MS_z3) {
                 isMS[k]++;
                 h_MS->Fill(ctau);
                 if(eta < 1.0)       isBarMS[k]++;
                 else if (eta < 2.5) isEndMS[k]++;
                 } else isMET[k]++;
                 */
                
                if(new_decay_pos[i].Perp() > 10000.) continue;
                
                /////////////////////////////////////////////
                //* Check if muon vertex is reconstructed *//
                
                double vxprob = rnd.Rndm();
                ///////////////////////////
                //* First in the barrel *//
                
                //if(eta<1.0 && BMSindex<nBTrigVXbins) {
                
                if (isBBTrig) {
                    if( vxprob < BBMSVxProb[BBMSindex]){
                        nVertices++;
                        isBBMSVx++;
                        MSVxCombo++;
                    }
                    if(vxprob < BBMSVxProb_maxStat[BBMSindex]) isBBMSVx_maxStat++;
                    if(vxprob < BBMSVxProb_minStat[BBMSindex]) isBBMSVx_minStat++;
                    
                    if(vxprob < BBMSVxProb_maxSyst[BBMSindex]) isBBMSVx_maxSyst++;
                    if(vxprob < BBMSVxProb_minSyst[BBMSindex]) isBBMSVx_minSyst++;
                }
                if(isBBTrig_maxSyst && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_maxTrigSyst++;
                if(isBBTrig_minSyst && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_minTrigSyst++;
                
                if(isBBTrig_maxStat && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_maxTrigStat++;
                if(isBBTrig_minStat && vxprob < BBMSVxProb[BBMSindex]) isBBMSVx_minTrigStat++;
                
                if(isBETrig && checkDecLoc(new_decay_pos[i]) == 1){
                    if(vxprob < BEMSVxProb[BEMSindex]) {
                        nVertices++;
                        isBEMSVx++;
                        MSVxCombo++;
                    }
                    if(vxprob < BEMSVxProb_maxStat[BEMSindex]){
                        isBEMSVx_maxStat++;
                        //isBEMSVx_maxStat.second = BEMSindex;
                    }
                    if(vxprob < BEMSVxProb_minStat[BEMSindex]){
                        isBEMSVx_minStat++;
                        //isBEMSVx_minStat.second = BEMSindex;
                    }
                    if(vxprob < BEMSVxProb_maxSyst[BEMSindex]) isBEMSVx_maxSyst++;
                    if(vxprob < BEMSVxProb_minSyst[BEMSindex]) isBEMSVx_minSyst++;
                }
                if(isBETrig && checkDecLoc(new_decay_pos[i]) == 2){
                    if(vxprob < EBMSVxProb[EBMSindex]) {
                        nVertices++;
                        isEBMSVx++;
                        MSVxCombo++;
                    }
                    if(vxprob < EBMSVxProb_maxStat[EBMSindex]){
                        //isEBMSVx_maxStat.second = EBMSindex;
                        isEBMSVx_maxStat++;
                    }
                    if(vxprob < EBMSVxProb_minStat[EBMSindex]){
                        //isEBMSVx_minStat.second = EBMSindex;
                        isEBMSVx_minStat++;
                    }
                    if(vxprob < EBMSVxProb_maxSyst[EBMSindex]) isEBMSVx_maxSyst++;
                    if(vxprob < EBMSVxProb_minSyst[EBMSindex]) isEBMSVx_minSyst++;
                }
                if(isBETrig_maxSyst && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_maxTrigSyst++;
                if(isBETrig_maxStat && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_maxTrigStat++;
                if(isBETrig_minSyst && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_minTrigSyst++;
                if(isBETrig_minStat && checkDecLoc(new_decay_pos[i]) == 1 && vxprob < BEMSVxProb[BEMSindex]) isBEMSVx_minTrigStat++;
                
                if(isBETrig_maxSyst && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_maxTrigSyst++;
                if(isBETrig_maxStat && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_maxTrigStat++;
                if(isBETrig_minSyst && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_minTrigSyst++;
                if(isBETrig_minStat && checkDecLoc(new_decay_pos[i]) == 2 && vxprob < EBMSVxProb[EBMSindex]) isEBMSVx_minTrigStat++;
                
                if(isEETrig) {
                    if(vxprob < EEMSVxProb[EEMSindex]){
                        nVertices++;
                        isEEMSVx++;
                        MSVxCombo++;
                    }
                    
                    if(vxprob < EEMSVxProb_maxStat[EEMSindex]) isEEMSVx_maxStat++;
                    if(vxprob < EEMSVxProb_minStat[EEMSindex]) isEEMSVx_minStat++;
                    
                    if(vxprob < EEMSVxProb_maxSyst[EEMSindex]) isEEMSVx_maxSyst++;
                    if(vxprob < EEMSVxProb_minSyst[EEMSindex]) isEEMSVx_minSyst++;
                    
                }
                if(isEETrig_maxSyst && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_maxTrigSyst++;
                if(isEETrig_maxStat && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_maxTrigStat++;
                if(isEETrig_minSyst && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_minTrigSyst++;
                if(isEETrig_minStat && (vxprob < EEMSVxProb[EEMSindex])) isEEMSVx_minTrigStat++;
                
                if(isBarTrig){
                    //first the MS vertex
                    if(checkDecLoc(new_decay_pos[i])==1){
                        ///////////////////////////////////
                        //* If the v-pion was triggered (except since ID+MS, vpion HAD to have been triggered already*//
                        
                        if(vxprob < BarTrigMSVxProb[BMSindex]) {
                            nVertices++;
                            isBarTrigMSVx++;
                            MSVxCombo++;
                        }
                        if(vxprob < BarTrigMSVxProb_maxSyst[BMSindex]) isBarTrigMSVx_maxSyst++;
                        if(vxprob < BarTrigMSVxProb_minSyst[BMSindex]) isBarTrigMSVx_minSyst++;
                        if(vxprob < BarTrigMSVxProb_maxStat[BMSindex]){
                            isBarTrigMSVx_maxStat++;
                            //isBarTrigMSVx_maxStat.second = BMSindex;
                        }
                        if(vxprob < BarTrigMSVxProb_minStat[BMSindex]){
                            isBarTrigMSVx_minStat++;
                            //isBarTrigMSVx_minStat.second = BMSindex;
                        }
                        
                    }
                    else if(checkDecLoc(new_decay_pos[i]) == 3){
                        //entire ID
                        if(vxprob < IDProb[BIDindex]){
                            isIDVx++;
                        }
                        if(vxprob < IDProb_maxStat[BIDindex]){
                            isIDVx_maxStat++;
                            //isIDVx_maxStat.second = BIDindex;
                        }
                        if(vxprob < IDProb_minStat[BIDindex]){
                            isIDVx_minStat++;
                            //isIDVx_minStat.second = BIDindex;
                        }
                        if(vxprob < IDProb_maxSyst[BIDindex]) isIDVx_maxSyst++;
                        if(vxprob < IDProb_minSyst[BIDindex]) isIDVx_minSyst++;
                        
                        //barrel ID
                        if(vxprob < BIDProb[BIDindex] && TMath::Abs(new_decay_pos[i].Eta()) < 1.5){
                            isBIDVx++;
                            nVertices++;
                        }
                        //endcap ID
                        if(vxprob < EIDProb[EIDindex] && TMath::Abs(new_decay_pos[i].Eta()) > 1.5 && TMath::Abs(new_decay_pos[i].Eta()) < 2.5){
                            isEIDVx++;
                            nVertices++;
                        }
                    }
                }
                if(isBarTrig_maxSyst && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_maxTrigSyst++;
                if(isBarTrig_maxStat && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_maxTrigStat++;
                if(isBarTrig_minSyst && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_minTrigSyst++;
                if(isBarTrig_minStat && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_minTrigStat++;
                
                if(isBarTrig_maxSyst && checkDecLoc(new_decay_pos[i])==1 && vxprob < BarTrigMSVxProb[BMSindex]) isBarTrigMSVx_maxTrigSyst++;
                if(isBarTrig_maxStat && checkDecLoc(new_decay_pos[i])==1 && vxprob < BarTrigMSVxProb[BMSindex]) isBarTrigMSVx_maxTrigStat++;
                if(isBarTrig_minSyst && checkDecLoc(new_decay_pos[i])==1 && vxprob < BarTrigMSVxProb[BMSindex]) isBarTrigMSVx_minTrigSyst++;
                if(isBarTrig_minStat && checkDecLoc(new_decay_pos[i])==1 && vxprob < BarTrigMSVxProb[BMSindex]) isBarTrigMSVx_minTrigStat++;
                
                if(isEndTrig){
                    //first the MS vertex
                    if(checkDecLoc(new_decay_pos[i])==2){
                        if(vxprob < EndTrigMSVxProb[EMSindex]) {
                            nVertices++;
                            isEndTrigMSVx++;
                            MSVxCombo++;
                        }
                        if(vxprob < EndTrigMSVxProb_maxSyst[EMSindex]) isEndTrigMSVx_maxSyst++;
                        if(vxprob < EndTrigMSVxProb_minSyst[EMSindex]) isEndTrigMSVx_minSyst++;
                        if(vxprob < EndTrigMSVxProb_maxStat[EMSindex]){
                            isEndTrigMSVx_maxStat++;
                            //isEndTrigMSVx_maxStat.second = EMSindex;
                        }
                        if(vxprob < EndTrigMSVxProb_minStat[EMSindex]){
                            isEndTrigMSVx_minStat++;
                            //sEndTrigMSVx_minStat.second = EMSindex;
                        }
                        
                        
                    } else if(checkDecLoc(new_decay_pos[i]) == 3){
                        //all the ID
                        if(vxprob < IDProb[BIDindex]){
                            isIDVx++;
                            nVertices++;
                        }
                        if(vxprob < IDProb_maxStat[BIDindex]){
                            isIDVx_maxStat++;
                            //isIDVx_maxStat.second = BIDindex;
                        }
                        if(vxprob < IDProb_minStat[BIDindex]){
                            isIDVx_minStat++;
                            //isIDVx_minStat.second = BIDindex;
                        }
                        if(vxprob < IDProb_maxSyst[BIDindex]) isIDVx_maxSyst++;
                        if(vxprob < IDProb_minSyst[BIDindex]) isIDVx_minSyst++;
                        
                        //barrel ID
                        if(vxprob < BIDProb[BIDindex] && TMath::Abs(new_decay_pos[i].Eta()) < 1.5) isBIDVx++;
                        //endcap ID
                        if(vxprob < EIDProb[EIDindex] && TMath::Abs(new_decay_pos[i].Eta()) > 1.5 && TMath::Abs(new_decay_pos[i].Eta()) < 2.5) isEIDVx++;
                    }
                }
                if(isEndTrig_maxSyst && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_maxTrigSyst++;
                if(isEndTrig_maxStat && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_maxTrigStat++;
                if(isEndTrig_minSyst && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_minTrigSyst++;
                if(isEndTrig_minStat && checkDecLoc(new_decay_pos[i])==3 && vxprob < IDProb[BIDindex]) isIDVx_minTrigStat++;
                
                if(isEndTrig_maxSyst && checkDecLoc(new_decay_pos[i])==2 && vxprob < EndTrigMSVxProb[EMSindex]) isEndTrigMSVx_maxTrigSyst++;
                if(isEndTrig_maxStat && checkDecLoc(new_decay_pos[i])==2 && vxprob < EndTrigMSVxProb[EMSindex]) isEndTrigMSVx_maxTrigStat++;
                if(isEndTrig_minSyst && checkDecLoc(new_decay_pos[i])==2 && vxprob < EndTrigMSVxProb[EMSindex]) isEndTrigMSVx_minTrigSyst++;
                if(isEndTrig_minStat && checkDecLoc(new_decay_pos[i])==2 && vxprob < EndTrigMSVxProb[EMSindex]) isEndTrigMSVx_minTrigStat++;
            }
            /*   timestamp_t t3 = get_timestamp();
             double time3 = (t3 - t2) / 1000000.0L;
             if(time3 > 0.0000031) cout << "time3 " << time3 << endl;*/
            
            if(vpion_p.size() == 2){
                vertex_dR = vpion_p[0].DeltaR(vpion_p[1]);
            } else cout << "not two vpions in this event" << endl;
            
            /////////////////////////////////////////////////////////
            //* Saving if trigger fired (both barrel and end-cap) *//
            if(isPassedTrig)     h_Expected_Trig->Fill(ctau);
            if(isPassedTrig_min) h_Expected_Trig_min->Fill(ctau);
            if(isPassedTrig_max) h_Expected_Trig_max->Fill(ctau);
            
            if(isBBTrig)     h_Expected_BBTrig->Fill(ctau);
            if(isBBTrig_minStat){
                h_Expected_BBTrig_minStatTotal->Fill(ctau);
            }
            if(isBBTrig_maxStat){
                h_Expected_BBTrig_maxStatTotal->Fill(ctau);
            }
            if(isBBTrig_minSyst) h_Expected_BBTrig_minSyst->Fill(ctau);
            if(isBBTrig_maxSyst) h_Expected_BBTrig_maxSyst->Fill(ctau);
            
            if(isEETrig)     h_Expected_EETrig->Fill(ctau);
            if(isEETrig_minStat){
                
                h_Expected_EETrig_minStatTotal->Fill(ctau);
            }
            if(isEETrig_maxStat){
                h_Expected_EETrig_maxStatTotal->Fill(ctau);
            }
            if(isEETrig_minSyst) h_Expected_EETrig_minSyst->Fill(ctau);
            if(isEETrig_maxSyst) h_Expected_EETrig_maxSyst->Fill(ctau);
            
            if(isBETrig)     h_Expected_BETrig->Fill(ctau);
            if(isBETrig_minStat){
                h_Expected_BETrig_minStatTotal->Fill(ctau);
            }
            if(isBETrig_maxStat){
                h_Expected_BETrig_maxStatTotal->Fill(ctau);
            }
            if(isBETrig_minSyst) h_Expected_BETrig_minSyst->Fill(ctau);
            if(isBETrig_maxSyst) h_Expected_BETrig_maxSyst->Fill(ctau);
            
            if(isBarTrig)     h_Expected_BIDTrig->Fill(ctau);
            if(isBarTrig_minSyst) h_Expected_BIDTrig_minSyst->Fill(ctau);
            if(isBarTrig_maxSyst) h_Expected_BIDTrig_maxSyst->Fill(ctau);
            if(isBarTrig_minStat){
                h_Expected_BIDTrig_minStatTotal->Fill(ctau);
            }
            if(isBarTrig_maxStat){
                h_Expected_BIDTrig_maxStatTotal->Fill(ctau);
            }
            
            if(isEndTrig)     h_Expected_EIDTrig->Fill(ctau);
            if(isEndTrig_minSyst) h_Expected_EIDTrig_minSyst->Fill(ctau);
            if(isEndTrig_maxSyst) h_Expected_EIDTrig_maxSyst->Fill(ctau);
            if(isEndTrig_minStat){
                h_Expected_EIDTrig_minStatTotal->Fill(ctau);
            }
            if(isEndTrig_maxStat){
                h_Expected_EIDTrig_maxStatTotal->Fill(ctau);
            }
            
            ////////////////////////////////////////////////////////////////////////////////////
            //* Saving if trigger fired and vertex was found (barrel and endcap separately) *//
            
            if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k){
                if(isBBMSVx==0)  nBB_Trig_0vx++;
                if(isBBMSVx==1)  nBB_Trig_1vx++;
                if(isBBMSVx==2)  nBB_Trig_2vx++;
                if(isBEMSVx==0)  nBE_Trig_0vx++;
                if((isBEMSVx==1&&isEBMSVx==0)||(isBEMSVx==0&&isEBMSVx==1))  nBE_Trig_1vx++;
                if(isBEMSVx==1 && isEBMSVx==1)  nBE_Trig_2vx++;
                if(isEEMSVx==0)  nEE_Trig_0vx++;
                if(isEEMSVx==1)  nEE_Trig_1vx++;
                if(isEEMSVx==2)  nEE_Trig_2vx++;
            }
            
            
            if(isBBMSVx==2)                             h_Expected_BBMSVx->Fill(ctau);
            if(isBEMSVx == 1 && isEBMSVx == 1)          h_Expected_BEMSVx->Fill(ctau);
            if(isEEMSVx==2)                             h_Expected_EEMSVx->Fill(ctau);
            
            if(isBarTrigMSVx==1 && isIDVx == 1)         h_Expected_BMSVxIDVx->Fill(ctau);
            if(isEndTrigMSVx==1 && isIDVx == 1)         h_Expected_EMSVxIDVx->Fill(ctau);
            
            if(isBBMSVx==2 || (isBEMSVx==1 && isEBMSVx==1) || isEEMSVx==2 ) h_Expected_2MSVx->Fill(ctau);
            if( (isBarTrigMSVx==1 || isEndTrigMSVx==1) && isIDVx == 1) h_Expected_MSVxIDVx->Fill(ctau);
            if( isBBMSVx==2 || (isBEMSVx==1 && isEBMSVx==1) || isEEMSVx==2 || ( (isBarTrigMSVx==1 || isEndTrigMSVx==1) && isIDVx == 1) ) h_Expected_Tot2Vx->Fill(ctau);
            
            //trigger systematic
            
            if(isBBMSVx_maxTrigSyst==2)                                      h_Expected_BBMSVx_maxTrigSyst->Fill(ctau);
            if(isBEMSVx_maxTrigSyst== 1 && isEBMSVx_maxTrigSyst == 1)        h_Expected_BEMSVx_maxTrigSyst->Fill(ctau);
            if(isEEMSVx_maxTrigSyst==2)                                      h_Expected_EEMSVx_maxTrigSyst->Fill(ctau);
            if(isBarTrigMSVx_maxTrigSyst==1 && isIDVx_maxTrigSyst == 1)      h_Expected_BMSVxIDVx_maxTrigSyst->Fill(ctau);
            if(isEndTrigMSVx_maxTrigSyst==1 && isIDVx_maxTrigSyst == 1)      h_Expected_EMSVxIDVx_maxTrigSyst->Fill(ctau);
            
            if(isBBMSVx_minTrigSyst==2)                                      h_Expected_BBMSVx_minTrigSyst->Fill(ctau);
            if(isBEMSVx_minTrigSyst==1 && isEBMSVx_minTrigSyst == 1)         h_Expected_BEMSVx_minTrigSyst->Fill(ctau);
            if(isEEMSVx_minTrigSyst==2)                                      h_Expected_EEMSVx_minTrigSyst->Fill(ctau);
            if(isBarTrigMSVx_minTrigSyst==1 && isIDVx_minTrigSyst == 1)      h_Expected_BMSVxIDVx_minTrigSyst->Fill(ctau);
            if(isEndTrigMSVx_minTrigSyst==1 && isIDVx_minTrigSyst == 1)      h_Expected_EMSVxIDVx_minTrigSyst->Fill(ctau);
            
            //trigger statistics
            
            if(isBBMSVx_maxTrigStat==2){
                h_Expected_BBMSVx_maxTrigStatTotal->Fill(ctau);
            }
            if(isBEMSVx_maxTrigStat==1 && isEBMSVx_maxTrigStat == 1){
                h_Expected_BEMSVx_maxTrigStatTotal->Fill(ctau);
            }
            if(isEEMSVx_maxTrigStat==2){
                h_Expected_EEMSVx_maxTrigStatTotal->Fill(ctau);
            }
            if(isBarTrigMSVx_maxTrigStat==1 && isIDVx_maxTrigStat == 1){
                h_Expected_BMSVxIDVx_maxTrigStatTotal->Fill(ctau);
            }
            if(isEndTrigMSVx_maxTrigStat==1 && isIDVx_maxTrigStat == 1){
                h_Expected_EMSVxIDVx_maxTrigStatTotal->Fill(ctau);
            }
            
            if(isBBMSVx_minTrigStat==2){
                h_Expected_BBMSVx_minTrigStatTotal->Fill(ctau);
            }
            if((isBEMSVx_minTrigStat==1 && isEBMSVx_minTrigStat == 1)){
                h_Expected_BEMSVx_minTrigStatTotal->Fill(ctau);
            }
            if(isEEMSVx_minTrigStat==2){
                h_Expected_EEMSVx_minTrigStatTotal->Fill(ctau);
            }
            if((isBarTrigMSVx_minTrigStat==1 && isIDVx_minTrigStat == 1)){
                h_Expected_BMSVxIDVx_minTrigStatTotal->Fill(ctau);
            }
            if((isEndTrigMSVx_minTrigStat==1 && isIDVx_minTrigStat == 1)){
                h_Expected_EMSVxIDVx_minTrigStatTotal->Fill(ctau);
            }
            
            //MS vertex syst
            
            if(isBBMSVx_maxSyst==2)                             h_Expected_BBMSVx_maxBVXSyst->Fill(ctau);
            if(isEEMSVx_maxSyst==2)                             h_Expected_EEMSVx_maxEVXSyst->Fill(ctau);
            
            if(isBEMSVx_maxSyst==1 && isEBMSVx == 1)            h_Expected_BEMSVx_maxBEVXSyst->Fill(ctau);
            if(isBEMSVx == 1 && isEBMSVx_maxSyst == 1)          h_Expected_BEMSVx_maxEBVXSyst->Fill(ctau);
            if(isBarTrigMSVx_maxSyst==1 && isIDVx == 1)         h_Expected_BMSVxIDVx_maxBMSVXSyst->Fill(ctau);
            if(isEndTrigMSVx_maxSyst==1 && isIDVx == 1)         h_Expected_EMSVxIDVx_maxEMSVXSyst->Fill(ctau);
            
            if(isBBMSVx_minSyst==2)                             h_Expected_BBMSVx_minBVXSyst->Fill(ctau);
            if(isBEMSVx_minSyst == 1 && isEBMSVx == 1)          h_Expected_BEMSVx_minBEVXSyst->Fill(ctau);
            if(isBEMSVx == 1 && isEBMSVx_minSyst == 1)          h_Expected_BEMSVx_minEBVXSyst->Fill(ctau);
            if(isEEMSVx_minSyst==2)                             h_Expected_EEMSVx_minEVXSyst->Fill(ctau);
            if(isBarTrigMSVx_minSyst==1 && isIDVx == 1)         h_Expected_BMSVxIDVx_minBMSVXSyst->Fill(ctau);
            if(isEndTrigMSVx_minSyst==1 && isIDVx == 1)         h_Expected_EMSVxIDVx_minEMSVXSyst->Fill(ctau);
            
            //MS vertex stat
            
            if(isBBMSVx_maxStat==2)  h_Expected_BBMSVx_maxBVXStatTotal->Fill(ctau);
            if(isBBMSVx_minStat==2)  h_Expected_BBMSVx_minBVXStatTotal->Fill(ctau);
            
            if(isEEMSVx_maxStat==2) h_Expected_EEMSVx_maxEVXStatTotal->Fill(ctau);
            if(isEEMSVx_minStat==2) h_Expected_EEMSVx_minEVXStatTotal->Fill(ctau);
            
            
            if(isBEMSVx_maxStat==1 && isEBMSVx == 1) h_Expected_BEMSVx_maxBEVXStatTotal->Fill(ctau);
            if(isBEMSVx_minStat==1 && isEBMSVx == 1) h_Expected_BEMSVx_minBEVXStatTotal->Fill(ctau);
            
            if(isBEMSVx == 1 && isEBMSVx_maxStat==1) h_Expected_BEMSVx_maxEBVXStatTotal->Fill(ctau);
            if(isBEMSVx == 1 && isEBMSVx_minStat==1) h_Expected_BEMSVx_minEBVXStatTotal->Fill(ctau);
            
            if(isBarTrigMSVx_maxStat==1 && isIDVx == 1)  h_Expected_BMSVxIDVx_maxBMSVXStatTotal->Fill(ctau);
            if(isEndTrigMSVx_maxStat==1 && isIDVx == 1)  h_Expected_EMSVxIDVx_maxEMSVXStatTotal->Fill(ctau);
            
            if(isBarTrigMSVx_minStat==1 && isIDVx == 1)  h_Expected_BMSVxIDVx_minBMSVXStatTotal->Fill(ctau);
            if(isEndTrigMSVx_minStat==1 && isIDVx == 1)  h_Expected_EMSVxIDVx_minEMSVXStatTotal->Fill(ctau);
            
            //ID vertex syst
            
            if(isBarTrigMSVx==1 && isIDVx_maxSyst == 1)         h_Expected_BMSVxIDVx_maxIDVXSyst->Fill(ctau);
            if(isEndTrigMSVx==1 && isIDVx_maxSyst == 1)         h_Expected_EMSVxIDVx_maxIDVXSyst->Fill(ctau);
            
            if(isBarTrigMSVx==1 && isIDVx_minSyst == 1)         h_Expected_BMSVxIDVx_minIDVXSyst->Fill(ctau);
            if(isEndTrigMSVx==1 && isIDVx_minSyst == 1)         h_Expected_EMSVxIDVx_minIDVXSyst->Fill(ctau);
            
            //ID vertex stat
            
            if(isBarTrigMSVx==1 && isIDVx_maxStat == 1)         h_Expected_BMSVxIDVx_maxIDVXStatTotal->Fill(ctau);
            if(isEndTrigMSVx==1 && isIDVx_maxStat == 1)         h_Expected_EMSVxIDVx_maxIDVXStatTotal->Fill(ctau);
            
            if(isBarTrigMSVx==1 && isIDVx_minStat == 1)         h_Expected_BMSVxIDVx_minIDVXStatTotal->Fill(ctau);
            if(isEndTrigMSVx==1 && isIDVx_minStat == 1)         h_Expected_EMSVxIDVx_minIDVXStatTotal->Fill(ctau);
            
            
            int testMSMS=0;
            bool goodVertexdR = false;
            if(nVertices == 2 && vertex_dR > 2.0) goodVertexdR = true;
            
            if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k){
                
                if((isBBMSVx==2 || (isBEMSVx==1 && isEBMSVx==1) || isEEMSVx==2) && isPassedTrig){
                    nMSVxMSVx++;
                    if(isBBMSVx == 2){
                        nMSvx_BB++;
                    }else if(isBEMSVx == 1 && isEBMSVx == 1){
                        nMSvx_BE++;
                    }else if(isEEMSVx == 2){
                        nMSvx_EE++;
                    }
                }
                if( (isBarTrigMSVx==1 || isEndTrigMSVx==1) && isIDVx == 1 ){
                    nMS_ID++;
                    if((isBarTrigMSVx==1 && isIDVx == 1)) nMSB_ID++;
                    if((isEndTrigMSVx==1 && isIDVx == 1)) nMSE_ID++;
                    
                    if((isBarTrigMSVx==1 && isBIDVx == 1)) nMSB_IDB++;
                    if((isBarTrigMSVx==1 && isEIDVx == 1)) nMSB_IDE++;
                    if((isEndTrigMSVx==1 && isBIDVx == 1)) nMSE_IDB++;
                    if((isEndTrigMSVx==1 && isEIDVx == 1)) nMSE_IDE++;
                }
            }
        }
        return kTRUE;
    }
    
    
    void myDecayPositions_TrigFix::SlaveTerminate() {
    }
    
    
    void myDecayPositions_TrigFix::Terminate() {
        TH1F g;
        /*g=*h_BP;
         cout << " nBP: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_ID;
         cout << " nID: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_ECAL;
         cout << " nECAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_HCAL;
         cout << " nHCAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_MS;
         cout << " nMS: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_BPBP;
         cout << " nBPBP: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_BPID;
         cout << " nBPID: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_BPECAL;
         cout << " nBPECAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_BPHCAL;
         cout << " nBPHCAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_BPMS;
         cout << " nBPMS: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_IDECAL;
         cout << " nIDECAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_IDHCAL;
         cout << " nIDHCAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_IDMS;
         cout << " nIDMS: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_ECALECAL;
         cout << " nECALECAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_ECALHCAL;
         cout << " nECALHCAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_ECALMS;
         cout << " nECALMS: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_HCALHCAL;
         cout << " nHCALHCAL: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_HCALMS;
         cout << " nHCALMS: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_MSMS;
         cout << " nMSMS: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         g=*h_Expected_MSVx;
         cout << "Expected triggered + >=1 MSVx events: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
         */
        g=*h_Expected_Trig;
        cout << "Expected triggered events: " << g.GetBinContent(g.FindBin(sim_ctau)) << endl;
        
        int binAt2m = g.FindBin(2.0);
        
        //cout << "WARNING: NOT APPLYING RPC TRIGGER SCALING for BB,BE efficiency histos AT THE MOMENT!" << endl;
        
        cout << "nBB, BE, EE decays, nMSB-ID, MSE-ID decays "  << endl;
        cout << nBB_Decay << ", " << nBE_Decay << ", " << nEE_Decay << ", " << nMSB_ID_Decay << ", " << nMSE_ID_Decay << endl;
        
        cout << "nBB, BE, EE, nMSB-ID, MSE-ID trig evts: " << nBB_Trig << ", " << nBE_Trig << ", " << nEE_Trig << ", " << nMSB_ID_Trig << ", " << nMSE_ID_Trig << endl;
        cout << "nBB, BE, EE trig evts, 0vx: " << nBB_Trig_0vx << ", " << nBE_Trig_0vx << ", " << nEE_Trig_0vx << endl;
        cout << "nBB, BE, EE trig evts, 1vx: " << nBB_Trig_1vx << ", " << nBE_Trig_1vx << ", " << nEE_Trig_1vx << endl;
        cout << "nBB, BE, EE, nMSB-ID, MSE-ID trig evts, 2vx: " << nBB_Trig_2vx << ", " << nBE_Trig_2vx << ", " << nEE_Trig_2vx << ", " << nMSB_ID << ", " << nMSE_ID << endl;
        cout << "BMS-ID EMS-ID BB BE EE" << endl;
        cout << " " << nMSB_ID << " " << nMSE_ID << nBB_Trig_2vx << " " << nBE_Trig_2vx << " " << nEE_Trig_2vx << endl;
        
        cout << "At 2.0 m, raw numbers from 2 million events: "  << endl;
        cout << "BMS-ID EMS-ID BB BE EE" << endl;
        cout <<  h_Expected_BMSVxIDVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EMSVxIDVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BBMSVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BEMSVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EEMSVx->GetBinContent(binAt2m) << endl;
        
        TH1F h;
        cout << "scale factor: " << LUMI*MediatorXS/nEvents << endl;
        
        h_Expected_Trig->Scale(LUMI*MediatorXS/nEvents);
        
        /*  h_Expected_BIDTrig->Scale(LUMI*MediatorXS/nEvents);
         h_Expected_BIDTrig_minSyst->Scale(LUMI*MediatorXS/nEvents);
         h_Expected_BIDTrig_maxSyst->Scale(LUMI*MediatorXS/nEvents);
         for(int i=0; i< 10; i++){
         h_Expected_BIDTrig_minStat[i]->Scale(LUMI*MediatorXS/nEvents);
         h_Expected_BIDTrig_maxStat[i]->Scale(LUMI*MediatorXS/nEvents);
         }
         h_Expected_EIDTrig->Scale(LUMI*MediatorXS/nEvents);
         h_Expected_EIDTrig_minSyst->Scale(LUMI*MediatorXS/nEvents);
         h_Expected_EIDTrig_maxSyst->Scale(LUMI*MediatorXS/nEvents);
         for(int i=0; i<10; i+){
         h_Expected_EIDTrig_minSyst[i]->Scale(LUMI*MediatorXS/nEvents);
         h_Expected_EIDTrig_maxSyst[i]->Scale(LUMI*MediatorXS/nEvents);
         }
         */
        
        
        /*h_Expected_IDVxIDVx->Scale(20300.0*MediatorXS/nEvents/49.3);
         h_IDID_Events->Scale(20300.0*MediatorXS/nEvents);
         h_IDJET_Events->Scale(20300.0*MediatorXS/nEvents);*/
        h_Expected_BBMSVx->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_minBVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_maxBVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_minBVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_maxBVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_minTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_maxTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_minTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BBMSVx_maxTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        
        h_Expected_EEMSVx->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_minEVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_maxEVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_minEVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_maxEVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_minTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_maxTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_minTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EEMSVx_maxTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        
        h_Expected_BEMSVx->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_minEBVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_maxEBVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_minEBVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_maxEBVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_minBEVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_maxBEVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_minBEVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_maxBEVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_minTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_maxTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_minTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BEMSVx_maxTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        
        h_Expected_BMSVxIDVx->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_minTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_maxTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_minTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_maxTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_minIDVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_maxIDVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_minIDVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_maxIDVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_minBMSVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_maxBMSVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_minBMSVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_BMSVxIDVx_maxBMSVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        
        h_Expected_EMSVxIDVx->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_minTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_maxTrigSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_minTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_maxTrigStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_minIDVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_maxIDVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_minIDVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_maxIDVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_minEMSVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_maxEMSVXSyst->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_minEMSVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        h_Expected_EMSVxIDVx_maxEMSVXStatTotal->Scale(LUMI*MediatorXS/nEvents);
        
        h_Expected_2MSVx->Scale(LUMI*MediatorXS/nEvents);
        
        h_Expected_MSVxIDVx->Scale(LUMI*MediatorXS/nEvents);
        
        h_eta->Scale(1./h_eta->GetEntries());
        
        
        cout << "At 2.0 m, scaled by LUMI*MediatorXS/nEvents: " << LUMI << "*" << MediatorXS << "/" << nEvents << endl;
        cout << "BMS-ID EMS-ID BB BE EE" << endl;
        cout <<  h_Expected_BMSVxIDVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EMSVxIDVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BBMSVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BEMSVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EEMSVx->GetBinContent(binAt2m) << endl;
        
        cout << "" << endl;
        
        
        //  double sim_ctau=0;
        
        TString inputFile = GetFileName();
        cout << "inputFile: " << inputFile << endl;
        
        if(inputFile.Contains("mH100_mVPI10")) {
            cout << "Number of events with mH100, mVPI10 and ctau 0.45 m" << endl;
            sim_ctau=0.45;
        } else if(inputFile.Contains("mH100_mVPI25")) {
            cout << "Number of events with mH100, mVPI25 and ctau 1.25 m" << endl;
            sim_ctau=1.25;
        } else if(inputFile.Contains("mH126_mVPI10")||inputFile.Contains("mH126mVPI10")) {
            cout << "Number of events with mH126, mVPI10 and ctau 0.35 m" << endl;
            sim_ctau=0.35;
        } else if(inputFile.Contains("mH126_mVPI25")) {
            cout << "Number of events with mH126, mVPI25 and ctau 0.9 m" << endl;
            sim_ctau=0.9;
        } else if(inputFile.Contains("mH126_mVPI40")) {
            cout << "Number of events with mH126, mVPI40 and ctau 1.85 m" << endl;
            sim_ctau=1.85;
        } else if(inputFile.Contains("mH140_mVPI10")) {
            cout << "Number of events with mH140, mVPI10 and ctau 0.275 m" << endl;
            sim_ctau=0.275;
        } else if(inputFile.Contains("mH140_mVPI20")) {
            cout << "Number of events with mH140, mVPI20 and ctau 0.63 m" << endl;
            sim_ctau=0.63;
        } else if(inputFile.Contains("mH140_mVPI40")) {
            cout << "Number of events with mH140, mVPI40 and ctau 0.45 m" << endl;
            sim_ctau=1.5;
        } else if(inputFile.Contains("mG110") || inputFile.Contains("mg110")){
            cout << "Number of events with mG110, and ctau 2.5 m" << endl;
            sim_ctau=2.5;
        } else if(inputFile.Contains("mG250")|| inputFile.Contains("mg250")) {
            cout << "Number of events with gluino mass = 250 GeV and ctau 2.2 m" << endl;
            sim_ctau = 2.2;
        } else if(inputFile.Contains("mG500")|| inputFile.Contains("mg500")) {
            cout << "Gluino mass is 500 GeV, ctau 1.38 m" << endl;
            sim_ctau = 1.38;
            //sim_ctau = 1.374;
        } else if(inputFile.Contains("mG800")|| inputFile.Contains("mg800")) {
            cout << "Gluino mass is 800 GeV, ctau 0.95 m" << endl;
            sim_ctau = 0.95;
        } else if(inputFile.Contains("mG1200")|| inputFile.Contains("mg1200")) {
            cout << "Gluino mass is 1200 GeV, ctau 0.69 m" << endl;
            sim_ctau = 0.69;
        } else {
            cout << "ERROR! Unrecognized input file: " << inputFile << endl;
        }
        cout << "number of events: " << nEvents << endl;
        cout << "events without 2 LLPs: " << nEventsWithout2LLPs << endl;
        /*  h=*h_MS;
         cout << "  - 1 MS decay " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
         h=*h_MSMS;
         cout << "  - 2 MS decays " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
         h=*h_ID;
         cout << "  - 1 ID decay " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
         h=*h_IDID;
         cout << "  - 2 ID decays " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
         h=*h_IDMS;
         cout << "  - 1 MS + 1 ID decays " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;*/
        
        h=*h_Expected_Trig;
        cout << "  - 1 trigger " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
        //cout << "         Trigger locations (B,E) = (" << nMStrig_B << ", " << nMStrig_E << ")" << endl;
        //h=*h_Expected_MSVx;
        //cout << "  - 1 trigger and 1 MS vertex " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
        h=*h_Expected_2MSVx;
        cout << "  - 1 trigger and 2 MS vertices  " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
        cout << "         2 Vertex locations (BB,BE,EE) -- unscaled = ("
        << nMSvx_BB << ", " << nMSvx_BE << ", " << nMSvx_EE << ")" << endl;
        cout << "         2 Vertex locations (MS-ID, MSB-IDB, MSB-IDE, MSE-IDB, MSE-IDE) -- unscaled = ("
        << nMS_ID << ", " << nMSB_IDB << ", " << nMSB_IDE << ", " << nMSE_IDB << ", " << nMSE_IDE << ")" << endl;
        
        cout << "         2 Vertex locations (BB,BE,EE) -- scaled by lumi and x-sec = ("
        << nMSvx_BB*(LUMI*MediatorXS/nEvents) << ", " << nMSvx_BE*(LUMI*MediatorXS/nEvents) << ", " << nMSvx_EE*(LUMI*MediatorXS/nEvents) << ")" << endl;
        
        h=*h_Expected_MSVxIDVx;
        cout << "  - 1 trigger and 1 MS + 1 ID vertices  " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
        h=*h_Expected_Tot2Vx;
        cout << "  - 1 trigger and 2 vertices  " << h.GetBinContent(h.FindBin(sim_ctau)) << endl;
        
        /*
         cout << "barrel endcap trig_barrel trig_endcap trigvtx_barrel trigvtx_endcap notrigvtx_barrel notrigvtx_endcap" << endl;
         
         for(int k = 0; k < 30; k++){
         cout << vpiBarrelMS[k] << " " << vpiEndcapMS_R10[k] << " " << BarrelRoI[k] << " " << EndcapRoI[k] << " " << vpiBarrelMS_TrigVtx[k] << " " << vpiEndcapMS_TrigVtx[k] << " " << vpiBarrelMS_NoTrigVtx[k] << " " << vpiEndcapMS_NoTrigVtx[k] << endl;
         }*/
        
        
        //Calculate systematic errors!
        
        double finestep = LTSTEP;
        double ctau=0;
        
        
        vector<double> binBVx_min(3,0.0);
        vector<double> binBVx_max(3,0.0);
        vector<double> binEVx_min(3,0.0);
        vector<double> binEVx_max(3,0.0);
        vector<double> binBTrig_min(3,0.0);
        vector<double> binBTrig_max(3,0.0);
        vector<double> binETrig_min(2,0.0);
        vector<double> binETrig_max(2,0.0);
        vector<double> binIDVx_min(2,0.0);
        vector<double> binIDVx_max(2,0.0);
        
        vector<double> binStat_min(13,0.0);
        vector<double> binStat_max(13,0.0);
        
        
        vector<double> binBBMSVx_min(4,0.0);
        vector<double> binBBMSVx_max(4,0.0);
        vector<double> binEEMSVx_min(4,0.0);
        vector<double> binEEMSVx_max(4,0.0);
        vector<double> binBEMSVx_min(6,0.0);
        vector<double> binBEMSVx_max(6,0.0);
        
        vector<double> binBMSVxIDVx_min(6,0.0);
        vector<double> binBMSVxIDVx_max(6,0.0);
        vector<double> binEMSVxIDVx_min(6,0.0);
        vector<double> binEMSVxIDVx_max(6,0.0);
        
        //  vector<double> binTotal_min_combined(6,0.0);
        //  vector<double> binTotal_max_combined(6,0.0);
        
        for(int i=1; i<(1201); ++i) {
            
            //ctau = (i-1)*finestep+0.0001;
            
            float BIDVXtrigVal = h_Expected_BMSVxIDVx->GetBinContent(i);
            float BIDVXStatMax = h_Expected_BMSVxIDVx_maxTrigStatTotal->GetBinContent(i);
            float BIDVXStatMin = h_Expected_BMSVxIDVx_minTrigStatTotal->GetBinContent(i);
            float BmsVXStatMax = h_Expected_BMSVxIDVx_maxBMSVXStatTotal->GetBinContent(i);
            float BmsVXStatMin = h_Expected_BMSVxIDVx_minBMSVXStatTotal->GetBinContent(i);
            
            binStat_max.at(0) = (BIDVXStatMax - BIDVXtrigVal);
            binStat_max.at(1) = (BmsVXStatMax - BIDVXtrigVal);
            
            binStat_min.at(0) = (BIDVXtrigVal - BIDVXStatMin);
            binStat_min.at(1) = (BIDVXtrigVal - BmsVXStatMin);
            
            float EIDVXtrigVal = h_Expected_EMSVxIDVx->GetBinContent(i);
            float EIDVXidStatMax = h_Expected_EMSVxIDVx_maxIDVXStatTotal->GetBinContent(i);
            float EIDVXidStatMin = h_Expected_EMSVxIDVx_minIDVXStatTotal->GetBinContent(i);
            float BIDVXidStatMax = h_Expected_BMSVxIDVx_maxIDVXStatTotal->GetBinContent(i);
            float BIDVXidStatMin = h_Expected_BMSVxIDVx_minIDVXStatTotal->GetBinContent(i);
            
            binStat_max.at(2) = (BIDVXidStatMax - BIDVXtrigVal);
            binStat_max.at(3) = (EIDVXidStatMax - EIDVXtrigVal);
            
            binStat_min.at(2) = (BIDVXtrigVal - BIDVXidStatMin);
            binStat_min.at(3) = (EIDVXtrigVal - EIDVXidStatMin);
            
            float EIDVXStatMax = h_Expected_EMSVxIDVx_maxTrigStatTotal->GetBinContent(i);
            float EIDVXStatMin = h_Expected_EMSVxIDVx_minTrigStatTotal->GetBinContent(i);
            float EmsVXStatMax = h_Expected_EMSVxIDVx_maxEMSVXStatTotal->GetBinContent(i);
            float EmsVXStatMin = h_Expected_EMSVxIDVx_minEMSVXStatTotal->GetBinContent(i);
            
            binStat_max.at(4) = (EIDVXStatMax - EIDVXtrigVal);
            binStat_max.at(5) = (EmsVXStatMax - EIDVXtrigVal);
            
            binStat_min.at(4) = (EIDVXtrigVal - EIDVXStatMin);
            binStat_min.at(5) = (EIDVXtrigVal - EmsVXStatMin);
            
            float BBMSVxVal = h_Expected_BBMSVx->GetBinContent(i);
            float BBtrigStatMax = h_Expected_BBMSVx_maxTrigStatTotal->GetBinContent(i);
            float BBtrigStatMin = h_Expected_BBMSVx_minTrigStatTotal->GetBinContent(i);
            float BBVxStatMax =   h_Expected_BBMSVx_maxBVXStatTotal->GetBinContent(i);
            float BBVxStatMin = h_Expected_BBMSVx_minBVXStatTotal->GetBinContent(i);
            
            binStat_max.at(6) = (BBtrigStatMax - BBMSVxVal);
            binStat_max.at(7) = (BBVxStatMax - BBMSVxVal);
            
            binStat_min.at(6) = (BBMSVxVal - BBtrigStatMin);
            binStat_min.at(7) = (BBMSVxVal - BBVxStatMin);
            
            float BEVXVal = h_Expected_BEMSVx->GetBinContent(i);
            float BEtrigStatMax =  h_Expected_BEMSVx_maxTrigStatTotal->GetBinContent(i);
            float BEtrigStatMin = h_Expected_BEMSVx_minTrigStatTotal->GetBinContent(i);
            float BEvxStatMax = h_Expected_BEMSVx_maxBEVXStatTotal->GetBinContent(i);
            float BEvxStatMin = h_Expected_BEMSVx_minBEVXStatTotal->GetBinContent(i);
            float EBvxStatMax =  h_Expected_BEMSVx_maxEBVXStatTotal->GetBinContent(i);
            float EBvxStatMin = h_Expected_BEMSVx_minEBVXStatTotal->GetBinContent(i);
            
            binStat_max.at(8) = (BEtrigStatMax - BEVXVal);
            binStat_max.at(9) = (BEvxStatMax - BEVXVal);
            binStat_max.at(10) = (EBvxStatMax - BEVXVal);
            
            binStat_min.at(8) = (BEVXVal - BEtrigStatMin);
            binStat_min.at(9) = (BEVXVal - BEvxStatMin);
            binStat_min.at(10) = (BEVXVal - EBvxStatMin);
            
            float EEvxVal = h_Expected_EEMSVx->GetBinContent(i);
            float EETrigStatMax = h_Expected_EEMSVx_maxTrigStatTotal->GetBinContent(i);
            float EETrigStatMin = h_Expected_EEMSVx_maxTrigStatTotal->GetBinContent(i);
            float EEvxStatMax = h_Expected_EEMSVx_maxEVXStatTotal->GetBinContent(i);
            float EEvxStatMin = h_Expected_EEMSVx_maxEVXStatTotal->GetBinContent(i);
            
            binStat_max.at(11) = (EETrigStatMax - EEvxVal);
            binStat_max.at(12) = (EEvxStatMax - EEvxVal);
            
            binStat_min.at(11) = (EEvxVal - EETrigStatMin);
            binStat_min.at(12) = (EEvxVal - EEvxStatMin);
            
            double binBBMSVx = 0.0;
            binBBMSVx = h_Expected_BBMSVx->GetBinContent(i);
            
            binBBMSVx_min.at(0) = (h_Expected_BBMSVx_minTrigSyst->GetBinContent(i));
            binBBMSVx_min.at(1) = (h_Expected_BBMSVx_minTrigStatTotal->GetBinContent(i));
            binBBMSVx_min.at(2) = (h_Expected_BBMSVx_minBVXSyst->GetBinContent(i));
            binBBMSVx_min.at(3) = (h_Expected_BBMSVx_minBVXStatTotal->GetBinContent(i));
            
            binBBMSVx_max.at(0) = (h_Expected_BBMSVx_maxTrigSyst->GetBinContent(i));
            binBBMSVx_max.at(1) = (h_Expected_BBMSVx_maxTrigStatTotal->GetBinContent(i));
            binBBMSVx_max.at(2) = (h_Expected_BBMSVx_maxBVXSyst->GetBinContent(i));
            binBBMSVx_max.at(3) = (h_Expected_BBMSVx_maxBVXStatTotal->GetBinContent(i));
            
            
            double binEEMSVx = 0.0;
            binEEMSVx = (h_Expected_EEMSVx->GetBinContent(i));
            
            binEEMSVx_min.at(0) = (h_Expected_EEMSVx_minTrigSyst->GetBinContent(i));
            binEEMSVx_min.at(1) = (h_Expected_EEMSVx_minTrigStatTotal->GetBinContent(i));
            binEEMSVx_min.at(2) = (h_Expected_EEMSVx_minEVXSyst->GetBinContent(i));
            binEEMSVx_min.at(3) = (h_Expected_EEMSVx_minEVXStatTotal->GetBinContent(i));
            
            binEEMSVx_max.at(0) = (h_Expected_EEMSVx_maxTrigSyst->GetBinContent(i));
            binEEMSVx_max.at(1) = (h_Expected_EEMSVx_maxTrigStatTotal->GetBinContent(i));
            binEEMSVx_max.at(2) = (h_Expected_EEMSVx_maxEVXSyst->GetBinContent(i));
            binEEMSVx_max.at(3) = (h_Expected_EEMSVx_maxEVXStatTotal->GetBinContent(i));
            
            for(int j=0; j< binEEMSVx_min.size(); j++){
                binEEMSVx_max.at(j) = binEEMSVx_max.at(j) -  binEEMSVx;
                binEEMSVx_min.at(j) = binEEMSVx - binEEMSVx_min.at(j);
                binBBMSVx_max.at(j) = binBBMSVx_max.at(j) -  binBBMSVx;
                binBBMSVx_min.at(j) = binBBMSVx - binBBMSVx_min.at(j);
            }
            
            binBVx_min.at(0) = binBBMSVx_min.at(2);
            binBVx_max.at(0) = binBBMSVx_max.at(2);
            binBTrig_min.at(0) = binBBMSVx_min.at(0);
            binBTrig_max.at(0) = binBBMSVx_max.at(0);
            
            binEVx_min.at(0) = binEEMSVx_min.at(2);
            binEVx_max.at(0) = binEEMSVx_max.at(2);
            binETrig_min.at(0) = binEEMSVx_min.at(0);
            binETrig_max.at(0) = binEEMSVx_max.at(0);
            
            /* binTotal_max_combined.at(0)+= binBBMSVx_max.at(0);
             binTotal_max_combined.at(1)+= binEEMSVx_max.at(0);
             binTotal_max_combined.at(4)+= binBBMSVx_max.at(2);
             binTotal_max_combined.at(5)+= binEEMSVx_max.at(2);
             binTotal_min_combined.at(0)+= binBBMSVx_min.at(0);
             binTotal_min_combined.at(1)+= binEEMSVx_min.at(0);
             binTotal_min_combined.at(4)+= binBBMSVx_max.at(2);
             binTotal_min_combined.at(5)+= binEEMSVx_min.at(2);*/
            
            double binBEMSVx = 0.0;
            binBEMSVx = h_Expected_BEMSVx->GetBinContent(i);
            
            binBEMSVx_min.at(0) = (h_Expected_BEMSVx_minTrigSyst->GetBinContent(i));
            binBEMSVx_min.at(1) = (h_Expected_BEMSVx_minTrigStatTotal->GetBinContent(i));
            binBEMSVx_min.at(2) = (h_Expected_BEMSVx_minBEVXSyst->GetBinContent(i));
            binBEMSVx_min.at(3) = (h_Expected_BEMSVx_minBEVXStatTotal->GetBinContent(i));
            binBEMSVx_min.at(4) = (h_Expected_BEMSVx_minEBVXSyst->GetBinContent(i));
            binBEMSVx_min.at(5) = (h_Expected_BEMSVx_minEBVXStatTotal->GetBinContent(i));
            
            binBEMSVx_max.at(0) = (h_Expected_BEMSVx_maxTrigSyst->GetBinContent(i));
            binBEMSVx_max.at(1) = (h_Expected_BEMSVx_maxTrigStatTotal->GetBinContent(i));
            binBEMSVx_max.at(2) = (h_Expected_BEMSVx_maxBEVXSyst->GetBinContent(i));
            binBEMSVx_max.at(3) = (h_Expected_BEMSVx_maxBEVXStatTotal->GetBinContent(i));
            binBEMSVx_max.at(4) = (h_Expected_BEMSVx_maxEBVXSyst->GetBinContent(i));
            binBEMSVx_max.at(5) = (h_Expected_BEMSVx_maxEBVXStatTotal->GetBinContent(i));
            
            double binBMSVxIDVx = 0.0;
            binBMSVxIDVx = h_Expected_BMSVxIDVx->GetBinContent(i);
            
            binBMSVxIDVx_min.at(0) = (h_Expected_BMSVxIDVx_minTrigSyst->GetBinContent(i));
            binBMSVxIDVx_min.at(1) = (h_Expected_BMSVxIDVx_minTrigStatTotal->GetBinContent(i));
            binBMSVxIDVx_min.at(2) = (h_Expected_BMSVxIDVx_minBMSVXSyst->GetBinContent(i));
            binBMSVxIDVx_min.at(3) = (h_Expected_BMSVxIDVx_minBMSVXStatTotal->GetBinContent(i));
            binBMSVxIDVx_min.at(4) = (h_Expected_BMSVxIDVx_minIDVXSyst->GetBinContent(i));
            binBMSVxIDVx_min.at(5) = (h_Expected_BMSVxIDVx_minIDVXStatTotal->GetBinContent(i));
            
            binBMSVxIDVx_max.at(0) = (h_Expected_BMSVxIDVx_maxTrigSyst->GetBinContent(i));
            binBMSVxIDVx_max.at(1) = (h_Expected_BMSVxIDVx_maxTrigStatTotal->GetBinContent(i));
            binBMSVxIDVx_max.at(2) = (h_Expected_BMSVxIDVx_maxBMSVXSyst->GetBinContent(i));
            binBMSVxIDVx_max.at(3) = (h_Expected_BMSVxIDVx_maxBMSVXStatTotal->GetBinContent(i));
            binBMSVxIDVx_max.at(4) = (h_Expected_BMSVxIDVx_maxIDVXSyst->GetBinContent(i));
            binBMSVxIDVx_max.at(5) = (h_Expected_BMSVxIDVx_maxIDVXStatTotal->GetBinContent(i));
            
            double binEMSVxIDVx = 0.0;
            binEMSVxIDVx = h_Expected_EMSVxIDVx->GetBinContent(i);
            
            binEMSVxIDVx_min.at(0) = (h_Expected_EMSVxIDVx_minTrigSyst->GetBinContent(i));
            binEMSVxIDVx_min.at(1) = (h_Expected_EMSVxIDVx_minTrigStatTotal->GetBinContent(i));
            binEMSVxIDVx_min.at(2) = (h_Expected_EMSVxIDVx_minEMSVXSyst->GetBinContent(i));
            binEMSVxIDVx_min.at(3) = (h_Expected_EMSVxIDVx_minEMSVXStatTotal->GetBinContent(i));
            binEMSVxIDVx_min.at(4) = (h_Expected_EMSVxIDVx_minIDVXSyst->GetBinContent(i));
            binEMSVxIDVx_min.at(5) = (h_Expected_EMSVxIDVx_minIDVXStatTotal->GetBinContent(i));
            
            binEMSVxIDVx_max.at(0) = (h_Expected_EMSVxIDVx_maxTrigSyst->GetBinContent(i));
            binEMSVxIDVx_max.at(1) = (h_Expected_EMSVxIDVx_maxTrigStatTotal->GetBinContent(i));
            binEMSVxIDVx_max.at(2) = (h_Expected_EMSVxIDVx_maxEMSVXSyst->GetBinContent(i));
            binEMSVxIDVx_max.at(3) = (h_Expected_EMSVxIDVx_maxEMSVXStatTotal->GetBinContent(i));
            binEMSVxIDVx_max.at(4) = (h_Expected_EMSVxIDVx_maxIDVXSyst->GetBinContent(i));
            binEMSVxIDVx_max.at(5) = (h_Expected_EMSVxIDVx_maxIDVXStatTotal->GetBinContent(i));
            
            for(int j=0; j< binBEMSVx_min.size(); j++){
                binBEMSVx_max.at(j) = binBEMSVx_max.at(j) -  binBEMSVx;
                binBEMSVx_min.at(j) = binBEMSVx - binBEMSVx_min.at(j);
                binBMSVxIDVx_max.at(j) = binBMSVxIDVx_max.at(j) -  binBMSVxIDVx;
                binBMSVxIDVx_min.at(j) = binBMSVxIDVx - binBMSVxIDVx_min.at(j);
                binEMSVxIDVx_max.at(j) = binEMSVxIDVx_max.at(j) -  binEMSVxIDVx;
                binEMSVxIDVx_min.at(j) = binEMSVxIDVx - binEMSVxIDVx_min.at(j);
            }
            
            binBVx_min.at(1) = binBEMSVx_min.at(2);
            binBVx_max.at(1) = binBEMSVx_max.at(2);
            binBVx_min.at(2) = binBMSVxIDVx_min.at(2);
            binBVx_max.at(2) = binBMSVxIDVx_max.at(2);
            
            binBTrig_min.at(1) = binBEMSVx_min.at(0);
            binBTrig_max.at(1) = binBEMSVx_max.at(0);
            binBTrig_min.at(2) = binBMSVxIDVx_min.at(0);
            binBTrig_max.at(2) = binBMSVxIDVx_max.at(0);
            
            binEVx_min.at(1) = binBEMSVx_min.at(4);
            binEVx_max.at(1) = binBEMSVx_max.at(4);
            binEVx_min.at(2) = binEMSVxIDVx_min.at(2);
            binEVx_max.at(2) = binEMSVxIDVx_max.at(2);
            
            binETrig_min.at(1) = binEMSVxIDVx_min.at(0);
            binETrig_max.at(1) = binEMSVxIDVx_max.at(0);
            
            binIDVx_min.at(0) = binEMSVxIDVx_min.at(4);
            binIDVx_max.at(0) = binEMSVxIDVx_max.at(4);
            binIDVx_min.at(1) = binEMSVxIDVx_min.at(4);
            binIDVx_max.at(1) = binEMSVxIDVx_max.at(4);
            
            
            double totBTrigsystMAX = std::accumulate(binBTrig_max.rbegin(),binBTrig_max.rend(),0.0);
            double totBTrigsystMIN = std::accumulate(binBTrig_min.rbegin(),binBTrig_min.rend(),0.0);
            double totETrigsystMAX = std::accumulate(binETrig_max.rbegin(),binETrig_max.rend(),0.0);
            double totETrigsystMIN = std::accumulate(binETrig_min.rbegin(),binETrig_min.rend(),0.0);
            
            double totBVXsystMAX = std::accumulate(binBVx_max.rbegin(),binBVx_max.rend(),0.0);
            double totBVXsystMIN = std::accumulate(binBVx_min.rbegin(),binBVx_min.rend(),0.0);
            double totEVXsystMAX = std::accumulate(binEVx_max.rbegin(),binEVx_max.rend(),0.0);
            double totEVXsystMIN = std::accumulate(binEVx_min.rbegin(),binEVx_min.rend(),0.0);
            double totIDVXsystMAX = std::accumulate(binIDVx_max.rbegin(),binIDVx_max.rend(),0.0);
            double totIDVXsystMIN = std::accumulate(binIDVx_min.rbegin(),binIDVx_min.rend(),0.0);
            
            double totStatMAX = sqrt(std::inner_product(binStat_max.begin(),binStat_max.end(),binStat_max.begin(),0.0));
            double totStatMIN = sqrt(std::inner_product(binStat_min.begin(),binStat_min.end(),binStat_min.begin(),0.0));
            
            double totSystMAX = sqrt(sq(totBTrigsystMAX) + sq(totETrigsystMAX) + sq(totBVXsystMAX) + sq(totEVXsystMAX)+ sq(totIDVXsystMAX));
            double totSystMIN = sqrt(sq(totBTrigsystMIN) + sq(totETrigsystMIN) + sq(totBVXsystMIN) + sq(totEVXsystMIN)+ sq(totIDVXsystMIN));
            
            double tmpMAXq = std::inner_product(binBTrig_max.begin(),binBTrig_max.end(),binBTrig_max.begin(),0.0);
            tmpMAXq += std::inner_product(binETrig_max.begin(),binETrig_max.end(),binETrig_max.begin(),0.0);
            tmpMAXq += std::inner_product(binEVx_max.begin(),binEVx_max.end(),binEVx_max.begin(),0.0);
            tmpMAXq += std::inner_product(binBVx_max.begin(),binBVx_max.end(),binBVx_max.begin(),0.0);
            tmpMAXq += std::inner_product(binIDVx_max.begin(),binIDVx_max.end(),binIDVx_max.begin(),0.0);
            
            double tmpMINq = std::inner_product(binBTrig_min.begin(),binBTrig_min.end(),binBTrig_min.begin(),0.0);
            tmpMINq += std::inner_product(binETrig_min.begin(),binETrig_min.end(),binETrig_min.begin(),0.0);
            tmpMINq += std::inner_product(binEVx_min.begin(),binEVx_min.end(),binEVx_min.begin(),0.0);
            tmpMINq += std::inner_product(binBVx_min.begin(),binBVx_min.end(),binBVx_min.begin(),0.0);
            tmpMINq += std::inner_product(binIDVx_min.begin(),binIDVx_min.end(),binIDVx_min.begin(),0.0);
            
            double totSystMAXq = sqrt(tmpMAXq);
            double totSystMINq = sqrt(tmpMINq);
            
            double totBBErrorMAX = sqrt( std::inner_product(binBBMSVx_max.begin(),binBBMSVx_max.end(),binBBMSVx_max.begin(),0.0));
            double totBBErrorMIN = sqrt( std::inner_product(binBBMSVx_min.begin(),binBBMSVx_min.end(),binBBMSVx_min.begin(),0.0));
            
            double totEEErrorMAX = sqrt( std::inner_product(binEEMSVx_max.begin(),binEEMSVx_max.end(),binEEMSVx_max.begin(),0.0));
            double totEEErrorMIN = sqrt( std::inner_product(binEEMSVx_min.begin(),binEEMSVx_min.end(),binEEMSVx_min.begin(),0.0));
            
            double totBEErrorMAX = sqrt( std::inner_product(binBEMSVx_max.begin(),binBEMSVx_max.end(),binBEMSVx_max.begin(),0.0));
            double totBEErrorMIN = sqrt( std::inner_product(binBEMSVx_min.begin(),binBEMSVx_min.end(),binBEMSVx_min.begin(),0.0));
            
            double totBMSIDErrorMAX = sqrt( std::inner_product(binBMSVxIDVx_max.begin(),binBMSVxIDVx_max.end(),binBMSVxIDVx_max.begin(),0.0));
            double totBMSIDErrorMIN = sqrt( std::inner_product(binBMSVxIDVx_min.begin(),binBMSVxIDVx_min.end(),binBMSVxIDVx_min.begin(),0.0));
            
            double totEMSIDErrorMAX = sqrt( std::inner_product(binEMSVxIDVx_max.begin(),binEMSVxIDVx_max.end(),binEMSVxIDVx_max.begin(),0.0));
            double totEMSIDErrorMIN = sqrt( std::inner_product(binEMSVxIDVx_min.begin(),binEMSVxIDVx_min.end(),binEMSVxIDVx_min.begin(),0.0));
            
            double totErrorMAX = sqrt( sq(totStatMAX) + sq(totSystMAX));
            double totErrorMIN = sqrt( sq(totStatMIN) + sq(totSystMIN));
            
            double totErrorMAXq = sqrt( sq(totStatMAX) + sq(totSystMAXq));
            double totErrorMINq = sqrt( sq(totStatMIN) + sq(totSystMINq));
            
            
            h_Expected_BBMSVx_maxTotal->SetBinContent(i,binBBMSVx+totBBErrorMAX);
            h_Expected_BBMSVx_minTotal->SetBinContent(i,binBBMSVx-totBBErrorMIN);
            h_Expected_EEMSVx_maxTotal->SetBinContent(i,binEEMSVx+totEEErrorMAX);
            h_Expected_EEMSVx_minTotal->SetBinContent(i,binEEMSVx-totEEErrorMIN);
            h_Expected_BEMSVx_maxTotal->SetBinContent(i,binBEMSVx+totBEErrorMAX);
            h_Expected_BEMSVx_minTotal->SetBinContent(i,binBEMSVx-totBEErrorMIN);
            h_Expected_BMSVxIDVx_maxTotal->SetBinContent(i,binBMSVxIDVx+totBMSIDErrorMAX);
            h_Expected_BMSVxIDVx_minTotal->SetBinContent(i,binBMSVxIDVx-totBMSIDErrorMIN);
            h_Expected_EMSVxIDVx_maxTotal->SetBinContent(i,binEMSVxIDVx+totEMSIDErrorMAX);
            h_Expected_EMSVxIDVx_minTotal->SetBinContent(i,binEMSVxIDVx-totEMSIDErrorMIN);
            
            h_Expected_Tot2Vx->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx);
            h_Expected_Tot2Vx_maxStatTotal->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx+totStatMAX);
            h_Expected_Tot2Vx_maxSystTotal->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx+totSystMAXq);
            h_Expected_Tot2Vx_maxSystLinear->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx+totSystMAX);
            h_Expected_Tot2Vx_maxTotal->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx+totErrorMAXq);
            h_Expected_Tot2Vx_maxTotalLin->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx+totErrorMAX);
            h_Expected_Tot2Vx_minStatTotal->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx-totStatMIN);
            h_Expected_Tot2Vx_minSystTotal->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx-totSystMINq);
            h_Expected_Tot2Vx_minSystLinear->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx-totSystMIN);
            h_Expected_Tot2Vx_minTotal->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx-totErrorMINq);
            h_Expected_Tot2Vx_minTotalLin->SetBinContent(i,binBBMSVx+binEEMSVx+binBEMSVx+binBMSVxIDVx+binEMSVxIDVx-totErrorMIN);
        }
        
        cout << "At 2.0 m, scaled by LUMI*MediatorXS/nEvents: " << LUMI << "*" << MediatorXS << "/" << nEvents << endl;
        cout << "BMS-ID EMS-ID BB BE EE TOTAL" << endl;
        cout <<  h_Expected_BMSVxIDVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EMSVxIDVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BBMSVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BEMSVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EEMSVx->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_Tot2Vx->GetBinContent(binAt2m) << endl;
        cout << " " << endl;
        cout << "Max at this value: " << endl;
        cout <<  h_Expected_BMSVxIDVx_maxTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EMSVxIDVx_maxTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BBMSVx_maxTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BEMSVx_maxTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EEMSVx_maxTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_Tot2Vx_maxTotal->GetBinContent(binAt2m) << endl;
        cout << " " << endl;
        cout << "Min at this value: " << endl;
        cout <<  h_Expected_BMSVxIDVx_minTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EMSVxIDVx_minTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BBMSVx_minTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_BEMSVx_minTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_EEMSVx_minTotal->GetBinContent(binAt2m);
        cout << " " <<   h_Expected_Tot2Vx_minTotal->GetBinContent(binAt2m) << endl;
        cout << " " << endl;
        
        
        h_barrel_gamma_50->Scale(1./h_barrel_gamma_50->GetEntries());
        h_endcap_gamma_50->Scale(1./h_endcap_gamma_50->GetEntries());
        h_barrel_gamma_025->Scale(1./h_barrel_gamma_025->GetEntries());
        h_endcap_gamma_025->Scale(1./h_endcap_gamma_025->GetEntries());
        
        pFileRoot->Write();
        
        delete vpion_e;
        delete vpion_px;
        delete vpion_py;
        delete vpion_pz;
    }
    
    double myDecayPositions_TrigFix::triggerProb(float llp_beta, float llp_dist){
        
        double deltaT = llp_dist*(1/llp_beta - 1)/c;
        
        double eff_DATA = 0.5*TMath::Erfc((deltaT - delta_DATA)/(sqrt(2.0)*sigma_DATA));
        
        return eff_DATA;
    }
    float myDecayPositions_TrigFix::deltaR(float eta1,float eta2, float phi1, float phi2) {
        float deta = eta1-eta2;
        float dphi = phi1-phi2;
        if(dphi > TMath::Pi()) dphi -= 2*TMath::Pi();
        else if(dphi < -TMath::Pi()) dphi += 2*TMath::Pi();
        
        float dR = sqrt(sq(deta)+sq(dphi));
        
        return dR;
    }
    
    double myDecayPositions_TrigFix::triggerPassProb(vector<TVector3>& vpi_loc, vector<TLorentzVector>& vpi_p, int& k){
        
        if(vpi_loc.size() != vpi_p.size()){
            cout << "ERROR!: have " << vpi_loc.size() << " llp decay locations, but " << vpi_p.size() << " 4-momenta" << endl;
            return -1;
        }
        
        if(isBadTopology(vpi_loc)) return -1;
        
        ///////////////////////////////////////////////////////
        //* Check if Trigger detects the v-pion *//
        
        double prob = rnd.Rndm();
        if(isBB_Event(vpi_loc)){
            
            if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k){
                nBB_Decay++;
                if(nEvents < 10) cout << "sim_ctau: " << sim_ctau << " and k: " << k << " and ctau " << k*LTSTEP+0.0001 << endl;
            }
            //////////////////////////////////////////////////////////////
            //* Calculate the v-pion time delay wrt the bunch crossing *//
            bool isGoodTrigger(false);
            double smallest_delT = 999.;
            int smallest_delT_index = -1;
            for(int i_vp = 0; i_vp < vpi_loc.size(); i_vp++){
                double tmp_delT = vpi_loc[i_vp].Mag()*(1.0/vpi_p[i_vp].Beta()-1.0)/c;
                if(tmp_delT < smallest_delT){
                    smallest_delT = tmp_delT;
                    smallest_delT_index = i_vp;
                }
            }
            
            double probTrig = rnd.Rndm();
            double eff_DATA = triggerProb(vpi_p[smallest_delT_index].Beta(),vpi_loc[smallest_delT_index].Mag());
            
            if(probTrig < eff_DATA) isGoodTrigger=true;
            
            if(isGoodTrigger) {
                return prob;
            } else return -1;
            
        } else if(isBE_Event(vpi_loc)){
            if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k) nBE_Decay++;
            //need some sort of trigger scalaing here?!? naw, just ignore for now...
            
            //find which decay is in the barrel
            int b_dec_index = -1;
            int e_dec_index = -1;
            if(checkDecLoc(vpi_loc[0]) == 1){
                b_dec_index = 0; e_dec_index = 1;
            } else{
                b_dec_index = 1; e_dec_index = 0;
            }
            bool isGoodTrigger(false);
            
            double endcap_delT = vpi_loc[e_dec_index].Mag()*(1.0/vpi_p[e_dec_index].Beta()-1.0)/c;
            
            if(endcap_delT < 25){
                return prob;
            } else{
                double probTrig = rnd.Rndm();
                double eff_DATA = triggerProb(vpi_p[b_dec_index].Beta(),vpi_loc[b_dec_index].Mag());
                
                if(probTrig < eff_DATA) isGoodTrigger=true;
                
                if(isGoodTrigger) {
                    return prob;
                } else return -1;
                
            }
            
        } else if(isEE_Event(vpi_loc)){
            if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k) nEE_Decay++;
            bool isGoodTrigger(false);
            double smallest_delT = 999.;
            int smallest_delT_index = -1;
            for(int i_vp = 0; i_vp < vpi_loc.size(); i_vp++){
                double tmp_delT = vpi_loc[i_vp].Mag()*(1.0/vpi_p[i_vp].Beta()-1.0)/c;
                if(tmp_delT < smallest_delT){
                    smallest_delT = tmp_delT;
                    smallest_delT_index = i_vp;
                }
            }
            if(smallest_delT < 25) isGoodTrigger=true;
            
            if(isGoodTrigger) {
                return prob;
            } else return -1;
            
        } else if(isBID_Event(vpi_loc)){
            if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k) nMSB_ID_Decay++;
            //find which decay is in the MS
            int b_dec_index = -1;
            int id_dec_index = -1;
            if(checkDecLoc(vpi_loc[0]) == 1){
                b_dec_index = 0; id_dec_index = 1;
            } else{
                b_dec_index = 1; id_dec_index = 0;
            }
            
            bool isGoodTrigger(false);
            
            double probTrig = rnd.Rndm();
            double eff_DATA = triggerProb(vpi_p[b_dec_index].Beta(),vpi_loc[b_dec_index].Mag());
            
            if(probTrig < eff_DATA) isGoodTrigger=true;
            
            if(isGoodTrigger) {
                return prob;
            } else return -1;
            
        } else if(isEID_Event(vpi_loc)){
            if((h_Expected_2MSVx->FindBin(sim_ctau)-1) == k) nMSE_ID_Decay++;
            //find which decay is in the MS
            int e_dec_index = -1;
            int id_dec_index = -1;
            if(checkDecLoc(vpi_loc[0]) == 2){
                e_dec_index = 0; id_dec_index = 1;
            } else{
                e_dec_index = 1; id_dec_index = 0;
            }
            
            bool isGoodTrigger=false;
            double endcap_delT = vpi_loc[e_dec_index].Mag()*(1.0/vpi_p[e_dec_index].Beta()-1.0)/c;
            
            if(endcap_delT < 25) isGoodTrigger = true;
            
            if(isGoodTrigger) {
                return prob;
            } else return -1;
        } else{
            return -1;
        }
        
        return -1;
    }
    int myDecayPositions_TrigFix::checkDecLoc(TVector3& vpi_loc){
        double eta_l = TMath::Abs(vpi_loc.Eta());
        if(eta_l > 2.5 || vpi_loc.Perp() > 10000. || TMath::Abs(vpi_loc.z()) > 15000.) return 0;
        else if(eta_l < 1.0 && vpi_loc.Perp() < 8000. && vpi_loc.Perp() > 3000.) return 1;
        else if(eta_l > 1.0 && eta_l < 2.5 && vpi_loc.Perp() < 10000. && TMath::Abs(vpi_loc.z()) > 5000. && TMath::Abs(vpi_loc.z()) < 15000.) return 2;
        else if(eta_l < 2.5 && vpi_loc.Perp() < 275. && TMath::Abs(vpi_loc.z()) < 840. ) return 3;
        //else if(eta_l < 2.9 && vpi_loc.Perp() < 3400.) return 5;
        else return 0;
    }
    bool myDecayPositions_TrigFix::isBB_Event(vector<TVector3>& vpi_locs){
        if(vpi_locs.size() < 2) return false;
        int nBarrel = 0;
        for(int i_vp=0; i_vp < vpi_locs.size(); i_vp++){
            if( checkDecLoc(vpi_locs[i_vp]) == 1 ) nBarrel++;
        }
        double dec_dR = vpi_locs[0].DeltaR(vpi_locs[1]);
        if(nBarrel == 2 && dec_dR > 2.0) return true;
        else return false;
    }
    bool myDecayPositions_TrigFix::isBE_Event(vector<TVector3>& vpi_locs){
        if(vpi_locs.size() < 2) return false;
        int nBarrel = 0;
        int nEndcap =0;
        for(int i_vp=0; i_vp < vpi_locs.size(); i_vp++){
            if( checkDecLoc(vpi_locs[i_vp]) == 1 ) nBarrel++;
            if( checkDecLoc(vpi_locs[i_vp]) == 2 ) nEndcap++;
        }
        double dec_dR = vpi_locs[0].DeltaR(vpi_locs[1]);
        if(nBarrel == 1 && nEndcap == 1 && dec_dR > 2.0) return true;
        else return false;
    }
    bool myDecayPositions_TrigFix::isEE_Event(vector<TVector3>& vpi_locs){
        if(vpi_locs.size() < 2) return false;
        int nEndcap = 0;
        for(int i_vp=0; i_vp < vpi_locs.size(); i_vp++){
            if( checkDecLoc(vpi_locs[i_vp]) == 2 ) nEndcap++;
        }
        double dec_dR = vpi_locs[0].DeltaR(vpi_locs[1]);
        if(nEndcap == 2 && dec_dR > 2.0) return true;
        else return false;
    }
    bool myDecayPositions_TrigFix::isBID_Event(vector<TVector3>& vpi_locs){
        if(vpi_locs.size() < 2) return false;
        int nBarrel=0;
        int nID = 0;
        for(int i_vp=0; i_vp < vpi_locs.size(); i_vp++){
            if( checkDecLoc(vpi_locs[i_vp]) == 1 ) nBarrel++;
            if( checkDecLoc(vpi_locs[i_vp]) == 3) nID++;
        }
        double dec_dR = vpi_locs[0].DeltaR(vpi_locs[1]);
        if(nBarrel == 1 && nID == 1 && dec_dR > 2.0) return true;
        else return false;
    }
    bool myDecayPositions_TrigFix::isEID_Event(vector<TVector3>& vpi_locs){
        if(vpi_locs.size() < 2) return false;
        int nEndcap=0;
        int nID = 0;
        for(int i_vp=0; i_vp < vpi_locs.size(); i_vp++){
            if( checkDecLoc(vpi_locs[i_vp]) == 2 ) nEndcap++;
            if( checkDecLoc(vpi_locs[i_vp]) == 3) nID++;
        }
        double dec_dR = vpi_locs[0].DeltaR(vpi_locs[1]);
        if(nEndcap == 1 && nID == 1 && dec_dR > 2.0) return true;
        else return false;
    }
    bool myDecayPositions_TrigFix::isBadTopology(vector<TVector3>& vpi_locs){
        
        if(vpi_locs.size() < 2) return true;
        
        for(int i_vp=0; i_vp < vpi_locs.size(); i_vp++){
            if(checkDecLoc(vpi_locs[i_vp])==0) return true;
        }
        return false;
        
    }
    
