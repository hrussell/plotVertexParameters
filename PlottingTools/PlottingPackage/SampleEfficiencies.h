//
//  SampleEfficiencies.h
//  plotVertexParameters
//
//  Created by Heather Russell on 28/02/17.
//
//

#ifndef plotVertexParameters_SampleEfficiencies_h
#define plotVertexParameters_SampleEfficiencies_h

#include "PlottingPackage/SampleDetails.h"

namespace Efficiencies{
    
    enum EffType {};

    std::vector<std::pair<EffType, std::vector<double> > efficiencies;

    void add2DEfficiency(){
        
    }

    void add1DEfficiency(EffType eff, TH1D *h_num, TH1D *h_den ){
    
    }
}

#endif
