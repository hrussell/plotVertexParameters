//
//  ResidualHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 06/12/15.
//
//

#ifndef plotVertexParameters_ResidualHistograms_h
#define plotVertexParameters_ResidualHistograms_h

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"

struct ResidualHistograms {
public:
    
    TH1D *h_dist_Residual;
    TH1D *h_Lxy_Residual;
    TH1D *h_Lz_Residual;
    TH1D *h_eta_Residual;
    TH1D *h_phi_Residual;
    TH1D *h_theta_Residual;
    
    void initializeHistograms();
    void fillHistograms(TVector3 object, TVector3 decay, double weight);
    
};


#endif
