//
//  SortingUtils.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef plotVertexParameters_SortingUtils_h
#define plotVertexParameters_SortingUtils_h
#include <utility>
#include <vector>
#include <iostream>
#include <algorithm>

namespace SortingUtils{
    namespace {
    bool is_unique_element (std::pair<int,int> a, std::pair<int,int> b) {
        return ((a.first == b.first) || (a.second == b.second));
    }
    bool sort_my_pair(std::pair<int,int> lhs, std::pair<int,int> rhs) {
        if (lhs.first < rhs.first) return false;
        if (rhs.first < lhs.first) return true;
        return lhs.second < rhs.second;
    }
    
    bool remove_higher_dR(std::pair<int,int> lhs, std::pair<int,int> rhs){
        return (rhs.second >= lhs.second && rhs.first <= lhs.first);
    }
    
    
    void make_unique_list(std::vector<std::pair<int,int> > &jet_list, bool test = false){
        
        if(test){
            std::cout << "original list" << std::endl;
            
            for(unsigned int i=0;i<jet_list.size();i++){
                std::cout << jet_list.at(i).first <<", " << jet_list.at(i).second << std::endl;
            }
            std::cout << "" << std::endl;
            std::cout << "sorted list" << std::endl;
        }
        
        std::sort(jet_list.begin(),jet_list.end(),sort_my_pair);
        if(test){
            for(unsigned int i=0;i<jet_list.size();i++){
                std::cout << jet_list.at(i).first <<", " << jet_list.at(i).second << std::endl;
            }
            std::cout << "" << std::endl;
            std::cout << "sorted list, unique elements only" << std::endl;
        }
        std::vector<std::pair<int,int> >::iterator last = std::unique(jet_list.begin(), jet_list.end(), is_unique_element);
        jet_list.erase(last,jet_list.end());
        if(test){
            for(unsigned int i=0;i<jet_list.size();i++){
                std::cout << jet_list.at(i).first <<", " << jet_list.at(i).second << std::endl;
            }
            std::cout << "" << std::endl;
            std::cout << "remove lower Pt, higher dR elements" << std::endl;
        }
        last = std::unique(jet_list.begin(), jet_list.end(), remove_higher_dR);
        jet_list.erase(last,jet_list.end());
        if(test){
            for(unsigned int i=0;i<jet_list.size();i++){
                std::cout << jet_list.at(i).first <<", " << jet_list.at(i).second << std::endl;
            }
        }
        return;
    }
    }
};

#endif
