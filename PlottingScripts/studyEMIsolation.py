import ROOT
import itertools
import os.path
import sys

def isHIso(hadIso = 999, roiP4 = None):
    if hadIso < (roiP4.Pt() / 23. - 0.2):
        return True
    return False

def getVThreshold(roiEta = 99):
    threshold = 8001
    if roiEta < 0.8:
        threshold = 9001
    elif roiEta < 1.1:
        threshold = 7001
    elif roiEta < 1.4:
        threshold = 6001
    elif roiEta < 1.5:
        threshold = 5001
    elif roiEta < 1.8:
        threshold = 7001
    elif roiEta < 2.5:
        threshold = 8001
    elif roiEta < 4.9:
        threshold = 9001   
    else:
        print "invalid RoI eta: ", roiEta
    return threshold

doMC = True
out_file = "output_data_studyL1EM8.root"
data_file = "/mnt/hdB/Bphys_datasets/enhancedBias.root"
weights_file = "/mnt/hdB/Bphys_datasets/enhancedBias.root.EBweights.root"

if doMC:
    out_file = "output_mc_studyL1EM8.root"
    data_file = "/mnt/hdB/Bphys_datasets/kstareeMC.root"

if os.path.exists(out_file):
    print "Output file ",out_file," already exists. Exiting."
    sys.exit(0)

f = ROOT.TFile.Open(data_file, "read")
t = f.Get("BeeK")
if not doMC:
    print 'added a friend'
    t.AddFriend('BeeK',weights_file)

fOut = ROOT.TFile.Open(out_file,"NEW")
    

print t.GetEntries(),' events in sample'

for ie,event in enumerate(t):
   if(ie % 10000 == 0): print 'event: ',ie
   weight = 1.
   if not doMC: 
      weight = event.EBweight

   # we're going to have to add a mu6 at L1/HLT in order to reduce rates
   # so we might as well only consider events that already pass mu6
   # makes code faster, and ensures we don't optimize toward the events mu6 will cut out!
#   if not event.HLT_mu6:
#       continue
   # L1_J15 will also likely have to happen, but we can leave this one out for now... 
   # maybe we can do studies with/without mu6 && L1_J15?
   #if not event.L1_J15:
   #    continue

   #get the reco electrons and the L1 EM RoIs
   electrons = event.recoelectrons
   l1ems = event.l1ems
   l1ems_match = event.l1ems_match
   isloose = event.recoloose

   l1mus =[mu.p4 for mu in event.l1muons]
   
   l1mus.sort(key=lambda tup: tup.Pt(), reverse = True)
   if len(l1mus) < 1:
       #print 'ERROR: passed HLT_mu6 but has no L1 muon RoIs...'
       sdf = 1
   else: 
       l1mus.sort(key=lambda tup: tup.Pt(), reverse = True)
   
   l1rois  = [(match,l1em) for match,l1em in zip(l1ems_match, l1ems)]
   
   passEM8VH_Emul = []
   passEM8H_Emul = []
   passEM8V_Emul = []
   passEM8_Emul = []

   l1rois.sort(key=lambda tup: tup[1].p4.Pt(), reverse=True)
 
   #to check if they're matched to the truth electrons
   pair_matches = False

   for iter1 in l1rois:
       pTVal = iter1[1].p4.Pt()
       if pTVal > 8001:
           passEM8_Emul = True
           if isHadIsol(iter1[1].hadIsol, pTVal):
               passEM8H_Emul = True
       if pTVal > getVThreshold(abs(iter1[1].p4.Eta())):
           passEM8V_Emul = True
           if isHadIsol(iter1[1].hadIsol, pTVal):
               passEM8VH_Emul = True

   if (event.L1_EM8VH == 0 && passEM8VH_Emul == True) || (event.L1_EM8VH == 1 && passEM8VH_Emul == False):
       print "MISMATCH"

fOut.Write()
fOut.Close()
