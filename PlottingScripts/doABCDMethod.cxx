#include "../PlottingTools/PlottingPackage/SampleDetails.h"
TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    std::cout << "file name : " << fileName << std::endl;
    TString title = "";
    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}
void doABCDMethod(TString outDir = "", TString sampleName = "", bool blind = true, std::vector<double> xMinBound = {0,0}, std::vector<double> yMinBound = {0,0}, std::vector<double> xBound = {0.3,0.4}, std::vector<double> yBound = {2000,2500}){

    TString expName = "e1";
    bool isData = (sampleName == "")? true : false;
    double nEvents, prodXS, lumi, scaling;
    TString label = "";
    if(!isData){ 
        label = convertFiletoLabel(sampleName);
        std::cout << "sample is: " << sampleName << std::endl;
        SampleDetails::setGlobalVariables(sampleName);
        nEvents = SampleDetails::nEventsInSample;
     prodXS = SampleDetails::mediatorXS;
     lumi =  32864.+3212.96;
     scaling = lumi*prodXS/nEvents;
     std::cout << "Applying scaling of " << scaling << std::endl;
    } else {
        std::cout << "Sample is data. Hope you blinded correctly!" << std::endl;
         scaling = 1.0;
    }
 
    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    gStyle->SetPalette(1);
    c1->SetLogz(1);
    c1->SetRightMargin(0.13);
    
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;
    
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    std::vector<TString> reg = {"_b","_ec"};
    std::vector<std::vector<TString>> histNames = {{"1j50_1j150","2j150"},{"1j100_1j250","2j250"}};
    std::vector<std::vector<TString>>  histLabels = {{"#splitline{E_{T,1} > 150 GeV,}{150 GeV > E_{T,2} > 50 GeV}", "#splitline{E_{T,1} > 150 GeV,}{E_{T,2} > 150 GeV}"},{"#splitline{E_{T,1} > 250 GeV,}{250 GeV > E_{T,2} > 100 GeV}", "#splitline{E_{T,1} > 250 GeV,}{E_{T,2} > 250 GeV}"}};
    
    TLine *line= new TLine(); line->SetLineColor(kRed);
    TLatex p2;
    p2.SetNDC();
    p2.SetTextSize(0.06);
    p2.SetTextFont(42);
    p2.SetTextColor(kRed);

    for(int i_reg=0;i_reg < 2; i_reg++){
        
        for(int i_cr =0; i_cr < 2; i_cr++){
            TString srName = "you're in the signal region!";
            TH2D *h2 = nullptr;
            if(i_cr !=1){
                if(i_reg == 0) srName= expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr+1)+"_low"+reg.at(i_reg);
                else if(i_reg == 1) srName= expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr+1)+reg.at(i_reg);
                h2 = (TH2D*)_file0->Get(srName);
                //h2->Scale(scaling);
                h2->SetDirectory(0);
            }
            //std::cout << "signal region: " << srName << std::endl;

            TString histname = expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr)+reg.at(i_reg);
            //std::cout << "looking at hist: " << histname << std::endl;
            TH2D *h = (TH2D*)_file0->Get(histname);
            h->SetDirectory(0);
            //h->Scale(scaling);
            if(i_cr != 1) h->Add(h2,-1.);

            double corr = h->GetCorrelationFactor();
            //std::cout << "correlation: " << h->GetCorrelationFactor() << std::endl;
            
            double errorA = 0;
            double errorB = 0;
            double errorC = 0;
            double errorD = 0;
            double errorBCD = 0;
            
            h->GetXaxis()->SetTitle("min(#Delta R(closest jet), #Delta R(closest track))");

            TBox *box1;
            h->GetYaxis()->SetTitle("vertex(nMDT + nTrig hits)");
            //std::cout << "ABCD Method with lines at: " << xBound<< ", " << yBound << std::endl;
            
            int xMin = h->GetXaxis()->FindBin(xMinBound.at(i_reg) + 0.001);
            int xMax = h->GetXaxis()->FindBin(xBound.at(i_reg) - 0.001);
            int yMin = h->GetYaxis()->FindBin(yMinBound.at(i_reg) + 0.001);
            int yMax = h->GetYaxis()->FindBin(yBound.at(i_reg) - 0.001);
            std::cout << "ABCD Method with lines at: " << h->GetXaxis()->GetBinLowEdge(xMax+1) << ", " << h->GetYaxis()->GetBinLowEdge(yMax+1) << std::endl;
            std::cout << "total number of events in histogram: " << h->Integral()*scaling << std::endl;
            double valA = h->IntegralAndError(xMax+1,h->GetXaxis()->GetNbins()+1,yMax+1,h->GetYaxis()->GetNbins()+1,errorA);
            double valB = h->IntegralAndError(xMin,xMax,yMax+1,h->GetYaxis()->GetNbins()+1,errorB);
            double valC = h->IntegralAndError(xMax+1,h->GetXaxis()->GetNbins()+1,yMin,yMax,errorC);
            double valD = h->IntegralAndError(xMin,xMax,yMin,yMax,errorD);
            errorA = sqrt(valA);
            errorB = sqrt(valB);
            errorC = sqrt(valC);
            errorD = sqrt(valD);
            double BCD = valB*valC/valD;
            errorBCD = sqrt(1/valB + 1/valC + 1/valD) * BCD;
            
            if((i_cr == 1)&& blind){
                std::cout <<  histNames.at(i_reg).at(i_cr) << " & blind & $" << BCD*scaling << " \\pm " << errorBCD*scaling << "$ & $";
                
                box1 = new TBox(xBound.at(i_reg), yBound.at(i_reg), 5, 10000);
                box1->SetFillColor(kBlue-10);
            } else{
                std::cout << histNames.at(i_reg).at(i_cr) << " & $"<<valA*scaling << " \\pm " << errorA*scaling << "$ & $" << BCD*scaling << " \\pm " << errorBCD*scaling << "$ & $";
            }
            std::cout << valB*scaling << " \\pm " << errorB*scaling << "$ & $";
            std::cout << valC*scaling << " \\pm " << errorC*scaling << "$ & $";
            std::cout << valD*scaling << " \\pm " << errorD*scaling << "$ \\\\ " << std::endl;
            //h->Scale(1./389000.*1.4891*22.1);
            h->GetXaxis()->SetRangeUser(0,5);
            h->SetMinimum(0.8);
            h->Scale(scaling);
            h->Draw("COLZ");
            
            if((i_cr == 1 ) && blind) box1->Draw();
            
            ATLASLabel(0.22,0.89,"Internal");
    
            line->DrawLine(xBound.at(i_reg),0,xBound.at(i_reg),10000);
            line->DrawLine(0,yBound.at(i_reg),5,yBound.at(i_reg));
            
            
            if(i_reg == 0){
                p2.DrawLatex(.45,.6,"#font[72]{A}");
                p2.DrawLatex(.45,.22,"#font[72]{C}");

                p2.DrawLatex(.16,.6,"#font[72]{B}");
                p2.DrawLatex(.16,.22,"#font[72]{D}");

            }else {
                p2.DrawLatex(.45,.55,"#font[72]{A}");
                p2.DrawLatex(.45,.22,"#font[72]{C}");
              p2.DrawLatex(.18,.55,"#font[72]{B}");
              p2.DrawLatex(.18,.22,"#font[72]{D}");

            }
            
            TLatex latexr;
            latexr.SetNDC();
            latexr.SetTextColor(kBlack);
            latexr.SetTextSize(0.04);
            latexr.SetTextAlign(13);  //align at top
            if(reg.at(i_reg) == "_b") latexr.DrawLatex(.6,.87,"Barrel vertices");
            else latexr.DrawLatex(.6,.87,"Endcap vertices");
            if(isData) latexr.DrawLatex(.6,.92,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}");
            else{
                latexr.DrawLatex(0.6,0.92,label);
                latexr.SetTextSize(0.05);
                latexr.DrawLatex(0.22,0.863,"Simulation");
            }
            TLatex latex2;
            latex2.SetNDC();
            latex2.SetTextColor(kBlack);
            latex2.SetTextSize(0.035);
            latex2.SetTextAlign(13);  //align at top
            latex2.DrawLatex(.6,.82,histLabels.at(i_reg).at(i_cr));
            TLatex latex3;
            latex3.SetNDC();
            latex3.SetTextColor(kBlack);
            latex3.SetTextSize(0.035);
            latex3.SetTextAlign(13);  //align at top
            latex3.DrawLatex(.6,.7,TString("correlation: "+to_string(corr)) );
        
            c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_"+histNames.at(i_reg).at(i_cr)+reg.at(i_reg)+".pdf");
            
            h = nullptr;
            h2 = nullptr;
        }
    }
}
