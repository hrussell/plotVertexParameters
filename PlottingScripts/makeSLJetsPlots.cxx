void makeSLJetsPlots(){
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c_1","c_1",800,600);
    c1->SetLogy(1);
    
    TChain* ch[4]; ch[1] = new TChain("recoTree");
    ch[2] = new TChain("recoTree");
    ch[3] = new TChain("recoTree");
    ch[1]->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304903.MadGraphPythia8EvtGen_StealthSUSY_mG250.AOD_DVAna.r7772_dv17_hist/*root*");
    ch[2]->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304905.MadGraphPythia8EvtGen_StealthSUSY_mG800.AOD_DVAna.r7772_dv17_hist/*root*");
    ch[3]->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304907.MadGraphPythia8EvtGen_StealthSUSY_mG1500.AOD_DVAna.r7772_dv17_hist/*root*");

    
    TChain* chjz[9];
    double jetSliceWeights[13];
            double lumi_in_nb = 32864.*1000. + 3193.68*1000.; //2016 MD2 + 2015 full repro. Needs to be the same as the prw!

        jetSliceWeights[0] = 1.0240     * 78420000.      * lumi_in_nb /100000.; //( Filter eff. ) * ( Cross-section [nb] ) * ( desired lumi [nb^-1] ) / ( Num Events )
        jetSliceWeights[1] = 0.00067198 * 78420000.      * lumi_in_nb /100000.; //JZ1W
        jetSliceWeights[2] = 0.00033264 * 2433400.       * lumi_in_nb /1982189.;
        jetSliceWeights[3] = 0.00031953 * 26454.         * lumi_in_nb /7874500.;
        jetSliceWeights[4] = 0.00053009 * 254.64         * lumi_in_nb /7979800.;
        jetSliceWeights[5] = 0.00092325 * 4.5536         * lumi_in_nb /7977600.;
        jetSliceWeights[6] = 0.00094016 * 0.25752        * lumi_in_nb /1893400.;
        jetSliceWeights[7] = 0.00039282 * 0.016214       * lumi_in_nb /1770200.; //JZ7W
        jetSliceWeights[8] = 0.010162   * 0.00062505     * lumi_in_nb /1743200.; //JZ8W
        jetSliceWeights[9] = 0.012054   * 0.00001964     * lumi_in_nb /1813200.; //JZ9W
        jetSliceWeights[10] = 0.0058935 * 0.0000011961   * lumi_in_nb /1996000.; //JZ10W
        jetSliceWeights[11] = 0.0027015 * 0.00000004226  * lumi_in_nb /1993200.; //JZ11W
        jetSliceWeights[12] = 0.00042502* 0.000000001037 * lumi_in_nb /1974600.; //JZ12W
    
    std::vector<TString> jzst = {"user.hrussell.mc15_13TeV.361024.Pythia8EvtGen_jetjet_JZ4W.EXOT15_DVAna.r7725_dv17_hist","user.hrussell.mc15_13TeV.361025.Pythia8EvtGen_jetjet_JZ5W.EXOT15_DVAna.r7725_dv17_hist","user.hrussell.mc15_13TeV.361026.Pythia8EvtGen_jetjet_JZ6W.EXOT15_DVAna.r7725_dv17_hist","user.hrussell.mc15_13TeV.361027.Pythia8EvtGen_jetjet_JZ7W.EXOT15_DVAna.r7725_dv17_hist","user.hrussell.mc15_13TeV.361028.Pythia8EvtGen_jetjet_JZ8W.EXOT15_DVAna.r7772_dv17_hist","user.hrussell.mc15_13TeV.361029.Pythia8EvtGen_jetjet_JZ9W.EXOT15_DVAna.r7772_dv17_hist","user.hrussell.mc15_13TeV.361030.Pythia8EvtGen_jetjet_JZ10W.EXOT15_DVAna.r7772_dv17_hist","user.hrussell.mc15_13TeV.361031.Pythia8EvtGen_jetjet_JZ11W.EXOT15_DVAna.r7772_dv17_hist","user.hrussell.mc15_13TeV.361032.Pythia8EvtGen_jetjet_JZ12W.EXOT15_DVAna.r7772_dv17_hist"};
        TH1D * jzh[9][2];
        TH1D *jztotb = new TH1D("jzb","jzb",500,0,1000);
        TH1D *jztote = new TH1D("jze","jze",500,0,1000);
    for (int i =0; i<9;i++){
        chjz[i] = new TChain("recoTree");
        chjz[i]->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v17/"+jzst.at(i)+"/*root*");
        jzh[i][0] = new TH1D(TString("njzb"+to_string(i)), TString("njzb"+to_string(i)),500,0,1000);
        chjz[i]->Project(TString("njzb"+to_string(i)), "CalibJet_pT[1]","eventWeight*pileupEventWeight*(CalibJet_isGoodStand[1] && Length$(MSVertex_indexTrigRoI > -1) > 0 && TMath::Abs(MSVertex_eta) < 0.8)");
        jztotb->Add(jzh[i][0], jetSliceWeights[i+1]);
        jzh[i][1] = new TH1D(TString("njze"+to_string(i)), TString("njze"+to_string(i)),500,0,1000);
        chjz[i]->Project(TString("njze"+to_string(i)), "CalibJet_pT[1]","eventWeight*pileupEventWeight*(CalibJet_isGoodStand[1] && Length$(MSVertex_indexTrigRoI > -1) > 0 && TMath::Abs(MSVertex_eta) > 1.3 && TMath::Abs(MSVertex_eta) < 2.5)");
        jztote->Add(jzh[i][1], jetSliceWeights[i+1]);
    }
    
    
    
    TH1D* h[2][4];
    
    
    Int_t colorSig[4] = {kGray, kBlue-3, kViolet-6, kTeal-6};
    TLegend *leg = new TLegend(0.6,0.73,.92,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.03);
    TString label[4] = {"","m_{#tilde{g}} = 250 GeV","m_{#tilde{g}} = 800 GeV","m_{#tilde{g}} = 1500 GeV"};
    
    for(int i=1;i<4;i++){
        h[0][i] = new TH1D(TString("n"+to_string(i)),TString("n"+to_string(i)),50,0,1000);
        ch[i]->Project(TString("n"+to_string(i)), "CalibJet_pT[1]","eventWeight*pileupEventWeight*(CalibJet_isGoodStand[1] && Length$(MSVertex_indexTrigRoI > -1) > 0 && TMath::Abs(MSVertex_eta) < 0.8)");
        h[0][i]->Scale(1./h[0][i]->Integral());
        h[0][i]->SetLineColor(colorSig[i]);
        h[0][i]->SetLineStyle(i+1);
        
        h[1][i] = new TH1D(TString("ne"+to_string(i)),TString("ne"+to_string(i)),50,0,1000);
        ch[i]->Project(TString("ne"+to_string(i)), "CalibJet_pT[1]","eventWeight*pileupEventWeight*(CalibJet_isGoodStand[1] && Length$(MSVertex_indexTrigRoI > -1) > 0 && TMath::Abs(MSVertex_eta) > 1.3 && TMath::Abs(MSVertex_eta) < 2.5)");
        h[1][i]->Scale(1./h[1][i]->Integral());
        h[1][i]->SetLineColor(colorSig[i]);
        h[1][i]->SetLineStyle(i+1);
        
        
        if(i==1){
            
            h[0][i]->GetXaxis()->SetTitle("Subleading jet p_{T} [GeV]");
            h[0][i]->GetYaxis()->SetTitle("Fraction of events with a barrel MS vertex");
            h[0][i]->GetYaxis()->SetRangeUser(0.0001,1);
            h[1][i]->GetXaxis()->SetTitle("Subleading jet p_{T} [GeV]");
            h[1][i]->GetYaxis()->SetTitle("Fraction of events with an endcap MS vertex");
            h[1][i]->GetYaxis()->SetRangeUser(0.0001,1);
            
            h[0][i]->Draw("hist");
        }
        
        else{
        
            h[0][i]->Draw("HIST SAME");
        }

        leg->AddEntry(h[0][i],label[i],"l");
    }
    
    jztotb->Scale(1./jztotb->Integral());
    jztotb->Draw("HIST SAME");
    jztotb->SetLineColor(kBlack); jztotb->SetMarkerColor(kBlack);
    leg->AddEntry(jztotb,"Multi-jet MC","lp");
    leg->Draw();
    gPad->RedrawAxis();
    ATLASLabel(0.19,0.88,"Internal", kBlack);
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    //latex.SetTextSize(0.04);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(.19,.87,"Simulation");
    
    c1->Print("subleadingjets_barrel.pdf");
    
    for(int i=1;i<4;i++){
        
        if(i==1){	h[1][i]->Draw("hist");
        }
        
        else{
            h[1][i]->Draw("HIST SAME");
        }
    }
    jztote->Scale(1./jztote->Integral());
    jztote->Draw("HIST SAME");
    jztote->SetLineColor(kBlack); jztotb->SetMarkerColor(kBlack);
    leg->Draw();
    
    gPad->RedrawAxis();
    ATLASLabel(0.19,0.88,"Internal", kBlack);
    latex.DrawLatex(.19,.87,"Simulation");
    
    c1->Print("subleadingjets_endcap.pdf");
}
