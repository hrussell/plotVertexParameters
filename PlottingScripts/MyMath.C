#include "TMath.h"

namespace MyMath{

    double myLogScale(double xMin, double nPoints, int nBin){
        double a = floor(double(nBin) / nPoints );
        double b = double(nBin % int(nPoints));
        return pow(10, log10(xMin) + a)*(1 + 0.1*b);
    }
    double deltaPhi(double phi1, double phi2){
	      double phi = phi1 - phi2;
	      while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    	      while (phi<-TMath::Pi()) phi += TMath::TwoPi();
              return phi;
	}
    double deltaR(double phi1, double phi2, double eta1, double eta2){
        double phi = (phi1-phi2);
        while (phi> TMath::Pi()) phi -= TMath::TwoPi();
        while (phi<-TMath::Pi()) phi += TMath::TwoPi();
        double delEta = eta1-eta2;
        return sqrt(phi*phi+delEta*delEta);
    }
}
