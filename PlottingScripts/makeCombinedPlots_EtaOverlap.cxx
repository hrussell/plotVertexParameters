//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;



int makeCombinedPlots_EtaOverlap(){
    
	
gROOT->LoadMacro("AtlasUtils.C");
gROOT->LoadMacro("AtlasLabels.C");
gROOT->LoadMacro("AtlasStyle.C");

/*TString *sigName[15] = {"mH100mS8lt5","mH100mS25lt5","mH125mS8lt5","mH125mS15lt5","mH125mS15lt9","mH125mS40lt5",
		"mH125mS40lt9","mH200mS8lt5","mH200mS8lt9","mH200mS25lt5","mH200mS50lt5","mH200mS50lt9","mH400mS100lt9","mH600mS150lt9","mH1000mS50lt9"};
	*/ //full signal for when this is all done	
TString sigName[12] = {"mH100mS25lt5","mH125mS8lt5","mH125mS15lt5","mH125mS40lt5","mH125mS40lt9","mH200mS8lt5","mH200mS8lt9",
		"mH200mS50lt5","mH200mS50lt9","mH400mS100lt9","mH600mS150lt9","mH1000mS50lt9"};
 int drawSig[12] = {0,1,0,1,0,0,0,1,0,0,1,0};
 TFile *_sig[12]; int nSIG=12;
 Int_t colorSig[12] = {kRed-7,kRed-4,kRed+1,kRed+2,kRed+3,kMagenta+4,kMagenta+3,kMagenta+2,kViolet-3,kViolet+7,kViolet+9,kBlue+1};
 Int_t markerSig[12] = {20,21,22,23,29,33,34,20,21,22,23,29};
 for(int i=0;i<nSIG;i++){
	 _sig[i] = new TFile("signalMC/"+sigName[i]+"/outputMSVxParams.root");
 }
 TString histNames[10];
 histNames[0] = "divide_MSVertex_eta_2MATCH_full_by_MSVertex_etaABS_full";

 TString titles[10];
 titles[0] = "fraction of vertices that are from a double-matched LLP";


 SetAtlasStyle();
 gStyle->SetPalette(1);

 
 TCanvas* c = new TCanvas("c_1","c_1",800,600);

 for(unsigned int i=0;i<1; i++){
	 TGraphAsymmErrors *g_sig[12]; 
	 int first=0;
	 for(int j=0; j<nSIG; j++){
		 g_sig[j] = (TGraphAsymmErrors*) _sig[j]->Get(histNames[i]);
		 g_sig[j]->SetMarkerColor(colorSig[j]); g_sig[j]->SetLineColor(colorSig[j]); g_sig[j]->SetMarkerStyle(markerSig[j]);g_sig[j]->SetLineStyle(j);
		 g_sig[j]->GetXaxis()->SetRangeUser(0,2.5);
		 if(!drawSig[j]) continue;
		 if(first==0){
			 g_sig[j]->SetMaximum(0.3);
			 g_sig[j]->Draw("AP");
		 }
		 else g_sig[j]->Draw("P SAME");
		 first++;
	 }
	 	 
	 
	 TLatex latex;
	 latex.SetNDC();
	 latex.SetTextColor(kBlack);
	 latex.SetTextSize(0.04);
	 latex.SetTextAlign(13);  //align at top
	 latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");
	 //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");

	 TLegend *legS = new TLegend(0.67,0.37,0.89,0.9);
	 legS->SetFillColor(0);
	 legS->SetBorderSize(0);
	 legS->SetTextSize(0.03);
	 for(int j=0;j<nSIG;j++){
		 if(!drawSig[j]) continue;
		 legS->AddEntry(g_sig[j],sigName[j],"lp");
	 }
	 legS->Draw();

	 gPad->RedrawAxis();
	 c->Print("combinedPlots/"+histNames[i]+".pdf");

 }
 /*
 for(unsigned int i=0;i<5; i++){
	 TGraphAsymmErrors *g_bkg = (TGraphAsymmErrors*) _bkg->Get(names[i]);
	 TGraphAsymmErrors *g_sig12515 = (TGraphAsymmErrors*) _sig12515->Get(names[i]);
	 TGraphAsymmErrors *g_sig12540 = (TGraphAsymmErrors*) _sig12540->Get(names[i]);
	 
	 g_bkg->SetMarkerColor(kBlack); 	 g_bkg->SetLineColor(kBlack); 	 g_bkg->SetMarkerStyle(20);
	 g_sig12515->SetMarkerColor(kBlue-3); 	 g_sig12515->SetLineColor(kBlue-3); 	 g_sig12515->SetMarkerStyle(21);
	 g_sig12540->SetMarkerColor(kTeal-6); 	 g_sig12540->SetLineColor(kTeal-6); 	 g_sig12540->SetMarkerStyle(22);
	 g_sig12515->GetYaxis()->SetRangeUser(0.0,1.2);
	 if(i==1 || i == 3 || i == 6 || i == 8)g_sig12515->GetXaxis()->SetRangeUser(0,1);
	 else if(i==0 || i == 5)g_sig12515->GetXaxis()->SetRangeUser(0,200);
	 else if(i==2 || i == 7)g_sig12515->GetXaxis()->SetRangeUser(0,20);
	 else if(i==4 || i == 9)g_sig12515->GetXaxis()->SetRangeUser(0,30);

	 TGraph *g_roc = new TGraph(g_bkg->GetN(),g_bkg->GetY(),g_sig12515->GetY());
	 g_roc->SetLineColor(colors[i]);
	 g_roc->SetMarkerColor(colors[i]);
	 g_roc->SetFillColor(kWhite);
	 g_roc->SetMarkerStyle(20+i);
	 g_roc->SetLineWidth(2);
	 g_roc->SetTitle(titles[i]);
	 if(i==0) g_roc->Draw("AL");
	 else g_roc->Draw("L SAME");
	 
	 c->Print("combinedPlots/ROC15.pdf");

	 
 }
*/
 return 0;
 
}