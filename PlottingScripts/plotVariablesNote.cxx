//
// ** Macro to make plots for the supporting note
//
// -- Author: Cristiano alpigiani
//    30 May 2017
//

#include "TString.h"
#include "Riostream.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TBox.h"
#include "TLine.h"
#include "TLegend.h"
#include "TColor.h"
#include "TStyle.h"
#include "TLatex.h"

void plot(TString c_name, TString dir, TString Xname, TString Yname, const int nHistos, TH1D *hc[], TString legName[], Color_t hColor[], Style_t hStyle[], bool logScale=false)
{
  TLatex p;
  p.SetNDC();
  p.SetTextSize(0.050);
  p.SetTextFont(42);
  p.SetTextColor(kBlack);
  
  TH1D *h[nHistos];
  double h_max=-999;
  for (int l=0;l<nHistos;l++){
    TString str=""; str+=l;
    h[l] = (TH1D*)hc[l]->Clone("h"+str);
    h[l]->Scale(1./h[l]->Integral());
    if (h[l]->GetMaximum()>h_max) h_max=h[l]->GetMaximum();
  }
  
  TCanvas *c = new TCanvas("c_"+c_name,"c_"+c_name,800,600);

  if (logScale){
    c->SetLogy();
    h[0]->SetMaximum(h_max*2.0);
    h[0]->SetMinimum(0.001);
  }else{
    if(c_name.Contains("nHits_Barrel")){ h[0]->SetMaximum(0.35);}
    else if(c_name.Contains("nHits_Endcap")){ h[0]->SetMaximum(0.25);}
    else {h[0]->SetMaximum(h_max*1.2);}
    h[0]->SetMinimum(0);
  }
  
  h[0]->GetYaxis()->SetTitle(Yname);
  h[0]->GetXaxis()->SetTitle(Xname);
  if (c_name.Contains("nHits")){
	 h[0]->GetXaxis()->SetRangeUser(0,10000);
	h[0]->GetXaxis()->SetNdivisions(5,10,0);
}
  h[0]->SetMarkerSize(1.5);
  h[0]->Draw("ep SAME");
  for (int l=1;l<nHistos;l++){
    h[l]->SetLineColor(hColor[l]);
    h[l]->SetLineStyle(hStyle[l]);
    h[l]->Draw("HISTO SAME");
  }
  
  //p.DrawLatex(.50,.63,"#font[72]{ATLAS}  Internal Simulation");
  p.DrawLatex(.6,.63,"#font[72]{ATLAS}  Internal");

  if (c_name.Contains("Barrel"))  p.DrawLatex(.6,.56,"#font[42]{Barrel vertices}");
  if (c_name.Contains("Endcaps")) p.DrawLatex(.6,.56,"#font[42]{Endcap vertices}");

  TLine *line = new TLine();
  line->SetLineWidth(3.0);
  if (c_name=="nTrackletsInVtx_Endcaps") line->DrawLine(5,0,5,0.35);
  
  TLegend *leg = new TLegend(0.5,0.68,0.9,0.93);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);
  leg->SetTextSize(0.035);
  for (int l=1;l<nHistos;l++)
    leg->AddEntry(h[l],legName[l],"l");
  leg->AddEntry(h[0],legName[0],"p");
  leg->Draw();
  c->Print(dir+c_name+"_v2.pdf");
 
}


void plotSigEffBkgRej(TString c_name, TString dir, TString Xname, TString Yname, const int nHistos, TH1D *h[], TString legName[], Color_t hColor[], Style_t hStyle[],
		      double xLegMin=0.51, double yLegMin=0.65, double xLegMax=0.9, double yLegMax=0.9, bool doReverse=false)
{
  TLatex p;
  p.SetNDC();
  p.SetTextSize(0.050);
  p.SetTextFont(42);
  p.SetTextColor(kBlack);

  TH1D *hClone[nHistos];
  for (int l=0;l<nHistos;l++){
    TString str=""; str+=l;
    hClone[l] = (TH1D*)h[l]->Clone("hClone_"+str);
    hClone[l]->Scale(hClone[0]->Integral()/hClone[l]->Integral());
    h[l]->Scale(1./h[l]->Integral());
  }

  TH1D *hEff[nHistos];
  TH1D *hSigEffBkgRej[nHistos];
  TH1D *hSoverB[nHistos];
  for (int l=0;l<5;l++){
    TString str=""; str+=l;
    hEff[l] = new TH1D(c_name+"_hSigEff_"+str,c_name+"_hSigEff_"+str,h[0]->GetNbinsX(),h[0]->GetBinLowEdge(1),h[0]->GetBinLowEdge(h[0]->GetNbinsX())+h[0]->GetBinWidth(1));
    hSigEffBkgRej[l] = new TH1D(c_name+"_hSigEffBkgRej_"+str,c_name+"_hSigEffBkgRej_"+str,10,0,1);
    hSoverB[l] = new TH1D(c_name+"_hSoverB_"+str,c_name+"_hSoverB_"+str,h[0]->GetNbinsX(),h[0]->GetBinLowEdge(1),h[0]->GetBinLowEdge(h[0]->GetNbinsX())+h[0]->GetBinWidth(1));
  }

  for (int l=0;l<nHistos;l++)
    for (int ll=1;ll<=h[0]->GetNbinsX();ll++){
      if (doReverse){
	hEff[l]->SetBinContent(ll,h[l]->Integral(1,ll));
	hSigEffBkgRej[l]->SetBinContent(hSigEffBkgRej[l]->GetXaxis()->FindBin(h[l]->Integral(1,ll)),1-h[0]->Integral(1,ll));
      }else{
	hEff[l]->SetBinContent(ll,h[l]->Integral(ll,h[0]->GetNbinsX()));
	hSigEffBkgRej[l]->SetBinContent(hSigEffBkgRej[l]->GetXaxis()->FindBin(h[l]->Integral(ll,h[l]->GetNbinsX())),1-h[0]->Integral(ll,h[0]->GetNbinsX()));
      }
      if (hClone[0]->GetBinContent(ll)!=0){
	double nBkg = hClone[0]->Integral(ll,hClone[0]->GetNbinsX());
	double nSig = hClone[l]->Integral(ll,hClone[l]->GetNbinsX());
	hSoverB[l]->SetBinContent(ll,nSig/sqrt(nBkg));
      }
    }

  for (int l=0;l<5;l++)
    hSoverB[l]->Rebin();

  TCanvas *cEff = new TCanvas(c_name+"_Eff",c_name+"_Eff",800,600);
  if (c_name.Contains("nHits")) hEff[0]->GetXaxis()->SetRangeUser(0,9000);
  hEff[0]->GetYaxis()->SetTitle(Yname);
  hEff[0]->GetXaxis()->SetTitle(Xname);
  if(c_name.Contains("Isolation")){
    hEff[0]->GetXaxis()->SetRangeUser(0,5);
  }
  hEff[0]->Draw("P SAME");
  for (int l=1;l<5;l++){
    hEff[l]->SetLineColor(hColor[l]);
    hEff[l]->SetLineStyle(hStyle[l]);
    hEff[l]->Draw("SAME");
  }
 
  p.DrawLatex(.60,.63,"#font[72]{ATLAS}  Internal");
  //p.DrawLatex(.65,.58,"Simulation");

  if (c_name.Contains("Barrel"))  p.DrawLatex(.60,.55,"#font[42]{Barrel vertices}");
  if (c_name.Contains("Endcaps")) p.DrawLatex(.575,.55,"#font[42]{Endcap vertices}");
  
  TLegend *leg1 = new TLegend(xLegMin,yLegMin,xLegMax,yLegMax);
  leg1->SetBorderSize(0);
  leg1->SetFillStyle(0);
  leg1->SetTextSize(0.035);
  for (int l=1;l<5;l++)
    leg1->AddEntry(hEff[l],legName[l],"l");
  leg1->AddEntry(hEff[0],legName[0],"p");
  leg1->Draw();
  cEff->Print(dir+c_name+"_Efficiency_v2.pdf");

  
  // ** Plot efficiency vs bkg rejection
  //
  TCanvas *cEffvsBkgRej = new TCanvas(c_name+"_EffvsBkgRej",c_name+"_EffvsBkgRej",800,600);
  hSigEffBkgRej[1]->SetMinimum(0.0);
  hSigEffBkgRej[1]->GetYaxis()->SetTitle("Background rejection");
  hSigEffBkgRej[1]->GetXaxis()->SetTitle("Signal efficiency");
  for (int l=1;l<5;l++){
    hSigEffBkgRej[l]->SetMarkerColor(hColor[l]);
    hSigEffBkgRej[l]->SetLineColor(hColor[l]);
    hSigEffBkgRej[l]->Draw("C SAME");
  }
  TLegend *leg2 = new TLegend(0.20,0.20,0.55,0.45);
  leg2->SetBorderSize(0);
  leg2->SetFillStyle(0);
  leg2->SetTextSize(0.035);
  for (int l=1;l<5;l++)
    leg2->AddEntry(hSigEffBkgRej[l],legName[l],"l");
  leg2->Draw();
  cEffvsBkgRej->Print(dir+c_name+"_SigEffvsBkgRej_v2.pdf");

  
  // ** Plot the S/B
  //
  TCanvas *cSoverB = new TCanvas(c_name+"_SoverB",c_name+"_SoverB",800,600);
  hSoverB[1]->SetMinimum(0.0);
  hSoverB[1]->SetMaximum(hSoverB[1]->GetMaximum()*3);
  hSoverB[1]->GetYaxis()->SetTitle("S/#sqrt{B}");
  hSoverB[1]->GetXaxis()->SetTitle(Xname);
  for (int l=1;l<5;l++){
    hSoverB[l]->SetMarkerColor(hColor[l]);
    hSoverB[l]->SetLineColor(hColor[l]);
    hSoverB[l]->Draw("C SAME");
  }
  TLegend *leg3 = new TLegend(0.40,0.70,0.75,0.95);
  leg3->SetBorderSize(0);
  leg3->SetFillStyle(0);
  leg3->SetTextSize(0.035);
  for (int l=1;l<5;l++)
    leg3->AddEntry(hSoverB[l],legName[l],"l");
  leg3->Draw();
  cSoverB->Print(dir+c_name+"_SigSoverB_v2.pdf");

}


void plotVariablesNote(){

  TH1::SetDefaultSumw2();
  
  TFile *f[5];
  f[0] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/data/v3/outputDataSearch_allData_v3.root");
  f[1] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/mg250/outputDataSearch_v3.root");
  f[2] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/mg800/outputDataSearch_v3.root");
  f[3] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/mg1200/outputDataSearch_v3.root");
  f[4] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/mg2000/outputDataSearch_v3.root");

  TH1D *h_Isolation_B[5];
  TH1D *h_Isolation_E[5];
  TH1D *h_nHits_B[5];
  TH1D *h_nHits_E[5];

  TH1D *h_Isolation_B_sr[5];
  TH1D *h_Isolation_E_sr[5];
  TH1D *h_nHits_B_sr[5];
  TH1D *h_nHits_E_sr[5];

  for (int l=0;l<5;l++){
    TString str=""; str+=l;
    h_Isolation_B[l] = (TH1D*)f[l]->Get("e1_MSVxIso_1j50_1j150_b");
    h_Isolation_B_sr[l] = (TH1D*)f[l]->Get("e1_MSVxIso_2j150_low_b");
    h_Isolation_E[l] = (TH1D*)f[l]->Get("e1_MSVxIso_1j100_1j250_ec");
    h_Isolation_E_sr[l] = (TH1D*)f[l]->Get("e1_MSVxIso_2j250_ec");

    h_nHits_B[l] = (TH1D*)f[l]->Get("e1_MSVxHits_1j50_1j150_b");
    h_nHits_B_sr[l] = (TH1D*)f[l]->Get("e1_MSVxHits_2j150_low_b");
    h_nHits_E[l] = (TH1D*)f[l]->Get("e1_MSVxHits_1j100_1j250_ec");
    h_nHits_E_sr[l] = (TH1D*)f[l]->Get("e1_MSVxHits_2j250_ec");

   
    if( l == 0){
      h_Isolation_B[l]->Add(h_Isolation_B_sr[l],-1.0);
      h_Isolation_E[l]->Add(h_Isolation_E_sr[l],-1.0);
      h_nHits_B[l]->Add(h_nHits_B_sr[l],-1.0);
      h_nHits_E[l]->Add(h_nHits_E_sr[l],-1.0);
    }
    h_nHits_B[l]->Rebin(5);
    h_nHits_E[l]->Rebin(5);
    h_Isolation_B[l]->GetXaxis()->SetRangeUser(0,5);
    h_Isolation_E[l]->GetXaxis()->SetRangeUser(0,5);
  }
 
  TString dir="../OutputPlots/notePlots/";
  TString legName[5]={"#font[42]{Data #sqrt{s}=13 TeV, 36.1 fb^{-1}}","#font[42]{m_{#tilde{g}} = 250 GeV}",
		      "#font[42]{m_{#tilde{g}} = 800 GeV}","#font[42]{m_{#tilde{g}} = 1200 GeV}",
		      "#font[42]{m_{#tilde{g}} = 2000 GeV}"};
  Color_t color[5]={kBlack,kRed,kBlue,kGreen+1,kOrange+1};
  Style_t style[5]={kSolid,kSolid,kDashed,kDotted,kDashDotted};

  plot("Isolation_Barrel"       ,dir,"min(#DeltaR(jet,vtx),#DeltaR(trk,vtx))","Arbitrary Units",5,h_Isolation_B,legName,color,style,true);
  plot("nHits_Barrel"           ,dir,"nMDT+nRPC hits #in vertex","Arbitrary Units",5,h_nHits_B,legName,color,style);
  //
  plot("Isolation_Endcaps"       ,dir,"min(#DeltaR(jet,vtx),#DeltaR(trk,vtx))","Arbitrary Units",5,h_Isolation_E,legName,color,style,true);
  plot("nHits_Endcaps"           ,dir,"nMDT+nTGC hits #in vertex","Arbitrary Units",5,h_nHits_E,legName,color,style);

  // ** Efficiency plots
  plotSigEffBkgRej("nHits_Barrel" ,dir,"nMDT+nRPC hits #in  vertex", "Efficiency" ,5,h_nHits_B,legName,color,style,0.51,0.68,0.90,0.93,false);
  plotSigEffBkgRej("nHits_Endcaps",dir,"nMDT+nTGC hits #in  vertex", "Efficiency" ,5,h_nHits_E,legName,color,style,0.51,0.68,0.90,0.93,false);
  plotSigEffBkgRej("Isolation_Barrel" ,dir,"min(#DeltaR(jet,vtx),#DeltaR(trk,vtx))", "Efficiency" ,5,h_Isolation_B,legName,color,style,0.51,0.68,0.90,0.93,false);
  plotSigEffBkgRej("Isolation_Endcaps",dir,"min(#DeltaR(jet,vtx),#DeltaR(trk,vtx))", "Efficiency" ,5,h_Isolation_E,legName,color,style,0.51,0.68,0.90,0.93,false);

}
