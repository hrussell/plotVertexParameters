#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-12) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }

    Double_t arg = 1.0 - ( x - peak ) * tail / width;

    if (arg < 1.e-12) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);

    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )

    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );

    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}
void plotGlobalEfficiencies_Scalar(){
    std::vector<std::vector<TString>> benchmark = {{"mH100mS8lt5", "mH100mS25lt5"}, {"mH200mS8lt5", "mH200mS25lt5","mH200mS50lt5"}, {"mH400mS50lt5", "mH400mS100lt5"}, {"mH600mS50lt5", "mH600mS150lt5"}, {"mH1000mS50lt5", "mH1000mS150lt5", "mH1000mS400lt5"}};

    std::vector<std::vector<TString>> scalar_labels = {{"m_{s} = 8 GeV", "m_{s} = 25 GeV"}, {"m_{s} = 8 GeV", "m_{s} = 25 GeV","m_{s} = 50 GeV"}, {"m_{s} = 50 GeV", "m_{s} = 100 GeV"}, 
            {"m_{s} = 50 GeV", "m_{s} = 150 GeV"}, {"m_{s} = 50 GeV", "m_{s} = 150 GeV", "m_{s} = 400 GeV"}};
    std::vector<TString> phi_labels = {"m_{#Phi} = 100 GeV","m_{#Phi} = 200 GeV","m_{#Phi} = 400 GeV","m_{#Phi} = 600 GeV","m_{#Phi} = 1000 GeV"};

    //std::vector<double> scalings = {{0.0937064*385000., 0.0937064*385000.},{0.0975053*370000., 0.0934636*386000., 0.0922684*391000.}, {0.0917989*393000., 0.0920331*392000.},{ 0.0932221*387000.,  0.0920331*392000.........blahblah.},{, , }};

    double scaling =  32864. + 3212.96; //lumi from doLimitExtrapolation, because prodXSs are set to 1.0 in SampleDetails.h and scaling is LUMI*SampleDetails::mediatorXS/nEvents
    //can plot these ones as global number of expected events/BR[H->ss]

    TCanvas *c1 = new TCanvas("c1","c1",800,600);

    int colors[6] = {kViolet+7, kAzure+7, kTeal, kSpring-2, kOrange-3, kRed-4};
    for(int j=4; j < benchmark.size(); j++){
        TFile* tfile[6];
        TH1D* thist[6];
        TH1D* thist_up[6];
        TH1D* thist_down[6];
        TH1D* thist2[6];
        TF1* fn[6];
        TF1* fn_up[6];
        TF1* fn_down[6];
        TGraph* fn3[6];
        TGraphAsymmErrors* g_err[6];
        double maxVals[6] = {0,0,0,0,0,0};
        double maxVal = 0;
        for(int i=0; i<benchmark.at(j).size(); i++){
            tfile[i] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/outputExtrapolation_"+benchmark.at(j).at(i)+"_dv17_v3.root");
            thist[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
            thist_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
            thist_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");

            char name1[10]; sprintf(name1,"fn_%d",i);
            fn[i] = new TF1(name1,novosibirsk,0.05,300,4);
            fn[i]->SetParameter(0,thist[i]->GetMaximum());
            fn[i]->SetParameter(1,-0.306313); //fn->SetParLimits(1,-1,2);
            fn[i]->SetParameter(2,0.380037); //fn->SetParLimits(2,0,2);
            fn[i]->SetParameter(3,-0.173861); //fn->SetParLimits(3,0,20.0);

            thist[i]->Fit( fn[i], "WRV" );
            std::cout << "success - fit nominal" << std::endl;
            sprintf(name1,"fnUp_%d",i);
            fn_up[i] = new TF1(name1,novosibirsk,0.05,300,4);
            fn_up[i]->SetParameter(0,thist_up[i]->GetMaximum());
            fn_up[i]->SetParameter(1,-0.306313); //fn->SetParLimits(1,-1,2);
            fn_up[i]->SetParameter(2,0.380037); //fn->SetParLimits(2,0,2);
            fn_up[i]->SetParameter(3,-0.173861); //fn->SetParLimits(3,0,20.0);

            thist_up[i]->Fit( fn_up[i], "WRV" );
            std::cout << "success - fit maxTotal" << std::endl;
            sprintf(name1,"fnDown_%d",i);
            fn_down[i] = new TF1(name1,novosibirsk,0.05,300,4);
            fn_down[i]->SetParameter(0,thist_down[i]->GetMaximum());
            fn_down[i]->SetParameter(1,-0.2); //fn->SetParLimits(1,-1,2);
            fn_down[i]->SetParameter(2,0.380037); //fn->SetParLimits(2,0,2);
            fn_down[i]->SetParameter(3,-0.173861); //fn->SetParLimits(3,0,20.0);

            thist_down[i]->Fit( fn_down[i], "WRV" );

            std::cout << "success - fit minTotal" << std::endl;

            Double_t ctau[1000];
            Double_t eff[1000];
            Double_t effUp[1000];
            Double_t effDown[1000];
            int ntot = 0;            
            TGraph *g_res = new TGraph(thist[i]);

            for (Int_t k = 0; k < 1000; k++) {
                Double_t life, nev, nev_up, nev_down;
                g_res->GetPoint(k, life, nev);
                nev = fn[i]->Eval(life);
                nev_up = fn_up[i]->Eval(life) - nev;
                nev_down = nev - fn_down[i]->Eval(life);
                if (nev == 0) {
                    std::cout << "Continue because nev= " << nev << std::endl;
                    continue;
                }
                if(nev_down< 0) nev_down = 0;
                if(nev/scaling > maxVals[i]) maxVals[i] = nev/scaling;
                eff[ntot] = nev/scaling;
                ctau[ntot] = life;
                effUp[ntot] = nev_up/scaling; 
                effDown[ntot] = nev_down/scaling;
                ntot++;
                if((nev_up+nev)/scaling > maxVal) maxVal = (nev_up+nev)/scaling;
            }

            fn3[i] = new TGraph(ntot, ctau, eff);
            fn3[i]->SetLineColor(colors[i]);
            fn3[i]->SetLineStyle(i+1);
            fn3[i]->SetLineWidth(3);

            g_err[i] = new TGraphAsymmErrors(ntot, ctau, eff);
            for(int k=0;k<ntot; k++){

                g_err[i]->SetPointEYlow(k, effDown[k]);
                g_err[i]->SetPointEYhigh(k, effUp[k]);
            }
            g_err[i]->SetFillColorAlpha(colors[i], 0.15);
            fn3[i]->SetFillColorAlpha(colors[i], 0.15);
            g_err[i]->SetLineWidth(0);
        }
        std::cout << "iterated through all samples on plot" << std::endl;
        if(benchmark.at(j).size() > 5) fn3[5]->SetLineColor(98);
        c1->SetLogx();
        fn3[1]->Draw("AL");
        fn3[1]->GetYaxis()->SetTitleOffset(1.6);
        fn3[1]->GetXaxis()->SetLimits(0.05,100);
        fn3[1]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
        fn3[1]->GetYaxis()->SetRangeUser(0,maxVal*1.2);
        fn3[1]->GetYaxis()->SetTitle("Global efficiency");
        fn3[1]->Draw("AL");

        c1->Update();
        g_err[1]->Draw("c3 SAME");

        for(int i=0; i<benchmark.at(j).size(); i++){
            if( i == 1) continue;
            g_err[i]->Draw("c3 SAME");
            fn3[i]->Draw("LSAME");
        }
        
        std::cout << "Drew all samples" << std::endl;
        double ymin = 0.75;
        if(benchmark.at(j).size() == 2) ymin = 0.78;
        TLegend *leg = new TLegend(0.18,ymin,0.5,0.86);
        leg->SetFillStyle(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.035);
        for(int i=0; i< benchmark.at(j).size(); i++) leg->AddEntry(fn3[i],scalar_labels.at(j).at(i),"lf");
        leg->Draw();

        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.045);
        latex.SetTextAlign(11);  //align at top
        latex.DrawLatex(.195,.885,phi_labels.at(j));

        ATLASLabel(0.66,0.88,"Internal");
        char name2[40];
        sprintf(name2,"%d_dv17_v3.pdf", j);
        c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/GlobalEfficiencies_Combined_scalar_"+TString(name2));
        std::cout << "made a plot" << std::endl;
    }
    return;
}
