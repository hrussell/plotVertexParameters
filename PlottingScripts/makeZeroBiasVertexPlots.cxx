void makeZeroBiasVertexPlots(){
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c_1","c_1",800,600);

    TChain* ch; ch = new TChain("recoTree");
    ch->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/ZeroBias/fullDataset_ZeroBias_v17.root");

    TH1D* h[2][10];

    TLegend *leg = new TLegend(0.6,0.73,.92,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.03);
    TString label[2] = {"All MS Vertices","Good MS Vertices"};

    std::vector<TString> plotTypes = {"MSVertex_eta", "MSVertex_phi", "MSVertex_nTrks","MSVertex_closestJetdR","MSVertex_closestTrackdR","MSVertex_sumPtTracksInCone","MSVertex_nMDT","MSVertex_nRPC+MSVertex_nTGC"};
    std::vector<TString> axesLabels = {"MS Vertex #eta", "MS Vertex #phi", "Number of tracklets in MS Vertex","#DeltaR(vertex, closest jet)","#DeltaR(vertex, closest track)",
            "#Sigma(p_{T}[Tracks in vertex cone])","Number of MDT hits in vertex cone","Number of RPC+TGC hits in vertex cone"};

    std::vector<double> xMin = {-2.5,-3.2,0,0,0,0,0,0};
    std::vector<double> xMax = {2.5,3.2,20,11,11,40,3000,3000};
    
    for(int i=0;i<plotTypes.size();i++){
        h[0][i] = new TH1D(TString("n"+to_string(i)),TString("n"+to_string(i)),20,xMin.at(i),xMax.at(i));
        h[0][i]->Sumw2();
        ch->Project(TString("n"+to_string(i)),plotTypes.at(i),"");		
        h[0][i]->SetLineColor(kBlack);
        h[0][i]->SetLineStyle(1);

        h[1][i] = new TH1D(TString("ne"+to_string(i)),TString("ne"+to_string(i)),20,xMin.at(i),xMax.at(i));
        ch->Project(TString("ne"+to_string(i)), plotTypes.at(i),"MSVertex_isGood");    	
        h[1][i]->SetLineColor(kGreen+1);
        h[1][i]->SetLineStyle(7);

        h[0][i]->GetXaxis()->SetTitle(axesLabels.at(i));
        h[0][i]->GetYaxis()->SetTitle("Number of vertices");
        h[0][i]->GetYaxis()->SetRangeUser(0,h[0][i]->GetMaximum()*1.5);

        c1->Clear();
        
        h[0][i]->Draw("hist");
        h[1][i]->Draw("hist same");
        leg->AddEntry(h[0][i],label[0],"l");
        leg->AddEntry(h[1][i],label[1],"l");


        leg->Draw();
        gPad->RedrawAxis();
        ATLASLabel(0.19,0.88,"Internal", kBlack);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.04);
        latex.SetTextAlign(13);  //align at top
        latex.DrawLatex(.19,.87,"Zero-bias data");
        latex.DrawLatex(.19,.83,"#sqrt{s}=13 TeV, 36.1 fb^{-1}");
        
        c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/ZeroBiasData/ZeroBias_"+plotTypes.at(i)+".pdf");
        leg->Clear();
    }
}