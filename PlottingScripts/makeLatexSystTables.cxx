//
//  makeLatexSystTables.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 13/03/17.
//
//

#include <stdio.h>
#include "../PlottingTools/PlottingPackage/SampleDetails.h"
#include "TString.h"

void makeLatexSystTables(TString model){
    
    std::vector<TString> benchmark;
    std::vector<string> massLine;
    if(model == "stealth"){
        benchmark = {"mg250", "mg500", "mg800", "mg1200", "mg1500", "mg2000"};
        massLine = {"250","500","800","1200","1500","2000"};
    }
    else if(model == "higgs"){
        benchmark = {"mH125mS5lt5","mH125mS8lt5","mH125mS15lt5","mH125mS25lt5","mH125mS40lt5","mH125mS55lt5"};
        massLine = {"5", "8", "15", "25", "40", "55"};
    }
    else if(model == "scalar"){
        benchmark = {"mH100mS8lt5", "mH100mS25lt5", "mH200mS8lt5", "mH200mS25lt5","mH200mS50lt5", "mH400mS50lt5", "mH400mS100lt5", "mH600mS50lt5", "mH600mS150lt5", "mH1000mS50lt5", "mH1000mS150lt5", "mH1000mS400lt5"};
        massLine = {"100 & 8","100 & 25", "200 & 8","200 & 25" ,"200 & 50", "400 & 50","400 & 100",
            "600 & 50","600 & 150", " 1000 & 50" ," 1000 & 150", "1000 & 400"};
    }
    ofstream file;
    file.open("SystematicsTables.tex",ios::out);
    
    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if(model == "higgs" || model == "stealth")file << "\\begin{tabular}{c|cccc}" << endl;
    else if( model == "scalar")file << "\\begin{tabular}{cc|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\Phi}$} & {$m_{s}$} & Trigger reconstruction & Pileup & PDF & Total \\" << endl;
    file << "\\midrule" << endl;
    
    double sc1 = 100.; double sc2 = 1.;
    for(unsigned int i=0; i<benchmark.size(); i++){
        SampleDetails::setGlobalVariables(benchmark.at(i));
        std::vector<double> systs_up = {SampleDetails::BTrigSysErr_up,SampleDetails::BTrigSysErr_down,SampleDetails::BTrigSysErr_up 
        file << setw(2) << sa[l] << setw(2) ;
        for (int ll=1;ll<=h[l]->GetNbinsX();ll++)
        {
            if(h[l]->GetBinError(ll) < 0.0000095){ sc1 = 1000000.; sc2 = 10000.;}
            else if(h[l]->GetBinError(ll) < 0.000095){ sc1 = 100000.; sc2 = 1000.;}
            else if(h[l]->GetBinError(ll) < 0.00095){ sc1 = 10000.; sc2 = 100.;}
            else if(h[l]->GetBinError(ll) < 0.0095){ sc1 = 1000.; sc2 = 10.;}
            else if(h[l]->GetBinError(ll) < 0.095){ sc1 = 100.; sc2 = 1.;}
    

        std::cout << massLine.at(i) <<
    }

    
}