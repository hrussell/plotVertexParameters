void Pal1()
{
    static Int_t  colors[50];
    static Bool_t initialized = kFALSE;
    Double_t Red[3]    = { 1.00, 0.00, 0.00};
    Double_t Green[3]  = { 1.00, 0.80, 0.00};
    Double_t Blue[3]   = { 1.00, 0.20, 0.00};
    Double_t Length[3] = { 0.00, 0.50, 1.00 };
    if(!initialized){
        Int_t FI = TColor::CreateGradientColorTable(3,Length,Red,Green,Blue,50,0.8);
        for (int i=0; i<50; i++) colors[i] = FI+i;
        initialized = kTRUE;
        return;
    }
    gStyle->SetPalette(50,colors);
}
void Pal2()
{
    static Int_t  colors[50];
    static Bool_t initialized = kFALSE;
    Double_t Red[3]    = { 1.00, 0.80, 0.00};
    Double_t Green[3]  = { 1.00, 0.00, 0.00};
    Double_t Blue[3]   = { 1.00, 0.20, 0.00};
    Double_t Length[3] = { 0.00, 0.50, 1.00 };
    if(!initialized){
        Int_t FI = TColor::CreateGradientColorTable(3,Length,Red,Green,Blue,50,0.8);
        for (int i=0; i<50; i++) colors[i] = FI+i;
        initialized = kTRUE;
        return;
    }
    gStyle->SetPalette(50,colors);
}

void overlay2dHists(TString name1, TString name2){
    TH2D* h1 = (TH2D*)_file0->Get(name1);
    TH2D* h2 = (TH2D*)_file0->Get(name2);

    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    TCanvas *c2 = new TCanvas("c2","c2",800,600);

    gStyle->SetPadRightMargin(0.1);

    h1->Rebin2D(2,2); h2->Rebin2D(2,2);

    h1->GetXaxis()->SetTitle("c#tau_{1} [mm]");
    h2->GetXaxis()->SetTitle("c#tau_{1} [mm]");
    h1->GetYaxis()->SetTitle("c#tau_{2} [mm]");
    h2->GetYaxis()->SetTitle("c#tau_{2} [mm]");
    TExec *ex1 = new TExec("ex1","Pal1();");
    TExec *ex2 = new TExec("ex2","Pal2();");
    gPad->RedrawAxis(); 
    gStyle->SetPadRightMargin(0.1);
    h1->SetMinimum(0);
    double tmpmax = h1->GetMaximum();
    if(h2->GetMaximum() > tmpmax) tmpmax = h2->GetMaximum();
    h1->SetMaximum(tmpmax*1.2);
    h2->SetMaximum(tmpmax*1.2);
    h2->SetMinimum(0);


    h1->Draw("COLZ"); 
    ex1->Draw();
    h1->Draw("COLZ same");

    c1->Print(name1+".pdf");
    c2->cd();  
    h2->Draw("COLZ");
    ex2->Draw();
    h2->Draw("COLZ SAME");
    gPad->RedrawAxis();
    gStyle->SetPadRightMargin(0.1);
    c2->Print(name2+".pdf");

}
