void makeMSVertexHitsPlots(){
  TCanvas *c1 = new TCanvas("c1","c1",800,600);
  c1->cd();
  TChain *ch = new TChain("recoTree");
  ch->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304908.MadGraphPythia8EvtGen_StealthSUSY_mG2000.AOD_DVAna.r7772_dv17_hist/*root*");

  TH1D *hits[8];
  char nameh[10];
  Int_t colorSig[8] = {kRed+2, kOrange+8,kYellow-3,kGreen-3,kTeal-6,kAzure+5,kBlue-3,kViolet+5 };
  Int_t lines[8] = {3,2,9,7,1,4,5,6};
  TLegend *leg = new TLegend(0.6,0.65,0.9,0.9);
  leg->SetFillStyle(0); leg->SetBorderSize(0);
  leg->SetTextSize(0.035);
  
  for(int i=0;i<8;i++){
    sprintf(nameh,"hits%d",i);
    int minCut = 3000 + 250*i;
    int maxCut = 3250 + 250*i;

    std::cout << nameh << ": " << TString::Itoa(minCut,10) << ", " << TString::Itoa(maxCut,10) << std::endl;

    hits[i] = new TH1D(nameh, nameh, 40, 0, 10000);
    ch->Draw("MSVertex_nMDT >>"+TString(nameh),"MSVertex_indexLLP > -1 && TMath::Abs(MSVertex_eta) < 0.8 && LLP_Lxy[MSVertex_indexLLP] > "+TString::Itoa(minCut,10) + " &&  LLP_Lxy[MSVertex_indexLLP] < "+TString::Itoa(maxCut,10));
    hits[i]->Scale(1./hits[i]->Integral());  
    hits[i]->SetLineColor(colorSig[i]); hits[i]->SetLineStyle(lines[i]); hits[i]->SetLineWidth(3);	
    leg->AddEntry(hits[i],TString::Itoa(minCut,10) + "< L_{xy} < " +TString::Itoa(maxCut,10) + " mm", "l");
  }
  hits[0]->GetXaxis()->SetTitle("Number of associated MDT hits"); hits[0]->GetYaxis()->SetTitle("Fraction of vertices");
  hits[0]->Draw();
  for(int i=1;i<8; i++){
     hits[i]->Draw("SAME");
  }
  leg->Draw();
  ATLASLabel(0.3,0.88,"Simulation Internal");

}
