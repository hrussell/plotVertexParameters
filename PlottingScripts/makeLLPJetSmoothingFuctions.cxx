void makeLLPJetSmoothingFunctions(){
    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    c1->SetLogy(1);
    TString lxy_slices[6] = {"0.0","0.5","1.0","1.5","2.0","3.5"};
    TString pT_slices[6] = {"0","150000","1000000000"};
    TF1* g1 = new TF1("m1","[0]*TMath::Landau(-x,[1],[2])",0,0.5);
       g1->SetParameters(1,0,1); //for example
    //TF1* g1 = new TF1("m1","landau",0,0.5);
    TF1* g2 = new TF1("m2","gaus",0.5,1.5);
    TF1* g3 = new TF1("m3","landau",0.8,4);
    // The total is the sum of the three, each has 3 parameters
    //p0*exp(-0.5*((x-p1)/p2)^2))
   TF1 *total = new TF1("total","m1 + gaus(3) + landau(6)",0,4);
    //TF1* total = new TF1("mstotal","[0]*exp(-0.5*( (x-[1])/[2])^2) +[3]*exp(-0.5*( (x-[4])/[5])^2) + [6]*exp(-0.5*( (x-[7])/[8])^2)",0,4);    
    //total->SetParameter(0,0.01);total->SetParameter(1,0.5);total->SetParameter(2,0.1);
    //total->SetParameter(3,0.15);total->SetParameter(4,1.0);total->SetParameter(5,0.2);
    //total->SetParameter(6,0.01);total->SetParameter(7,2.0);total->SetParameter(8,0.5);
    TH1F* scaleHists[5][2]; for(int i=0; i<5;i++){ 
        for(int j=0;j<2;j++){
        TString histName = TString("scaleHist" + to_string(i) + to_string(j)); std::cout << "histogram name: " << histName << std::endl;
        scaleHists[i][j] = new TH1F(histName, histName,80,0,4);
        TString cuts = "(CalibJet_isGoodStand && CalibJet_indexLLP > -1 && TMath::Abs(LLP_eta[CalibJet_indexLLP]) < 1.5 && LLP_Lxy[CalibJet_indexLLP]*0.001 > -1 && LLP_Lxy[CalibJet_indexLLP]*0.001 > "
                +lxy_slices[i]+ " && LLP_Lxy[CalibJet_indexLLP]*0.001 < "+ lxy_slices[i+1]+" && LLP_pT[CalibJet_indexLLP] > " + pT_slices[j] + "&& LLP_pT[CalibJet_indexLLP] < " 
                +pT_slices[j+1]+ " && ( (CalibJet_jvt > 0.59 && CalibJet_pT < 60. && TMath::Abs(CalibJet_eta) < 2.4) || CalibJet_pT > 60. || TMath::Abs(CalibJet_eta) > 2.4 ) )";
        ch->Draw("CalibJet_pT/(LLP_pT[CalibJet_indexLLP]*0.001)>>"+histName, cuts,"");
        scaleHists[i][j]->Scale(1./scaleHists[i][j]->Integral());
        scaleHists[i][j]->Draw();
        // Define the parameter array for the total function
         Double_t par[9];

        // Fit each function and add it to the list of functions
        scaleHists[i][j]->Fit(g1,"R");
        scaleHists[i][j]->Fit(g2,"R+");
        scaleHists[i][j]->Fit(g3,"R+");

        // Get the parameters from the fit
        g1->GetParameters(&par[0]);
       g2->GetParameters(&par[3]);
       g3->GetParameters(&par[6]);

        // Use the parameters on the sum
       total->SetParameters(par);

        scaleHists[i][j]->Fit("total");
        c1->Print(histName+".pdf");
        
        }
    }

}