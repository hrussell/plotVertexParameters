
void makeMSSystematicsPlots_variableBins_RMSError(){
    //0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    SetAtlasStyle();

    //c1->SetRightMargin(0.05);
    //c1->SetTopMargin(0.07);
    //c1->SetLeftMargin(0.15);
    //c1->SetBottomMargin(0.15);
    TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
    pad1->SetTopMargin(0.075);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    pad1->SetLogy();
    pad1->Draw();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();



    //v13: TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/data/outputMSSystematics_20p7_j600.root");
    TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/data/outputMSSystematics_RMS.root");

    //v13: TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/bkgdJZ/JZAll/outputMSSystematics_20p7_j600.root");
    TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/bkgdJZ/JZAll/outputMSSystematics_RMS.root");


    TString plotDir = "../OutputPlots/SystematicsOutputs/allData_j600_variableBins_dv14_RMSE/";

    std::vector<TString> histNames;
    histNames.push_back("eta1_pT_nMSTracklets");
    histNames.push_back("eta3_pT_nMSTracklets");
    histNames.push_back("eta1_pT_nMuonRoIs");
    histNames.push_back("eta3_pT_nMuonRoIs");
    histNames.push_back("eta1_pT_nAssocMSeg");
    histNames.push_back("eta3_pT_nAssocMSeg");

    TH1D *h_pT_data = (TH1D*)_file0->Get("eta1_pT_denom_LJ");
    TH1D *h_pT_mc = (TH1D*)_file1->Get("eta1_pT_denom_LJ");   

    Double_t xAxisValues[] = {0,600,700,800,900,1000,1200,1400,1600,1800,2000,2500,3500};
    Int_t  binnum = 12;
    TH1D* h_mc_denom = new TH1D("mc_den","mc_den", binnum, xAxisValues);
    TH1D* h_data_denom = new TH1D("data_den","data_den", binnum, xAxisValues);
    h_mc_denom->Sumw2(); h_data_denom->Sumw2();

    for(unsigned int iBin = 2; iBin < 6; iBin++){
        std::cout << "bin " << iBin << " has content: " << iBin+5 << std::endl;
        h_mc_denom->SetBinContent(iBin,h_pT_mc->GetBinContent(iBin+5));
        h_mc_denom->SetBinError(iBin,sqrt(h_pT_mc->GetBinContent(iBin+5)));
        h_data_denom->SetBinContent(iBin,h_pT_data->GetBinContent(iBin+5));
        h_data_denom->SetBinError(iBin,sqrt(h_pT_data->GetBinContent(iBin+5)));
    }
    for(unsigned int iBin = 6; iBin < 11; iBin++){
        std::cout <<"bin " << iBin << " has content: " << iBin*2-1 << "+" << iBin*2 << std::endl;
        h_mc_denom->SetBinContent(iBin,
                h_pT_mc->GetBinContent(iBin*2 - 1) + h_pT_mc->GetBinContent(iBin*2));
        h_data_denom->SetBinContent(iBin,
                h_pT_data->GetBinContent(iBin*2 - 1) + h_pT_data->GetBinContent(iBin*2));
        h_data_denom->SetBinError(iBin,
                sqrt(h_pT_data->GetBinContent(iBin*2 - 1) + h_pT_data->GetBinContent(iBin*2)));
    }
    h_mc_denom->SetBinContent(11, h_pT_mc->Integral(21, 25));
    h_mc_denom->SetBinError(11, sqrt(h_pT_mc->Integral(21,25)) );

    h_data_denom->SetBinContent(11, h_pT_data->Integral(21, 25));
    h_data_denom->SetBinError(11, sqrt(h_pT_data->Integral(21,25)) );

    h_mc_denom->SetBinContent(12, h_pT_mc->Integral(26, 35));
    h_mc_denom->SetBinError(12, sqrt(h_pT_mc->Integral(26,35)) );

    h_data_denom->SetBinContent(12, h_pT_data->Integral(26, 35));
    h_data_denom->SetBinError(12, sqrt(h_pT_data->Integral(26,35)) );

    h_pT_data = (TH1D*)_file0->Get("eta3_pT_denom_LJ");
    h_pT_mc = (TH1D*)_file1->Get("eta3_pT_denom_LJ");   

    Double_t xAxisValuesec[] = {0,600,700,800,900,1000,1100,1300,1500,2000,2500};
    TH1D* h_mc_eta3_denom = new TH1D("mc_den3","mc_den3", 10, xAxisValuesec);
    TH1D* h_data_eta3_denom = new TH1D("data_den3","data_den3", 10, xAxisValuesec);
    h_mc_eta3_denom->Sumw2(); h_data_eta3_denom->Sumw2();

    for(unsigned int iBin = 2; iBin < 7; iBin++){
        std::cout << "bin " << iBin << " has content: " << iBin+5 << std::endl;
        h_mc_eta3_denom->SetBinContent(iBin,h_pT_mc->GetBinContent(iBin+5));
        h_mc_eta3_denom->SetBinError(iBin,sqrt(h_pT_mc->GetBinContent(iBin+5)));
        h_data_eta3_denom->SetBinContent(iBin,h_pT_data->GetBinContent(iBin+5));
        h_data_eta3_denom->SetBinError(iBin,sqrt(h_pT_data->GetBinContent(iBin+5)));
    }
    for(unsigned int iBin = 7; iBin < 9; iBin++){
        std::cout <<"bin " << iBin << " has content: " << iBin*2-1 << "+" << iBin*2 << std::endl;
        h_mc_eta3_denom->SetBinContent(iBin,
                h_pT_mc->GetBinContent(iBin*2 - 1) + h_pT_mc->GetBinContent(iBin*2-2));
        h_mc_eta3_denom->SetBinError(iBin,
                sqrt(h_pT_mc->GetBinContent(iBin*2 - 1) + h_pT_mc->GetBinContent(iBin*2-2)));
        h_data_eta3_denom->SetBinContent(iBin,
                h_pT_data->GetBinContent(iBin*2 - 1) + h_pT_data->GetBinContent(iBin*2-2));
        h_data_eta3_denom->SetBinError(iBin,
                sqrt(h_pT_data->GetBinContent(iBin*2 - 1) + h_pT_data->GetBinContent(iBin*2-2)));
    }
    h_mc_eta3_denom->SetBinContent(9, h_pT_mc->Integral(16, 20));
    h_mc_eta3_denom->SetBinError(9, sqrt(h_pT_mc->Integral(16,20)) );

    h_data_eta3_denom->SetBinContent(9, h_pT_data->Integral(16, 20));
    h_data_eta3_denom->SetBinError(9, sqrt(h_pT_data->Integral(16,20)) );

    h_mc_eta3_denom->SetBinContent(10, h_pT_mc->Integral(21, 25));
    h_mc_eta3_denom->SetBinError(10, sqrt(h_pT_mc->Integral(21,25)) );

    h_data_eta3_denom->SetBinContent(10, h_pT_data->Integral(21, 25));
    h_data_eta3_denom->SetBinError(10, sqrt(h_pT_data->Integral(21,25)) );

    for (auto histName : histNames){
        TH1D* h_data_num;        TH1D* h_mc_num;
        TH1D* h_data_sq;        TH1D* h_mc_sq;

        if(histName.Contains("eta1")){
            h_mc_num = new TH1D("mc_num","mc_num", binnum, xAxisValues);
            h_data_num = new TH1D("data_num","data_num", binnum, xAxisValues);
            h_mc_sq = new TH1D("mc_sq","mc_sq", binnum, xAxisValues);
            h_data_sq = new TH1D("data_sq","data_sq", binnum, xAxisValues);
        } else{
            h_mc_num = new TH1D("mc_num","mc_num", 10, xAxisValuesec);
            h_data_num = new TH1D("data_num","data_num", 10, xAxisValuesec);
            h_mc_sq = new TH1D("mc_sq","mc_sq", 10, xAxisValuesec);
            h_data_sq = new TH1D("data_sq","data_sq", 10, xAxisValuesec);
        }

        h_mc_num->Sumw2(); h_data_num->Sumw2();
        h_data_num->SetLineColor(kBlack);h_data_num->SetMarkerColor(kBlack);h_data_num->SetMarkerSize(1.2);h_data_num->SetLineWidth(2);h_data_num->SetMarkerStyle(20);
        h_mc_num->SetLineColor(kBlue);h_mc_num->SetMarkerColor(kBlue);h_mc_num->SetMarkerSize(1.2);h_mc_num->SetLineWidth(2);h_mc_num->SetMarkerStyle(4); h_mc_num->SetLineStyle(2);  

        pad1->cd(); 
        gStyle->SetPadTickX(2);
        gStyle->SetPadTickY(2);
        TH1D *h_MC =  (TH1D*)_file1->Get(histName+"_numLJ");
        TH1D *h_Data = (TH1D*)_file0->Get(histName+"_numLJ");

        TH1D *h_MCsq =  (TH1D*)_file1->Get(histName+"_sqLJ");
        TH1D *h_Datasq = (TH1D*)_file0->Get(histName+"_sqLJ");

        std::cout <<" entries data/mc: " << h_Data->GetEntries()  <<"/" <<h_MC->GetEntries() <<  std::endl;
        std::cout <<" entries data/mc sq: " << h_Datasq->GetEntries()  <<"/" <<h_MCsq->GetEntries() <<  std::endl;

        if(h_MC->GetEntries() == 0 || h_Data->GetEntries() == 0) continue;

        pad1->SetLogy(0); h_data_num->SetMinimum(0);

        if( histName.Contains("eta1") ){
            for(unsigned int iBin = 2; iBin < 6; iBin++){
                std::cout << "bin " << iBin << " has content: " << iBin+5 << std::endl;
                h_mc_num->SetBinContent(iBin,h_MC->GetBinContent(iBin+5));
                h_data_num->SetBinContent(iBin,h_Data->GetBinContent(iBin+5));
                h_mc_sq->SetBinContent(iBin,h_MCsq->GetBinContent(iBin+5));
                h_data_sq->SetBinContent(iBin,h_Datasq->GetBinContent(iBin+5));
            }
            for(unsigned int iBin = 6; iBin < 11; iBin++){
                std::cout <<"bin " << iBin << " has content: " << iBin*2-1 << "+" << iBin*2 << std::endl;
                h_mc_num->SetBinContent(iBin,
                        h_MC->GetBinContent(iBin*2 - 1) + h_MC->GetBinContent(iBin*2));
                h_data_num->SetBinContent(iBin,
                        h_Data->GetBinContent(iBin*2 - 1) + h_Data->GetBinContent(iBin*2));

                h_mc_sq->SetBinContent(iBin,
                        h_MCsq->GetBinContent(iBin*2 - 1) + h_MCsq->GetBinContent(iBin*2));
                h_data_sq->SetBinContent(iBin,
                        h_Datasq->GetBinContent(iBin*2 - 1) + h_Datasq->GetBinContent(iBin*2));
            }
            h_mc_num->SetBinContent(11, h_MC->Integral(21, 25));
            h_data_num->SetBinContent(11, h_Data->Integral(21, 25));
            h_mc_num->SetBinContent(12, h_MC->Integral(26, 35));
            h_data_num->SetBinContent(12, h_Data->Integral(26, 35));

            h_mc_sq->SetBinContent(11, h_MCsq->Integral(21, 25));
            h_data_sq->SetBinContent(11, h_Datasq->Integral(21, 25));
            h_mc_sq->SetBinContent(12, h_MCsq->Integral(26, 35));
            h_data_sq->SetBinContent(12, h_Datasq->Integral(26, 35));

            for(int iBin = 2; iBin<13; iBin++){
                h_mc_num->SetBinError(iBin,1);
                h_data_num->SetBinError(iBin,1);
            }

            h_mc_num->Divide(h_mc_denom);
            h_data_num->Divide(h_data_denom);

            for(int iBin=2; iBin < 13; iBin++){
                h_mc_num->SetBinError(iBin, sqrt( h_mc_sq->GetBinContent(iBin)/h_mc_denom->GetBinContent(iBin) - h_mc_num->GetBinContent(iBin)*h_mc_num->GetBinContent(iBin) ) );
                h_data_num->SetBinError(iBin, sqrt( h_data_sq->GetBinContent(iBin)/h_data_denom->GetBinContent(iBin) - h_data_num->GetBinContent(iBin)*h_data_num->GetBinContent(iBin) ) );
            }

        } else{
            for(unsigned int iBin = 2; iBin < 7; iBin++){
                std::cout << "bin " << iBin << " has content: " << iBin+5 << std::endl;
                h_mc_num->SetBinContent(iBin,h_MC->GetBinContent(iBin+5));
                h_data_num->SetBinContent(iBin,h_Data->GetBinContent(iBin+5));
                h_mc_sq->SetBinContent(iBin,h_MCsq->GetBinContent(iBin+5));
                h_data_sq->SetBinContent(iBin,h_Datasq->GetBinContent(iBin+5));
            }
            for(unsigned int iBin = 7; iBin < 9; iBin++){
                std::cout <<"bin " << iBin << " has content: " << iBin*2-1 << "+" << iBin*2 << std::endl;
                h_mc_num->SetBinContent(iBin,
                        h_MC->GetBinContent(iBin*2 - 1) + h_MC->GetBinContent(iBin*2-2));
                h_data_num->SetBinContent(iBin,
                        h_Data->GetBinContent(iBin*2 - 1) + h_Data->GetBinContent(iBin*2-2));
                h_mc_sq->SetBinContent(iBin,
                        h_MCsq->GetBinContent(iBin*2 - 1) + h_MCsq->GetBinContent(iBin*2-2));
                h_data_sq->SetBinContent(iBin,
                        h_Datasq->GetBinContent(iBin*2 - 1) + h_Datasq->GetBinContent(iBin*2-2));
            }
            h_mc_num->SetBinContent(9, h_MC->Integral(16, 20));
            h_data_num->SetBinContent(9, h_Data->Integral(16, 20));
            h_mc_num->SetBinContent(10, h_MC->Integral(21, 25));
            h_data_num->SetBinContent(10, h_Data->Integral(21, 25));
            h_mc_sq->SetBinContent(9, h_MCsq->Integral(16, 20));
            h_data_sq->SetBinContent(9, h_Datasq->Integral(16, 20));
            h_mc_sq->SetBinContent(10, h_MCsq->Integral(21, 25));
            h_data_sq->SetBinContent(10, h_Datasq->Integral(21, 25));

            for(int iBin = 2; iBin<11; iBin++){
                h_mc_num->SetBinError(iBin,1);
                h_data_num->SetBinError(iBin,1);
            }

            h_mc_num->Divide(h_mc_eta3_denom);
            h_data_num->Divide(h_data_eta3_denom);

            for(int iBin=2; iBin < 11; iBin++){
                h_mc_num->SetBinError(iBin, sqrt( h_mc_sq->GetBinContent(iBin)/h_mc_eta3_denom->GetBinContent(iBin) - h_mc_num->GetBinContent(iBin)*h_mc_num->GetBinContent(iBin) ) );
                h_data_num->SetBinError(iBin, sqrt( h_data_sq->GetBinContent(iBin)/h_data_eta3_denom->GetBinContent(iBin) - h_data_num->GetBinContent(iBin)*h_data_num->GetBinContent(iBin) ) );
            }
        }
        //h_mc_num->SetBinContent(12,h_mc_num->GetBinContent(12)+h_mc_num->GetBinError(12));      
        /*
        for(unsigned int iBin = 1; iBin < 13; iBin++){
            double nDmc = 0;
            if(histName.Contains("eta1")) nDmc = h_mc_denom->GetBinContent(iBin);
            else nDmc = h_mc_eta3_denom->GetBinContent(iBin);
            if(nDmc != 0){
                double effMC = h_mc_num->GetBinContent(iBin)/nDmc;
                h_mc_num->SetBinContent(iBin,effMC); h_mc_num->SetBinError(iBin,sqrt(effMC*(1.-effMC) / nDmc));
            }
            double nDdata = 0;
            if(histName.Contains("eta1")) nDdata = h_data_denom->GetBinContent(iBin);
            else nDdata = h_data_eta3_denom->GetBinContent(iBin);
            if(nDdata != 0){
                double effdata = h_data_num->GetBinContent(iBin)/nDdata;
                h_data_num->SetBinContent(iBin,effdata); h_data_num->SetBinError(iBin,sqrt(effdata*(1.-effdata) / nDdata));
            }
        }*/

        if(histName.Contains("pT_n")){ h_data_num->GetXaxis()->SetTitle("jet p_{T} [GeV]");
        if(histName.Contains("Track")) h_data_num->GetYaxis()->SetTitle("<# tracklets in jet cone>");
        else if(histName.Contains("RoI") ) h_data_num->GetYaxis()->SetTitle("<# Muon RoIs in jet cone>");
        else  h_data_num->GetYaxis()->SetTitle("<#in jet cone>");
        }

        double maxVal = h_mc_num->GetMaximum(); if(h_data_num->GetMaximum() > maxVal) maxVal = h_data_num->GetMaximum();
        h_data_num->SetMaximum(2.*maxVal);
        h_data_num->SetTitle("");

        TString append = "";

        if(histName.Contains("LJ")) append = ", leading jets"; 
        else if(histName.Contains("AllJets")) append = ", leading and sub-leading jets"; 
        TString title = "";
        if(histName.Contains("full")) title = TString("|#eta| < 2.7") + append;
        else if(histName.Contains("eta1")) title = TString("|#eta| < 0.8") + append;
        else if(histName.Contains("eta3")) title = TString("1.3 < |#eta| < 2.5") + append;

        h_data_num->GetYaxis()->SetLabelSize(0.070);
        h_data_num->GetYaxis()->SetTitleSize(0.065);
        h_data_num->GetYaxis()->SetTitleOffset(0.18);
        h_data_num->GetXaxis()->SetTitleOffset(2.7);
        h_data_num->GetXaxis()->SetLabelOffset(0.5);
        h_data_num->GetYaxis()->SetTitleOffset(0.95);
        double maxxval = histName.Contains("eta1") ? 3500 : 2000;
        double minval = 0;
        bool doHighpT=false;
        if(doHighpT) minval = histName.Contains("eta1") ? 1500 : 800;
        h_data_num->GetXaxis()->SetRangeUser(0,maxxval); 

        h_data_num->Draw();
        h_mc_num->Draw("SAME");

        TLegend *leg = new TLegend(0.65,0.75,0.85,0.9);
        leg->SetFillColor(0);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.05);
        leg->AddEntry(h_mc_num,"Pythia JZXW MC","lp");
        leg->AddEntry(h_data_num,"data","lp");
        leg->Draw();

        TLatex latex2;
        latex2.SetNDC();
        latex2.SetTextColor(kBlack);
        latex2.SetTextSize(0.06);
        latex2.SetTextAlign(31);  //align at bottom
        latex2.DrawLatex(0.95,.94,title);

        //c1->Print(plotDir+histName+".pdf");
        TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
        l.SetNDC();
        l.SetTextFont(72);
        l.SetTextSize(0.06);
        l.SetTextColor(kBlack);
        //l.DrawLatex(.72,.84,"ATLAS");
        TLatex p; 
        p.SetNDC();
        p.SetTextSize(0.06);
        p.SetTextFont(42);
        p.SetTextColor(kBlack);
        // p.DrawLatex(0.72+0.105,0.84,"Internal");

        c1->cd();

        pad2->cd();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);

        TH1D *p1_new = (TH1D*)h_data_num->Clone("data_clone");

        p1_new->Divide(h_mc_num);
        /*for(int i=1;i<p1_new->GetNbinsX(); i++){
            p1_new->SetBinError(i+1,0.0001);
        }*/

        p1_new->SetMarkerStyle(8);
        p1_new->SetMarkerSize(0.8);
        p1_new->SetMarkerColor(kBlack);
        p1_new->SetMinimum(0.0);
        p1_new->SetMaximum(2.0);
        //p1_new->GetXaxis()->SetTitle("to do");
        p1_new->GetXaxis()->SetLabelFont(42);
        p1_new->GetXaxis()->SetLabelSize(0.16);
        p1_new->GetXaxis()->SetLabelOffset(0.05);
        p1_new->GetXaxis()->SetTitleFont(42);
        p1_new->GetXaxis()->SetTitleSize(0.14);
        p1_new->GetXaxis()->SetTitleOffset(1.3);
        p1_new->GetYaxis()->SetNdivisions(505);
        p1_new->GetYaxis()->SetTitle("#font[42]{Data/MC}");
        p1_new->GetYaxis()->SetLabelFont(42);
        p1_new->GetYaxis()->SetLabelSize(0.15);
        p1_new->GetYaxis()->SetTitleFont(42);
        p1_new->GetYaxis()->SetTitleSize(0.13);
        p1_new->GetYaxis()->SetTitleOffset(0.44); 
        p1_new->DrawCopy("eP");

        TLine* line = new TLine();
        line->DrawLine(p1_new->GetXaxis()->GetXmin(),1,p1_new->GetXaxis()->GetXmax(),1);

        TString fitVal = "";
        TString fitVal2 = "";

        TF1 *f1 = new TF1("fit1", "pol0", minval, maxxval);
        p1_new->Fit("fit1","R");
        p1_new->GetXaxis()->SetRangeUser(0,maxxval); 
        fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) 
                                    + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
        //fitVal2 = TString("slope: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(1) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(1) );     

        std::cout << fitVal << std::endl; 
        pad2->SetLogy(0);

        //latex2.DrawLatex(.15,.98,title + ", Data/MC");

        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.1);
        latex.SetTextAlign(13);  //align at top
        //latex.DrawLatex(.25,.92, fitVal);
        //latex.DrawLatex(.25,.82, fitVal2);

        c1->Print(plotDir+"ratio_"+histName+"_pol0.pdf");
        c1->Print(plotDir+"ratio_"+histName+"_pol0.png");
        c1->Print(plotDir+"ratio_"+histName+"_pol0.root");

        delete h_mc_num; delete h_data_num; delete p1_new;
    }


}
